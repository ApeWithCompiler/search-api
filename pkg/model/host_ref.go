package model

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type HostReference struct {
	Host web.DigestHost
	
	Robots *web.DigestUrl
	Sitemaps []web.DigestUrl
}

func NewHostReference(host web.DigestHost) HostReference {
	return HostReference{
		Host: host,
	}
}

func (h *HostReference) SetRobots(url web.DigestUrl) {
	h.Robots = &url
}

func (h *HostReference) AddSitemap(url web.DigestUrl) {
	h.Sitemaps = append(h.Sitemaps, url)
}
