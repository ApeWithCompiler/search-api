package web

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestDigestUrl_String(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := "https://search.home.arpa/search?q=Foobar"

	u, _ := UrlFromString(src)
	got := u.String()

	if got != want {
		t.Errorf("String() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_Hostname(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := "search.home.arpa"

	u, _ := UrlFromString(src)
	got := u.Hostname()

	if got != want {
		t.Errorf("Hostname() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_GetQueryParamter(t *testing.T) {
	var tests = []struct {
		Inp string
		Key string
		WantVal string
		WantFound bool
	}{
		{
			Inp : "http://example.org/foo/bar",
			Key : "bar",
			WantVal: "",
			WantFound: false,
		},
		{
			Inp : "http://example.org/foo?bar=baz",
			Key : "bar",
			WantVal: "baz",
			WantFound: true,
		},
		{
			Inp : "http://example.org/foo?bar=",
			Key : "bar",
			WantVal: "",
			WantFound: true,
		},
	}

	for _, c := range tests {
		uInp, _ := UrlFromString(c.Inp)

		gotVal, gotFound := uInp.GetQueryParam(c.Key)

		if gotFound != c.WantFound || gotVal != c.WantVal {
			t.Errorf("GetQueryParam(%v) returned (%v,%v), want (%v,%v)", c.Key, gotVal, gotFound, c.WantVal, c.WantFound)
		}
	}
}

func TestDigestUrl_ReverseHostname(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := "arpa.home.search"

	u, _ := UrlFromString(src)
	got := u.ReverseHostname()

	if got != want {
		t.Errorf("ReverseHostname() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_Path(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar&s=Bazz#Fragment"
	want := "/search"

	u, _ := UrlFromString(src)
	got := u.Path()

	if got != want {
		t.Errorf("Path() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_Hash(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := "8d700171fa8d3094f800fdc620093d4e67b7e7df"

	u, _ := UrlFromString(src)
	got := u.Hash()

	if got != want {
		t.Errorf("Hash() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_HostHash(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := "93d3c70c03001cb312cc9fcd50bd1caceed45f83"

	u, _ := UrlFromString(src)
	got := u.HostHash()

	if got != want {
		t.Errorf("HostHash() returned %v, want %v", got, want)
	}
}

func TestDigestUrl_MarshalJSON(t *testing.T) {
	src := "https://search.home.arpa/search?q=Foobar"
	want := []byte("\"" + src + "\"")

	u, _ := UrlFromString(src)
	got, err := u.MarshalJSON()
	if err != nil {
		t.Errorf("MarshalJSON() returned error: %v", err)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("MarshalJSON() returned %v, want %v", string(got), string(want))
	}
}

func TestDigestUrl_UnmarshalJSON(t *testing.T) {
	want := "https://search.home.arpa/search?q=Foobar"
	src := []byte(want)

	u := DigestUrl{}
	err := u.UnmarshalJSON([]byte(src))
	if err != nil {
		t.Errorf("UnmarshalJSON() returned error: %v", err)
	}

	if u.String() != want {
		t.Errorf("UnmarshalJSON() returned %v, want %v", u.String(), want)
	}
}

func TestDigestUrl_MarshalJSONStruct(t *testing.T) {
	type Aux struct {
		Url DigestUrl `json:"url"`
	}

	src := "https://search.home.arpa/search?q=Foobar"
	want := []byte("{\"url\":\"https://search.home.arpa/search?q=Foobar\"}")

	u, _ := UrlFromString(src)

	a := Aux{
		Url: u,
	}

	got, _ := json.Marshal(a)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("json.Marshal() returned %v, want %v", string(got), string(want))
	}
}

func TestDigestUrl_Normalize(t *testing.T) {
	var tests = []struct {
		Inp string
		Want string
	}{
		{
			Inp: "http://example.org/foo/bar#baz",
			Want: "http://example.org/foo/bar",
		},
		{
			Inp: "http://example.org/foo/bar?b=foo&a=bar&c=baz",
			Want: "http://example.org/foo/bar?a=bar&b=foo&c=baz",
		},
		{
			Inp: "http://example.org/foo/bar?",
			Want: "http://example.org/foo/bar",
		},
	}

	for _, c := range tests {
		uInp, _ := UrlFromString(c.Inp)

		got, _ := uInp.Normalize()

		if got.String() != c.Want {
			t.Errorf("Normalize() returned %v, want %v", got.String(), c.Want)
		}
	}
}

func TestDigestUrl_Surt(t *testing.T) {
	ENFORCE_FULL_URL = false

	var tests = []struct {
		Inp string
		Want string
	}{
		{
			Inp : "http://example.org/foo/bar",
			Want: "http://(org,example,)/foo/bar",
		},
		{
			Inp : "https://example.org/foo?bar=baz",
			Want: "http://(org,example,)/foo?bar=baz",
		},
		{
			Inp : "/foo?bar=baz",
			Want: "/foo?bar=baz",
		},
		{
			Inp : "https://example.org",
			Want: "http://(org,example,)",
		},
	}

	for _, c := range tests {
		uInp, _ := UrlFromString(c.Inp)

		got := uInp.Surt()

		if got != c.Want {
			t.Errorf("Surt() returned %v, want %v", got, c.Want)
		}
	}
}

func TestDigestUrl_FromRelative(t *testing.T) {
	ENFORCE_FULL_URL = false

	var tests = []struct {
		Ref string
		Inp string
		Want string
	}{
		{
			Ref: "http://example.org/foo/bar",
			Inp: "/baz",
			Want: "http://example.org/baz",
		},
		{
			Ref: "http://example.org/foo/bar",
			Inp: "baz",
			Want: "http://example.org/foo/bar/baz",
		},
		{
			Ref: "http://example.org/foo/bar",
			Inp: "#baz",
			Want: "http://example.org/foo/bar#baz",
		},
		{
			Ref: "http://example.org/foo/bar",
			Inp: "/baz?foo=bar",
			Want: "http://example.org/baz?foo=bar",
		},
	}

	for _, c := range tests {
		uRef, _ := UrlFromString(c.Ref)
		uInp, _ := UrlFromString(c.Inp)

		got, _ := uRef.FromRelative(uInp)

		if got.String() != c.Want {
			t.Errorf("FromRelative() returned %v, want %v", got.String(), c.Want)
		}
	}
}
