package web

type Anchor struct {
	href string
	text string
	rel string
}

func NewAnchor(href, text, rel string) Anchor {
	return Anchor {
		href: href,
		text: text,
		rel: rel,
	}

}

func (a *Anchor) Text() string {
	return a.text
}

func (a *Anchor) Href() string {
	return a.href
}

func (a *Anchor) IsNoFollow() bool {
	return a.rel == "nofollow"
}

func (a *Anchor) IsCannonical() bool {
	return a.rel == "cannonical"
}

// Implement Link interface
func (a *Anchor) Location() string {
	return a.Href()
}

type AnchorEdge struct {
	vertexFrom DigestUrl
	vertexTo DigestUrl
	
	label string
}

func NewAnchorEdge(vertexFrom, vertexTo DigestUrl, label string) AnchorEdge {
	return AnchorEdge {
		vertexFrom: vertexFrom,
		vertexTo: vertexTo,
		label: label,
	}
}

func NewAnchorEdgeFromAnchor(src DigestUrl, anchor Anchor) (AnchorEdge, error) {
	href, err := PromoteLink(src, &anchor)
	if err != nil {
		return AnchorEdge{}, err
	}

	return NewAnchorEdge(src, href, anchor.Text()), nil
}

func (ae *AnchorEdge) VertexFrom() *DigestUrl {
	return &ae.vertexFrom
}

func (ae *AnchorEdge) VertexTo() *DigestUrl {
	return &ae.vertexTo
}

func (ae *AnchorEdge) Label() string {
	return ae.label
}
