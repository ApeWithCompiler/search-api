package web

import (
	"fmt"
	"net/url"
)

var ENFORCE_FULL_URL = true

func AssertEnforceUrl(u *url.URL) bool {
	if !ENFORCE_FULL_URL {
		return true
	}

	if u.Hostname() == "" {
		return false
	}

	if u.Scheme == "" {
		return false
	}

	return true
}

func AssertEnforceError() error {
	return fmt.Errorf("Assertion failed: Enforce absolute Url")
}
