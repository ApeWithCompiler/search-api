package web

import (
	"fmt"
	"net/url"
	"path"
	"sort"
	"strings"

	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
)

type QueryParameter struct {
	Key   string
	Value string
}

func UrlFromString(input string) (DigestUrl, error) {
	p, err := url.Parse(input)
	if err != nil {
		return DigestUrl{}, err
	}

	if p.Hostname() == "" && p.Path == "" && p.RawQuery == "" && p.Fragment == "" {
		return DigestUrl{}, fmt.Errorf("Url is empty")
	}

	if !AssertEnforceUrl(p) {
		return DigestUrl{}, AssertEnforceError()
	}

	return DigestUrl{u: *p}, nil
}

func UrlFromLocation(host DigestHost, location string) (DigestUrl, error) {
	p, err := url.Parse(location)
	if err != nil {
		return DigestUrl{}, err
	}

	return NewUrlBuilderFromNet(*p).
		SetHost(host.String()).
		DigestUrl()
}

type DigestUrl struct {
	u url.URL
}


// Host returns the hostname of the url as DigestHost struct
func (d *DigestUrl) Host() DigestHost {
	return HostFromString(d.u.Hostname())
}

// Hostname returns hostname of the url as string
func (d *DigestUrl) Hostname() string {
	return d.u.Hostname()
}

// ReverseHostname returns the hostname as reverse domain name notation
func (d *DigestUrl) ReverseHostname() string {
	h := d.Host()
	return h.Reverse()
}

// String returns complete url as string
// String implements fmt.Stringer interface
func (d *DigestUrl) String() string {
	return d.u.String()
}

// Scheme retruns scheme as string
func (d *DigestUrl) Scheme() string {
	return d.u.Scheme
}

// Path returns url path as string
func (d *DigestUrl) Path() string {
	return d.u.Path
}

// RawQuery returns url query as string
func (d *DigestUrl) RawQuery() string {
	return d.u.RawQuery
}

// QueryParams returns a list of all query parameters
func (d *DigestUrl) QueryParams() []QueryParameter {
	params := []QueryParameter{}

	for _, q := range strings.Split(d.RawQuery(), "&") {
		s := strings.Split(q, "=")	
		if len(s) > 1 {
			params = append(params, QueryParameter{Key: s[0], Value: s[1]})	
		} else {
			params = append(params, QueryParameter{Key: s[0], Value: ""})	
		}
	}
	
	return params
}

// GetQueryParam returns the decoded value of the query key
func (d *DigestUrl) GetQueryParam(key string) (string, bool) {
	for _, q := range d.QueryParams() {
		if q.Key == key {
			return q.Value, true	
		}
	}

	return "", false
}

// Fragment returns fragment as string
func (d *DigestUrl) Fragment() string {
	return d.u.Fragment
}

// Hash returns sha1 hash as string
// Sort of fingerprint
func (d *DigestUrl) Hash() string {
	h := sha1.New()
	h.Write([]byte(d.String()))
	return hex.EncodeToString(h.Sum(nil))
}

// HostHash returns sha1 of hostname as string
// Sort of host fingerprint
func (d *DigestUrl) HostHash() string {
	h := d.Host()
	return h.Hash()
}

// Equals compares two complete urls.
func (d *DigestUrl) Equals(cmp DigestUrl) bool {
	return d.String() == cmp.String()
}

// HostEquals compares the hostnames of two urls
// Usefull if comparing if urls share the same host
func (d *DigestUrl) HostEquals(cmp DigestUrl) bool {
	h := d.Host()
	return h.Equals(d.Host())
}

// MarshalJSON implementation for JSON Marshaler interface
// direct receiver allows Marshal to work on values and pointers
func (d DigestUrl) MarshalJSON() ([]byte, error) {
	s := d.String()
	return json.Marshal(s)
}

// UnmarshalJSON implementation for JSON Unmarshaler interface
func (d *DigestUrl) UnmarshalJSON(data []byte) error {
	s := string(data)

	s = strings.TrimPrefix(s, "\"")
	s = strings.TrimSuffix(s, "\"")

	p, err := url.Parse(s)
	if err != nil {
		return err
	}
	d.u = *p
	return nil
}

// Normalize performs a minimal general normalization of the url
// Steps include:
// * Remove fragment
// * Remove forced query quotation mark
// * Sort query parameter alphabetically
// For sophisticated normalization this method will not be sufficiant
func (d *DigestUrl) Normalize() (DigestUrl, error) {
	b := NewUrlBuilderFromDigest(*d).
		SetFragment("")

	if d.RawQuery() != "" {
		b.SetQuery(sortQueryParameter(d.RawQuery()))
	} else {
		b.DisableForceQuery()
	}

	return b.DigestUrl()
}


// Surt outputs the URL as a SURT string
// For further information search for "Sort-friendly URI Reordering Transform"
func (d *DigestUrl) Surt() string {
	surt := ""

	if d.HasHostname() {
		h := d.Host()
		surt = fmt.Sprintf("(%v,)", h.ReverseDelimiter(","))
	}

	if d.Path() != "" {
		surt = surt + d.Path()
	}

	if d.RawQuery() != "" {
		surt = surt + "?" + d.RawQuery()
	}
	
	if d.Scheme() == "https" {
		surt = "http://" + surt
	} else if d.Scheme() != "" {
		surt = d.Scheme() + "://" + surt
	}

	return strings.ToLower(surt)
}

func (d *DigestUrl) HasHostname() bool {
	return d.Hostname() != ""
}

// FromRelative can be used to create a full Digesturl with the current struct as reference.
// For example:
// (www.example.org/foo/bar).FromRelative(/baz) => www.example.org/baz
// Does not work if ENFORCE_FULL_URL is true!
func (d *DigestUrl) FromRelative(input DigestUrl) (DigestUrl, error) {
	if input.String() == "" {
		return DigestUrl{}, fmt.Errorf("Error concatonating Urls: Input Url empty")
	}

	// If path is relative, add host
	if !input.HasHostname() {
		// If only fragment supplied, url keeps basicly unchanged
		if strings.HasPrefix(input.String(), "#") {
			return NewUrlBuilder().
				SetScheme(d.Scheme()).
				SetHost(d.Hostname()).
				SetPath(d.Path()).
				SetQuery(d.RawQuery()).
				SetFragment(input.Fragment()).
			        DigestUrl()
		}

		if strings.HasPrefix(input.Path(), "/") {
			return NewUrlBuilder().
				SetScheme(d.Scheme()).
				SetHost(d.Hostname()).
				SetPath(input.Path()).
				SetQuery(input.RawQuery()).
				SetFragment(input.Fragment()).
			        DigestUrl()

		}

		return NewUrlBuilder().
			SetScheme(d.Scheme()).
			SetHost(d.Hostname()).
			SetPath(path.Join(d.Path(), input.Path())).
			SetQuery(input.RawQuery()).
			SetFragment(input.Fragment()).
			DigestUrl()
	}

	return input, nil
}

func sortQueryParameter(q string) string {
	s := strings.Split(q, "&")
	sort.Strings(s)
	return strings.Join(s, "&")
}
