package web

import (
	"strings"
	"net/url"
)

type UrlBuilder struct {
	u url.URL	
}

func NewUrlBuilder() *UrlBuilder {
	return &UrlBuilder{}
}

func NewUrlBuilderFromDigest(d DigestUrl) *UrlBuilder {
	return &UrlBuilder{
		u: d.u,
	}
}

func NewUrlBuilderFromNet(u url.URL) *UrlBuilder {
	return &UrlBuilder{
		u: u,
	}
}

func (b *UrlBuilder) SetScheme(scheme string) *UrlBuilder {
	b.u.Scheme = scheme
	return b
}

func (b *UrlBuilder) SetHost(host string) *UrlBuilder {
	b.u.Host = host
	return b
}

func (b *UrlBuilder) SetPath(path string) *UrlBuilder {
	b.u.Path = path
	return b
}

func (b *UrlBuilder) SetQuery(query string) *UrlBuilder {
	b.u.RawQuery = query
	return b
}

func (b *UrlBuilder) EnableForceQuery() *UrlBuilder {
	b.u.ForceQuery = true
	return b
}

func (b *UrlBuilder) DisableForceQuery() *UrlBuilder {
	b.u.ForceQuery = false
	return b
}

func (b *UrlBuilder) AddQueryParameter(key, value string) *UrlBuilder {
	if b.u.RawQuery == "" {
		b.u.RawQuery = key + "=" + value
	} else {
		b.u.RawQuery = b.u.RawQuery + "&" + key + "=" + value
	}
	return b
}

func (b *UrlBuilder) RemoveQueryParmater(key string) *UrlBuilder {
	if b.u.RawQuery != "" {
		var f []string
		for _, q := range strings.Split(b.u.RawQuery, "&") {
			s := strings.Split(q, "=")	
			if s[0] != key {
				f = append(f, q)
			}
		}

		b.u.RawQuery = strings.Join(f, "&")
	}	

	return b
}

func (b *UrlBuilder) SetFragment(fragment string) *UrlBuilder {
	f := strings.TrimPrefix(fragment, "#")
	b.u.Fragment = f
	return b
}

func (b *UrlBuilder) String() (string, error) {
	if !AssertEnforceUrl(&b.u) {
		return "", AssertEnforceError()
	}

	return b.u.String(), nil
}

func (b *UrlBuilder) DigestUrl() (DigestUrl, error) {
	if !AssertEnforceUrl(&b.u) {
		return DigestUrl{}, AssertEnforceError()
	}

	return DigestUrl{
		u: b.u,
	}, nil
}
