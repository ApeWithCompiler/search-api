package web

type Link interface {
	Location() string
}

func PromoteLink(ref DigestUrl, link Link) (DigestUrl, error) {
	return UrlFromLocation(ref.Host(), link.Location())
}

func PromoteLinkHost(ref DigestHost, link Link) (DigestUrl, error) {
	return UrlFromLocation(ref, link.Location())
}
