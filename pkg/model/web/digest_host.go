package web

import (
	"strings"
	"slices"

	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
)

type DigestHost struct {
	raw string
}

func HostFromString(input string) DigestHost {
	return DigestHost{
		raw: input,
	}
}

// String returns host as string
// String implements fmt.Stringer interface
func (d *DigestHost) String() string {
	return d.raw
}

// Reverse returns hostname as revers domain name notation
func (d *DigestHost) Reverse() string {
	return d.ReverseDelimiter(".")
}

// ReverseDelimiter returns hostname as revers domain name notation, lets specify the delimiter
func (d *DigestHost) ReverseDelimiter(delim string) string {
	s := strings.Split(d.raw, ".")
	slices.Reverse(s)
	return strings.Join(s, delim)
}

// Tld returns top level domain as string
func (d *DigestHost) Tld() string {
	s := strings.Split(d.raw, ".")

	if len(s) > 0 {
		return s[len(s)-1]
	}

	return ""
}

// Domain returns domain incl top level domain as string
// Returns only tld if no domain present
func (d *DigestHost) Domain() string {
	s := strings.Split(d.raw, ".")

	if len(s) > 1 {
		return strings.Join([]string{s[len(s)-2], s[len(s)-1]}, ".")
	}

	if len(s) > 0 {
		return s[len(s)-1]
	}

	return ""
}

// IsSubDomainOf checks if this host is a subdomain of reference
// host.foo.bar IsSubDomainOf foo.bar => true
// host.foo IsSubDomainOf foo.bar => true
// host.foo IsSubDomainOf baz.foo.bar => false
func (d *DigestHost) IsSubDomainOf(cmp DigestHost) bool {
	a := strings.Split(d.String(), ".")
	slices.Reverse(a)

	b := strings.Split(cmp.String(), ".")
	slices.Reverse(b)

	if len(a) < len(b) {
		return false
	}

	for i := range b {
		if b[i] != a[i] {
			return false
		}
	}

	return true
}

// Hash returns sha1 hash as string
// Sort of fingerprint
func (d *DigestHost) Hash() string {
	h := sha1.New()
	h.Write([]byte(d.String()))
	return hex.EncodeToString(h.Sum(nil))
}

// Equals compares two hosts.
func (d *DigestHost) Equals(cmp DigestHost) bool {
	return d.String() == cmp.String()
}
// MarshalJSON implementation for JSON Marshaler interface
// direct receiver allows Marshal to work on values and pointers
func (d DigestHost) MarshalJSON() ([]byte, error) {
	s := d.String()
	return json.Marshal(s)
}

// UnmarshalJSON implementation for JSON Unmarshaler interface
func (d *DigestHost) UnmarshalJSON(data []byte) error {
	s := string(data)

	s = strings.TrimPrefix(s, "\"")
	s = strings.TrimSuffix(s, "\"")

	d.raw = s
	return nil
}
