package web

import (
	"testing"
)

func TestUrlBuilder(t *testing.T) {
	want := "http://example.com/foo?bar=&baz=b"
	got, _ := NewUrlBuilder().
		SetScheme("http").
		SetHost("example.com").
		SetPath("/foo").
		AddQueryParameter("bar", "").
		AddQueryParameter("baz", "b").
		String()

	if got != want {
		t.Errorf("Urlbuilder.String() returned %v, want %v", got, want)
	}
}

func TestRemoveQuryParameter(t *testing.T) {
	src := "http://example.com/foo?bar=&baz=b&remove=this"
	want := "http://example.com/foo?bar=&baz=b"

	u, _ := UrlFromString(src)
	got, _ := NewUrlBuilderFromDigest(u).
		RemoveQueryParmater("remove").
		String()

	if got != want {
		t.Errorf("RemoveQueryParameter() returned %v, want %v", got, want)
	}
}
