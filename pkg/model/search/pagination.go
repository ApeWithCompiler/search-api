package search

import ()

type Pagination struct {
	Next int `json:"next"`
	Current int `json:"current"`
	Previous int `json:"previous"`
	Window []int `json:"window"`
	Max int `json:"max"`
}
