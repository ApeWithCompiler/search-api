package search

import (
	giql "gitlab.com/ApeWithCompiler/giql"
)

// Query describes the search query a user entered
type Query struct {
	Raw         string
	Expressions []giql.Expr
}
