package search

import (
	"time"
)

// ResultDocument is mostly derived from Document model.
// Additionally it has a "score" property to indicate the match
// to the query
type ResultDocument struct {
	// Id provides idendifier for the result
	Id string `jsonapi:"primary,searchresult"`
	// Source may be used to identify where the result was retrived from
	Source string `jsonapi:"attr,source" json:"source"`
	// Score is the the score of the result
	Score float64 `jsonapi:"attr,score"`
	// Title is the title of the result
	Title string `jsonapi:"attr,title"`
	// HtmlTitle is the title, but HTML formatted
	HtmlTitle string `jsonapi:"attr,htmlTitle"`
	// Link is the complete link to the result
	Link string `jsonapi:"attr,link"`
	// DisplayLink is the shortened link to the result
	DisplayLink string `jsonapi:"attr,displayLink"`
	// Snippet is the short text descriping the result
	Snippet string `jsonapi:"attr,snippet"`
	// HtmlSnippet is the short text descriping the result, bur HTML formatted
	HtmlSnippet string `jsonapi:"attr,htmlSnippet"`
	// FormattedUrl is the url displayed after every result
	FormattedUrl string `jsonapi:"attr,formattedUrl"`
	// HtmlFormattedUrl is the url displayed after every result, bur HTML formatted
	HtmlFormattedUrl string `jsonapi:"attr,htmlFormattedUrl"`
	// ThumbnailUrl provides a URL to a thumbnail decorating the result
	ThumbnailUrl string `jsonapi:"attr,thumbnailUrl"`
	// FaviconUrl provides a URL to a favicon for the site
	FaviconUrl string `jsonapi:"attr,faviconlUrl"`
	// Mime is the MIME type of the result
	Mime string `jsonapi:"attr,mime"`
	// FileFormat is the file format of the result
	FileFormat string `jsonapi:"attr,fileFormat"`
	// Labels for additional information
	Labels []string `jsonapi:"attr,labels"`
	// Updated indicated when the result was last modified
	Updated time.Time `jsonapi:"attr,updated" json:"updated"`
}

type Result struct {
	// Documents returned in this result
	Documents []ResultDocument
	// MaxCount stores the count of all documents matching the query
	// Documents counted don't have to be included in this result
	MaxCount int
}

// GetMaxCount returns the max count of Documents
// However, if the field is not set, it is assumed that all Documents
// in this result are the maximum
func (r *Result) GetMaxCount() int {
	if r.MaxCount == 0 {
		return len(r.Documents)
	}

	return r.MaxCount
}
