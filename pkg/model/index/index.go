package index

import (
	"fmt"
	"github.com/google/jsonapi"
)

type Index struct {
	Name     string `jsonapi:"primary,index" json:"name"`
	Provider string `jsonapi:"attr,provider" json:"provider"`
}

func (i Index) JSONAPILinks() *jsonapi.Links {
	return &jsonapi.Links{
		"self": fmt.Sprintf("/api/indexes/%v", i.Name),
	}
}
