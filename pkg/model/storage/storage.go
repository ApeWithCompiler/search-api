package storage

import (
	"fmt"
)

type ObjectKey interface {
	fmt.Stringer
}
