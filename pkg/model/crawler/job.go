package crawler

const (
	FILTERMODE_ALLOW string = "allow"
	FILTERMODE_DENY         = "deny"
	FILTERMODE_SAME_HOST    = "same-host"
	FILTERMODE_NONE         = ""
)

type CrawlerJob struct {
	Id           string   `jsonapi:"primary,crawlerjob"`
	Links        []string `jsonapi:"attr,links"`
	FilterMode   string   `jsonapi:"attr,filter_mode"`
	Filter       []string `jsonapi:"attr,filter"`
	IgnoreRobots bool     `jsonapi:"attr,ignoreRobots"`
	Depth        int      `jsonapi:"attr,depth"`
}
