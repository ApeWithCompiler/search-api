package document

func DocumentDataFromBytes(b []byte) DocumentData {
	return DocumentData {
		b: b,
	}
}

type DocumentData struct {
	b []byte

	head DocumentHead
}

func (d *DocumentData) AddHead(head DocumentHead) {
	d.head = head
}

func (d *DocumentData) GetHead() DocumentHead{
	return d.head
}

func (d *DocumentData) Bytes() []byte {
	return d.b
}
