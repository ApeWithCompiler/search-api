package document

import (
	"time"
)

// Document describes the core element search is managing
// All properties asociated with a element added to an index is
// defined here.
type DocumentResult struct {
	// Id provides idendifier for the document
	Id string `jsonapi:"primary,document" json:"id"`
	// Source may be used to identify where the document was retrived from
	Source string `jsonapi:"attr,source" json:"source"`
	// Title is the title of the document
	Title string `jsonapi:"attr,title" json:"title"`
	// HtmlTitle is the title, but HTML formatted
	HtmlTitle string `jsonapi:"attr,htmlTitle" json:"htmlTitle"`
	// Link is the complete link to the document
	Link string `jsonapi:"attr,link" json:"link"`
	// DisplayLink is the shortened link to the document
	DisplayLink string `jsonapi:"attr,displayLink" json:"displayLink"`
	// Snippet is the short text descriping the document
	Snippet string `jsonapi:"attr,snippet" json:"snippet"`
	// HtmlSnippet is the short text descriping the document, bur HTML formatted
	HtmlSnippet string `jsonapi:"attr,htmlSnippet" json:"htmlSnippet"`
	// FormattedUrl is the url displayed after every document
	FormattedUrl string `jsonapi:"attr,formattedUrl" json:"formattedUrl"`
	// HtmlFormattedUrl is the url displayed after every document, bur HTML formatted
	HtmlFormattedUrl string `jsonapi:"attr,htmlFormattedUrl" json:"htmlFormattedUrl"`
	// ThumbnailUrl provides a URL to a thumbnail decorating the result
	ThumbnailUrl string `jsonapi:"attr,thumbnailUrl" json:"thumbnailUrl"`
	// FaviconUrl provides a URL to a favicon for the site
	FaviconUrl string `jsonapi:"attr,faviconlUrl" json:"faviconUrl"`
	// Mime is the MIME type of the document
	Mime string `jsonapi:"attr,mime" json:"mime"`
	// FileFormat is the file format of the document
	FileFormat string `jsonapi:"attr,fileFormat" json:"fileFormat"`
	// Labels for additional information
	Labels []string `jsonapi:"attr,labels" json:"labels"`
	// Updated indicated when the document was last modified
	Updated time.Time `jsonapi:"attr,updated" json:"updated"`
}
