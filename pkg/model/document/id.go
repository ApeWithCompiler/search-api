package document

import () 

type DocumentId interface {
	String() string
	Version() int8
}

type PrimitiveId string

func (p PrimitiveId) String() string{
	return string(p)
}

func (p PrimitiveId) Version() int8{
	return 0
}
