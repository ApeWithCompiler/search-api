package document

import (
	"encoding/json"
	"time"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/resolver"
)

type DocumentMetadata struct {
	Id         DocumentId       `json:"id"`
	RawId      string	    `jsonapi:"primary,metadata" json:"ri,omitempty"`
	Url        web.DigestUrl `json:"url"`
	// jsonapi only parses primitives, even with custom marshaller
	// so work arround by seting it as a string
	RawUrl     string	    `jsonapi:"attr,url" json:"ru,omitempty"`
	StorageRef string           `jsonapi:"attr,storage_ref" json:"storage_ref"`
	Created    time.Time        `jsonapi:"attr,created" json:"created"`
}

// MarshalJSON implementation for JSON Marshaler interface
func (d *DocumentMetadata) MarshalJSON() ([]byte, error) {
	return json.Marshal(d)
}

// UnmarshalJSON implementation for JSON Unmarshaler interface
func (d *DocumentMetadata) UnmarshalJSON(data []byte) error {
	type Base DocumentMetadata
	aux := &struct {
		Id string `json:"id"`
		*Base
	}{
		Base: (*Base)(d),
	}
	
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	id, err := resolver.NewDocumentIdV2FromString(aux.Id)
	if err != nil {
		return err
	}
	d.Id = &id

	return nil
}
