package document

import ()

type DocumentHead struct {
	Id           string   `jsonapi:"primary,head" json:"id"`
	Charset      string   `jsonapi:"attr,charset" yaml:"charset" json:"charset"`
	CanonicalUrl string   `jsonapi:"attr,canonical" yaml:"canonical" json:"canonical"`
	MimeType     string   `jsonapi:"attr,mimetype" yaml:"mimetype" json:"mimetype"`
	Languages    []string `jsonapi:"attr,languages" yaml:"languages" json:"languages"`
	Titles       []string `jsonapi:"attr,titles" yaml:"titles" json:"titles"`
	Descriptions []string `jsonapi:"attr,descriptions" yaml:"descriptions" json:"descriptions"`
	Keywords     []string `jsonapi:"attr,keywords" yaml:"keywords" json:"keywords"`
	Favicon      string   `jsonapi:"attr,favicon" yaml:"favicon" json:"favicon"`
	Creator      string   `jsonapi:"attr,creator" yaml:"creator" json:"creator"`
}

