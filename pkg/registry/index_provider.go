package registry

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

// IndexRegistryEntry is a indermediate container for both types
// of index factories. Allowes to handle both providers as a single
// unit identified by a single name in a single hashmap, without type inflection.
type IndexRegistryEntry struct {
	Reader provider.IndexReaderFactory
	Writer provider.IndexWriterFactory
}

// IndexRegistry is a key value cataloqe of available index providers
// Adds symantic sugar arround existing hashmap structure
type IndexRegistry struct {
	IndexFactories map[string]IndexRegistryEntry
}

func NewIndexRegistry() IndexRegistry {
	reg := IndexRegistry{}
	reg.IndexFactories = make(map[string]IndexRegistryEntry)

	return reg
}

func (r *IndexRegistry) RegisterFactory(name string, factory IndexRegistryEntry) {
	log.WithFields(log.Fields{"module": "registry", "provider": name}).Info("Provider registered")
	r.IndexFactories[name] = factory
}

func (r *IndexRegistry) FactoryIsRegistered(name string) bool {
	_, ok := r.IndexFactories[name]
	return ok
}

func (r *IndexRegistry) GetFactory(name string) (IndexRegistryEntry, error) {
	log.WithFields(log.Fields{"module": "registry", "provider": name}).Debug("Provider requested")
	factory, ok := r.IndexFactories[name]
	if !ok {
		return IndexRegistryEntry{}, fmt.Errorf("Factory not registered: %v", name)
	}
	return factory, nil
}
