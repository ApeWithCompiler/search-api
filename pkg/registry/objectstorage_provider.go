package registry

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

// ObjectStorageRegistry is a key value cataloqe of available repository providers
// Adds symantic sugar arround existing hashmap structure
type ObjectStorageRegistry struct {
	ObjectStorageFactories map[string]provider.ObjectStorageFactory
}

func NewObjectStorageRegistry() ObjectStorageRegistry {
	reg := ObjectStorageRegistry{}
	reg.ObjectStorageFactories = make(map[string]provider.ObjectStorageFactory)

	return reg
}

func (r *ObjectStorageRegistry) RegisterFactory(name string, factory provider.ObjectStorageFactory) {
	log.WithFields(log.Fields{"module": "registry", "provider": name}).Info("Provider registered")
	r.ObjectStorageFactories[name] = factory
}

func (r *ObjectStorageRegistry) FactoryIsRegistered(name string) bool {
	_, ok := r.ObjectStorageFactories[name]
	return ok
}

func (r *ObjectStorageRegistry) GetFactory(name string) (*provider.ObjectStorageFactory, error) {
	log.WithFields(log.Fields{"module": "registry", "provider": name}).Debug("Provider requested")
	factory, ok := r.ObjectStorageFactories[name]
	if !ok {
		return nil, fmt.Errorf("Factory not registered: %v", name)
	}
	return &factory, nil
}
