package requestid

import (
	"context"
	"github.com/google/uuid"
)

type ContextKey string

const RequestID string = "requestId"

func NewToContext(ctx context.Context) context.Context {
	id := uuid.New()
	return context.WithValue(ctx, ContextKey(RequestID), id.String())
}

func FromContext(ctx context.Context) string {
	reqId := ctx.Value(ContextKey(RequestID))
	return reqId.(string)
}
