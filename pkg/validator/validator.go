package validator

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/registry"
)

type IndexConfigValidator struct {
	Config             config.Config
	IndexProvider      registry.IndexRegistry
}

func NewIndexConfigValidator(prov registry.IndexRegistry, cfg config.Config) IndexConfigValidator {
	return IndexConfigValidator{
		Config: cfg,
		IndexProvider: prov,
	}
}

func (v *IndexConfigValidator) Validate() bool {
	log.WithFields(log.Fields{"module": "validator"}).Info("Validating indexes")
	for _, index := range v.Config.GetAllIndexConfigs() {
		log.WithFields(log.Fields{"module": "validator"}).Debug("Validating: ", index.Name)

		providerExists := v.IndexProvider.FactoryIsRegistered(index.Provider)
		if !providerExists {
			log.WithFields(log.Fields{"module": "validator"}).Error("Provider not registered: ", index.Provider)
			return false
		}
	}
	return true
}
