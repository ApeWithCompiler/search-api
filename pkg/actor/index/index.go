package index

import (
	"github.com/prometheus/client_golang/prometheus"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/actor"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

// IndexMessage describes a interface alowing different message types accepted
// by the IndexUpdateActor.
type IndexMessage interface {
	visit(a *IndexUpdateActor)
}

func NewIndexUpdateActor(name string, provider provider.IndexWriter, mailboxSize int) IndexUpdateActor {
	c := make(chan IndexMessage, mailboxSize)
	logger := log.WithFields(log.Fields{"module": "actor", "index": name})

	return IndexUpdateActor{
		mailbox:     c,
		mailboxSize: mailboxSize,
		name:        name,
		provider:    provider,
		logger:      *logger,
	}
}

// IndexUpdateActor implements a actor responsible for updating indexes
// This means updating or deleting results in a index
type IndexUpdateActor struct {
	mailbox     chan IndexMessage
	mailboxSize int

	name     string
	provider provider.IndexWriter

	logger log.Entry
}

func (a *IndexUpdateActor) MailboxQueued() int {
	return len(a.mailbox)
}

func (a *IndexUpdateActor) MailboxSize() int {
	return a.mailboxSize
}

// SendToMailbox meant for external clients to add this actor a new message
func (a *IndexUpdateActor) SendToMailbox(msg IndexMessage) {
	a.logger.Debug("Received message")
	a.mailbox <- msg
}

// DoWork starts a infinite loop executing incoming messages
func (a *IndexUpdateActor) DoWork() {

	a.logger.Info("Start handler")
	defer a.logger.Info("Stop handler")

	for msg := range a.mailbox {
		a.logger.Debug("Executing message")
		msg.visit(a)
		actor.EventsProcessedCount.With(prometheus.Labels{"name": a.name, "type": "indexwriter"}).Inc()
	}
}

// handleUpdate implements the operation for handeling update messages
func (a *IndexUpdateActor) handleUpdate(msg *IndexUpdateMessage) {
	a.logger.Debug("Updating result: ", msg.ResultId.String())

	err := a.provider.SetDocument(msg.ResultId, msg.Record)
	if err != nil {
		a.logger.Error("Updating result returned err: ", err)
	}
}

// handleDelete implements the operation for handeling delete messages
func (a *IndexUpdateActor) handleDelete(msg *IndexDeleteMessage) {
	a.logger.Debug("Deleting result: ", msg.ResultId.String())
	err := a.provider.DeleteDocument(msg.ResultId)
	if err != nil {
		a.logger.Error("Deleting result returned err: ", err)
	}
}
