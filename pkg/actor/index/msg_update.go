package index

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewIndexUpdateMessage(resultId document.DocumentId, record document.DocumentResult) IndexUpdateMessage {
	return IndexUpdateMessage{
		ResultId:   resultId,
		Record:     record,
	}
}

type IndexUpdateMessage struct {
	ResultId   document.DocumentId
	Record     document.DocumentResult
}

func (m *IndexUpdateMessage) visit(a *IndexUpdateActor) {
	a.handleUpdate(m)
}
