package index

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewIndexDeleteMessage(resultId document.DocumentId) IndexDeleteMessage {
	return IndexDeleteMessage{
		ResultId: resultId,
	}
}

type IndexDeleteMessage struct {
	ResultId document.DocumentId
}

func (m *IndexDeleteMessage) visit(a *IndexUpdateActor) {
	a.handleDelete(m)
}
