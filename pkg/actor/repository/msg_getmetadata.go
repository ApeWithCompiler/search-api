package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewGetMetadataMessage(documentId document.DocumentId) GetMetadataMessage {
	c := make(chan document.DocumentMetadata, 1)
	e := make(chan error, 1)

	return GetMetadataMessage{
		DocumentId: documentId,
		result:     c,
		error:      e,
	}
}

type GetMetadataMessage struct {
	DocumentId document.DocumentId

	result chan document.DocumentMetadata
	error  chan error
}

// AwaitResult comfort method for clients to block until results are delivered
func (m *GetMetadataMessage) AwaitResult() (document.DocumentMetadata, error) {
	select {
	case res := <-m.result:
		return res, nil
	case err := <-m.error:
		return document.DocumentMetadata{}, err
	}
}

// DeliverResult comfort method for actor to deliver results
func (m *GetMetadataMessage) DeliverResult(result document.DocumentMetadata) {
	m.result <- result
}

// DeliverError comfort method for actor to indicate error handeling this message
func (m *GetMetadataMessage) DeliverError(err error) {
	m.error <- err
}

func (m *GetMetadataMessage) visit(a *RepositoryActor) {
	a.handleGetMetadata(m)
}
