package repository

import (
	//	"context"
	"fmt"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	log "github.com/sirupsen/logrus"

	omu "gitlab.com/ApeWithCompiler/omufsgenerator"

	"gitlab.com/apewithcompiler/heru/pkg/actor"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
	"gitlab.com/apewithcompiler/heru/pkg/actor/repository/storage"
)

type MetadataKey string

func (k MetadataKey) String() string {
	s := strings.Replace(string(k), "-", "/", -1)
	return fmt.Sprintf("/meta/%v/info.json", s)
}

type DataKey string

func (k DataKey) String() string {
	return string(k)
}

type RepositoryMessage interface {
	visit(a *RepositoryActor)
}

func NewRepositoryActor(name string, provider provider.ObjectStorage, mailboxSize int) RepositoryActor {
	c := make(chan RepositoryMessage, mailboxSize)
	logger := log.WithFields(log.Fields{"module": "actor", "repository": name})

	gen := omu.NewGeneratorBuilder().
	   SetGenerator(omu.NewSha1Generator()).
	   SetSplitter(omu.NewStaticSegmentSplitter(4,2)).
	   AddAppender(omu.NewAppender(omu.APPENDER_PREFIX, omu.NewStaticAppenderValue("data/"))).
	   Build()

	return RepositoryActor{
		mailbox:     c,
		mailboxSize: mailboxSize,
		name:        name,
		datakeyGen:  &gen,
		pageStorage: storage.NewGzipPageObjectStorage(name, provider),
		metadataStorage: storage.NewJsonMetadataObjectStorage(name, provider),
		logger:      *logger,
	}
}

type RepositoryActor struct {
	mailbox     chan RepositoryMessage
	mailboxSize int

	name     string
	datakeyGen omu.Generator
	pageStorage storage.GzipPageObjectStorage
	metadataStorage storage.JsonMetadataObjectStorage

	logger log.Entry
}

func (a *RepositoryActor) MailboxQueued() int {
	return len(a.mailbox)
}

func (a *RepositoryActor) MailboxSize() int {
	return a.mailboxSize
}

// SendToMailbox meant for external clients to add this actor a new message
func (a *RepositoryActor) SendToMailbox(msg RepositoryMessage) {
	a.logger.Debug("Received message")
	a.mailbox <- msg
}

// DoWork starts a infinite loop executing incoming messages
func (a *RepositoryActor) DoWork() {
	a.logger.Info("Start handler")
	defer a.logger.Info("Stop handler")

	for msg := range a.mailbox {
		a.logger.Debug("Executing message")
		msg.visit(a)
		actor.EventsProcessedCount.With(prometheus.Labels{"name": a.name, "type": "repository"}).Inc()
	}
}

func (a *RepositoryActor) handleGetMetadata(msg *GetMetadataMessage) {
	key := MetadataKey(msg.DocumentId.String())

	metadata, err := a.metadataStorage.GetMetadata(key)
	if err != nil {
		a.logger.Error("Error handling get metadata: ", err)
		msg.DeliverError(err)
		return
	}

	msg.DeliverResult(metadata)
}

func (a *RepositoryActor) handleGetData(msg *GetDataMessage) {
	key := MetadataKey(msg.DocumentId.String())

	metadata, err := a.metadataStorage.GetMetadata(key)
	if err != nil {
		a.logger.Error("Error handling get metadata: ", err)
		msg.DeliverError(err)
		return
	}
	
	dataKey := DataKey(metadata.StorageRef)
	data, err := a.pageStorage.GetPageObject(dataKey)
	if err != nil {
		a.logger.Error("Error hadnling get data: ", err)
		msg.DeliverError(err)
	}

	msg.DeliverResult(data)
}

func (a *RepositoryActor) handleSetMetadata(msg *SetMetadataMessage) {
	key := MetadataKey(msg.DocumentId.String())

	err := a.metadataStorage.SetMetadata(key, msg.Metadata)
	if err != nil {
		a.logger.Error("Error handling set metadata: ", err)
	}
}


func (a *RepositoryActor) handleSetData(msg *SetDataMessage) {
	metadataKey := MetadataKey(msg.DocumentId.String())

	var metadata document.DocumentMetadata

	exists, err := a.metadataStorage.MetadataExists(metadataKey)
	if err != nil {
		a.logger.Error("Error stat metadata: ", err)
		return
	}
	if exists {
		a.logger.Debug("Inserting data, metadata allready exists for ", msg.DocumentId.String())

		metadata, err = a.metadataStorage.GetMetadata(metadataKey)
		if err != nil {
			a.logger.Error("Error reading metadata: ", err)
			return
		}

		dataKey := DataKey(metadata.StorageRef)
		err = a.pageStorage.SetPageObject(dataKey, msg.Data)
		if err != nil {
			a.logger.Error("Error handling set head: ", err)
			return
		}
	} else {
		a.logger.Debug("Inserting data, initializing new metadata for ", msg.DocumentId.String())

		k, err := a.datakeyGen.GenerateValue(msg.Url.String())
		if err != nil {
			a.logger.Error("Error generating data key: ", err)
			return
		}

		dataKey := DataKey(k)

		metadata = document.DocumentMetadata{
			Url: msg.Url,
			Id: msg.DocumentId,
			StorageRef: dataKey.String(),
			Created: time.Now(),
		}

		err = a.metadataStorage.SetMetadata(metadataKey, metadata)
		if err != nil {
			a.logger.Error("Error handling set data: ", err)
		}

		err = a.pageStorage.SetPageObject(dataKey, msg.Data)
		if err != nil {
			a.logger.Error("Error handling set data: ", err)
		}
	}
}

func (a *RepositoryActor) handleDeleteDocument(msg *DeleteDocumentMessage) {
	key := MetadataKey(msg.DocumentId.String())

	err := a.deleteDocument(key)
	if err != nil {
		a.logger.Error("Error handling delete document: ", err)
		return
	}
}

func (a *RepositoryActor) deleteDocument(key MetadataKey) error {
	metadata, err := a.metadataStorage.GetMetadata(key)
	if err != nil {
		a.logger.Error("Error deleting document, failed reading metadata: ", err)
		return err
	}

	datakey := DataKey(metadata.StorageRef)

	err = a.pageStorage.DeletePageObject(datakey)
	if err != nil {
		a.logger.Error("Error deleting document data: ", err)
		return err
	}

	err = a.metadataStorage.DeleteMetadata(key)
	if err != nil {
		a.logger.Error("Error deleting metadata: ", err)
		return err
	}

	return nil
}
