package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewSetDataMessage(id document.DocumentId, url web.DigestUrl, data document.DocumentData) SetDataMessage {
	return SetDataMessage{
		DocumentId: id,
		Url:        url,
		Data:       data,
	}
}

type SetDataMessage struct {
	DocumentId document.DocumentId
	Url        web.DigestUrl
	Data       document.DocumentData
}

func (m *SetDataMessage) visit(a *RepositoryActor) {
	a.handleSetData(m)
}
