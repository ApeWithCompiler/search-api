package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewDeleteDocumentMessage(id document.DocumentId) DeleteDocumentMessage {
	return DeleteDocumentMessage{
		DocumentId: id,
	}
}

type DeleteDocumentMessage struct {
	DocumentId document.DocumentId
}

func (m *DeleteDocumentMessage) visit(a *RepositoryActor) {
	a.handleDeleteDocument(m)
}
