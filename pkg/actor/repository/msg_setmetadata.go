package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewSetMetadataMessage(documentId document.DocumentId, metadata document.DocumentMetadata) SetMetadataMessage {
	return SetMetadataMessage{
		DocumentId: documentId,
		Metadata:   metadata,
	}
}

type SetMetadataMessage struct {
	DocumentId document.DocumentId
	Metadata   document.DocumentMetadata
}

func (m *SetMetadataMessage) visit(a *RepositoryActor) {
	a.handleSetMetadata(m)
}
