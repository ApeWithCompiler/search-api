package storage

import (
	"bytes"
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type JsonMetadataObjectStorage struct {
	provider provider.ObjectStorage
	logger log.Entry
}

func NewJsonMetadataObjectStorage(name string, provider provider.ObjectStorage) JsonMetadataObjectStorage {
	logger := log.WithFields(log.Fields{"module": "metadatastorage", "repository": name})

	return JsonMetadataObjectStorage{
		provider: provider,
		logger: *logger,
	}
}

func (s *JsonMetadataObjectStorage) GetMetadata(key fmt.Stringer) (document.DocumentMetadata, error) {
	metadata := document.DocumentMetadata{}

	reader, err := s.provider.GetObject(key)

	if err != nil {
		s.logger.Error("Cant get document metadata: ", err)
		return metadata, err
	}

	buff := new(bytes.Buffer)
	_, err = buff.ReadFrom(reader)
	if err != nil {
		s.logger.Error("Error reading buffer: ", err)
		return metadata, err
	}

	err = json.Unmarshal(buff.Bytes(), &metadata)
	if err != nil {
		s.logger.Error("Error while unmarshaling: ", err)
		return metadata, err
	}

	return metadata, nil
}

func (s *JsonMetadataObjectStorage) SetMetadata(key fmt.Stringer, metadata document.DocumentMetadata) error {
	out, err := json.Marshal(metadata)
	if err != nil {
		s.logger.Error("Error while marshaling: ", err)
		return err
	}

	err = s.provider.SetObject(key, bytes.NewReader(out), int64(len(out)))
	if err != nil {
		s.logger.Error("Cant set document: ", err)
		return err
	}

	s.logger.Debug("Inserted metadata: ", key.String())

	return nil
}

func (s *JsonMetadataObjectStorage) DeleteMetadata(key fmt.Stringer) error {
	err := s.provider.DeleteObject(key)

	if err != nil {
		s.logger.Error("Error deleting metadata: ", err)
	}

	return err
}

func (s *JsonMetadataObjectStorage) MetadataExists(key fmt.Stringer) (bool, error) {
	exists, err := s.provider.ObjectExists(key)
	if err != nil {
		s.logger.Error("Error stating metadata: ", err)
	}

	return exists, err
}
