package storage

import (
	"bytes"
	"compress/gzip"
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type GzipPageObjectStorage struct {
	provider provider.ObjectStorage
	logger log.Entry
}

func NewGzipPageObjectStorage(name string, provider provider.ObjectStorage) GzipPageObjectStorage {
	logger := log.WithFields(log.Fields{"module": "pageobjectstorage", "repository": name})

	return GzipPageObjectStorage{
		provider: provider,
		logger: *logger,
	}
}

func (s *GzipPageObjectStorage) GetPageObject(key fmt.Stringer) (document.DocumentData, error) {
	data, err := s.provider.GetObject(key)
	if err != nil {
		s.logger.Error("Error get data: ", err)
		return document.DocumentData{}, err
	}

	zr, err := gzip.NewReader(data)
	if err != nil {
		s.logger.Error("Failed creating compress reader: ", err)
	}

	buff := bytes.Buffer{}
	_, err = buff.ReadFrom(zr)
	if err != nil {
		s.logger.Error("Failed compress data read: ", err)
	}

	err = zr.Close()
	if err != nil {
		s.logger.Error("Faild compress data close: ", err)
	}

	return document.DocumentDataFromBytes(buff.Bytes()), nil
}

func (s *GzipPageObjectStorage) SetPageObject(key fmt.Stringer, data document.DocumentData) error {
	var buff bytes.Buffer

	zw := gzip.NewWriter(&buff)
	_, err := zw.Write(data.Bytes())
	if err != nil {
		s.logger.Error("Failed compress data write: ", err)
	}

	err = zw.Close()
	if err != nil {
		s.logger.Error("Faild compress data close: ", err)
	}

	l := int64(len(buff.Bytes()))
	r := bytes.NewReader(buff.Bytes())

	err = s.provider.SetObject(key, r, l)
	if err != nil {
		s.logger.Error("Cant set document: ", err)
		return err
	}

	s.logger.Debug("Inserted head: ", key.String())

	return nil
}

func (s *GzipPageObjectStorage) DeletePageObject(key fmt.Stringer) error {
	err := s.provider.DeleteObject(key)

	if err != nil {
		s.logger.Error("Error deleting page object: ", err)
	}

	return err
}
