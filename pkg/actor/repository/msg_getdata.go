package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewGetDataMessage(documentId document.DocumentId) GetDataMessage {
	c := make(chan document.DocumentData, 1)
	e := make(chan error, 1)

	return GetDataMessage{
		DocumentId: documentId,
		result:     c,
		error:      e,
	}
}

type GetDataMessage struct {
	DocumentId document.DocumentId

	result chan document.DocumentData
	error  chan error
}

// AwaitResult comfort method for clients to block until results are delivered
func (m *GetDataMessage) AwaitResult() (document.DocumentData, error) {
	select {
	case res := <-m.result:
		return res, nil
	case err := <-m.error:
		return document.DocumentData{}, err
	}
}

// DeliverResult comfort method for actor to deliver results
func (m *GetDataMessage) DeliverResult(result document.DocumentData) {
	m.result <- result
}

// DeliverError comfort method for actor to indicate error handeling this message
func (m *GetDataMessage) DeliverError(err error) {
	m.error <- err
}

func (m *GetDataMessage) visit(a *RepositoryActor) {
	a.handleGetData(m)
}
