package actor

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	EventsProcessedCount = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "search_actor_events_processed_count",
			Help: "Count of events a actor processed",
		},
		[]string{"name", "type"})
)
