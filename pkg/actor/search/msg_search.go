package search

import (
	"context"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

// NewSearchMessage returns a new SearchMessage
func NewSearchMessage(ctx context.Context, query string, count, offset int) SearchMessage {
	c := make(chan search.Result, 1)
	e := make(chan error, 1)

	return SearchMessage{
		result:  c,
		error:   e,
		Context: ctx,
		Query: search.Query{
			Raw: query,
		},
		Count: count,
		Offset: offset,
	}
}

// SearchMessage implements a request to a SearchActor
type SearchMessage struct {
	Context context.Context
	Query   search.Query
	Count   int
	Offset  int
	result  chan search.Result
	error   chan error
}

// AwaitResult comfort method for clients to block until results are delivered
func (m *SearchMessage) AwaitResult() (search.Result, error) {
	select {
	case res := <-m.result:
		return res, nil
	case err := <-m.error:
		return search.Result{}, err
	}
}

// DeliverResult comfort method for actor to deliver results
func (m *SearchMessage) DeliverResult(result search.Result) {
	m.result <- result
}

// DeliverError comfort method for actor to indicate error handeling this message
func (m *SearchMessage) DeliverError(err error) {
	m.error <- err
}
