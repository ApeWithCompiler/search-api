package search

import (
	"github.com/prometheus/client_golang/prometheus"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/actor"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/interpreter"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
	"gitlab.com/apewithcompiler/heru/pkg/transform"
)

// NewSearchActor returns a new Searchactor
func NewSearchActor(name string, indexReaderProvider provider.IndexReader, trans transform.ResultTransformer, mailboxSize int) SearchActor {
	c := make(chan SearchMessage, mailboxSize)
	logger := log.WithFields(log.Fields{"module": "actor", "index": name})

	return SearchActor{
		mailbox:     c,
		mailboxSize: mailboxSize,
		name:        name,
		provider:    indexReaderProvider,
		trans:       trans,
		logger:      *logger,
	}
}

// SearchActor implements a actor for searching in indices
type SearchActor struct {
	mailbox     chan SearchMessage
	mailboxSize int

	name     string
	provider provider.IndexReader

	trans    transform.ResultTransformer

	logger log.Entry
}

func (a *SearchActor) MailboxQueued() int {
	return len(a.mailbox)
}

func (a *SearchActor) MailboxSize() int {
	return a.mailboxSize
}

// SendToMailbox meant for external clients to add this actor a new message
func (a *SearchActor) SendToMailbox(msg SearchMessage) {
	a.logger.Debug("Received message")
	a.mailbox <- msg
}

// DoWork starts a infinite loop executing incoming messages
func (a *SearchActor) DoWork() {
	a.logger.Info("Start handler")
	defer a.logger.Info("Stop handler")

	for msg := range a.mailbox {
		a.logger.Debug("Executing message")
		a.handleSearch(msg)
		actor.EventsProcessedCount.With(prometheus.Labels{"name": a.name, "type": "indexreader"}).Inc()
	}
}

// handleSearch implements the operation for handeling search messages
func (a *SearchActor) handleSearch(msg SearchMessage) {
	query := interpreter.InterpretQuery(msg.Context, msg.Query.Raw)
	result := a.provider.Search(query, msg.Count, msg.Offset)
	result = a.transformResult(query, result)
	msg.DeliverResult(result)
}

func (a *SearchActor) transformResult(query search.Query, result search.Result) search.Result {
	for i := range result.Documents {
		result.Documents[i] = a.trans.TransformResult(query, result.Documents[i])
	}

	return result
}
