package text

import (
	"strings"
	"testing"
)

func TestReader(t *testing.T) {
	text := "Lorem ipsum\ndolor! sit amet, consectetur\" adipiscing elit,"
	
	want := []string{
		"Lorem",
		"ipsum",
		"dolor",
		"sit",
		"amet",
		"consectetur",
		"adipiscing",
		"elit",
	}

	sut := NewNaturalTextScanner(strings.NewReader(text))

	for _, c := range want {
		got, err := sut.NextToken()
		if err != nil {
			t.Errorf("NextToken() returned error %v", err.Error())
		}

		if got != c {
			t.Errorf("NextToken() returned %v, want %v", got, c)
		}
	}

}

