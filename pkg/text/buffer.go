package text

// RuneBuffer encapsulates logic to aggregate runes
// Example use case is to read each rune and combine to a string
type RuneBuffer struct {
	b []rune
}

func NewRuneBuffer() RuneBuffer {
	return RuneBuffer{
		b: []rune{},
	}
}

func (r *RuneBuffer) Clear() {
	r.b = []rune{}
}

func (r *RuneBuffer) Write(value rune) {
	r.b = append(r.b, value)
}

func (r *RuneBuffer) Read() []rune {
	return r.b
}

func (r *RuneBuffer) ReadPosition(p int) *rune {
	if p < r.Size() {
		return &r.b[p]
	}

	return nil
}

func (r *RuneBuffer) Size() int {
	return len(r.b)
}

func (r *RuneBuffer) String() string {
	return string(r.b)
}
