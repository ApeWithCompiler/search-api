package text

import (
	"gitlab.com/apewithcompiler/heru/pkg/set"
)

// Dictionary is a semantically specialized form of a set of strings
// It contains a set of terms
type Dictionary struct {
	terms set.StringSet
}

func NewDictionary(terms []string) Dictionary {
	return Dictionary{
		terms: set.NewStringSet(terms),
	}
}

func NewDictionaryFromSet(set set.StringSet) Dictionary {
	return Dictionary{
		terms: set,
	}
}

func (d *Dictionary) Has(term string) bool {
	return d.terms.Has(term)
}

// Contains to implement collection interface
func (d *Dictionary) Contains(e string) bool {
	return d.Has(e)
}

func (d *Dictionary) Terms() []string {
	return d.terms.Elements()
}

func (d *Dictionary) Size() int {
	return d.terms.Size()
}

func (d *Dictionary) Intersection(b set.Collection) set.StringSet {
	return d.terms.Intersection(b)
}

func (d *Dictionary) Difference(b set.Collection) set.StringSet {
	return d.terms.Difference(b)
}
