package text

import (
	"bufio"
	"io"
	"unicode"
)

func isAlphaNumeric(c rune) bool {
	return unicode.IsDigit(c) || unicode.IsLetter(c)
}


type NaturalTextScanner struct {
	reader bufio.Reader

	charBuf RuneBuffer
}


func NewNaturalTextScanner(r io.Reader) NaturalTextScanner {
	return NaturalTextScanner {
		reader: *bufio.NewReader(r),
		charBuf: NewRuneBuffer(),
	}
}

func (s *NaturalTextScanner) NextToken() (string, error) {
	s.charBuf.Clear()

	for !s.isAtEnd() {
		c, err := s.advance()
		if err == io.EOF {
			return "", err
		} else if err != nil {
			return "", err
		}

		if !isAlphaNumeric(c) {
			if s.charBuf.Size() == 0 {
				return s.NextToken()
			}

			return string(s.charBuf.Read()), nil
		}

		s.charBuf.Write(c)
	}

	return string(s.charBuf.Read()), nil
}

func (s *NaturalTextScanner) advance() (rune, error) {
	n, _, err := s.reader.ReadRune()
	return n, err
}

func (s *NaturalTextScanner) isAtEnd() bool {
	_, err := s.reader.Peek(1)
	if err == io.EOF {
		return true
	}

	return false
}


