package text

import "gitlab.com/apewithcompiler/heru/pkg/set"

type Score struct {
	Term string
	Count int
}

// TermCounter is a set like structure that counts occurences of inserted terms
type TermCounter struct {
	terms map[string]int
}

func NewTermCounter() TermCounter {
	return TermCounter{
		terms: make(map[string]int),
	}
}

func (c *TermCounter) Insert(term string) {
	val, ok := c.terms[term]
	if ok {
		c.terms[term] = val + 1
	} else {
		c.terms[term] = 1
	}
}

func (c *TermCounter) Remove(term string) {
	val, ok := c.terms[term]
	if ok {
		if val > 1 {
			c.terms[term] = val -1
		} else {
			delete(c.terms, term)
		}
	}
}

func (c *TermCounter) Size() int {
	return len(c.terms)
}

func (c *TermCounter) Count(term string) int {
	val, ok := c.terms[term]
	if ok {
		return val
	}

	return 0
}

func (c *TermCounter) Sum() int {
	sum := 0

	for _, v := range c.terms {
		sum = sum + v
	}

	return sum
}

func (c *TermCounter) Score(term string) Score {
	val, ok := c.terms[term]
	if ok {
		return Score{
			Term: term,
			Count: val,
		}
	}

	return Score{
		Term: term,
		Count: 0,
	}
}

func (c *TermCounter) Scores() []Score {
	var scores []Score

	for k, v := range c.terms {
		scores = append(scores, Score{Term: k, Count: v})
	}

	return scores
}

// Contains to implement collection interface
func (c *TermCounter) Contains(e string) bool {
	return c.Size() > 0
}

func (c *TermCounter) Terms() []string {
	t := make([]string, len(c.terms))
	for k := range c.terms {
		t = append(t, k)
	}

	return t
}

func (a *TermCounter) Intersection(b set.Collection) TermCounter {
	c := make(map[string]int)

	for t, v := range a.terms {
		if b.Contains(t) {
			c[t] = v
		}
	}

	return TermCounter{
		terms: c,
	}
}

func (a *TermCounter) Difference(b set.Collection) TermCounter {
	c := make(map[string]int)

	for t, v := range a.terms {
		if !b.Contains(t) {
			c[t] = v
		}
	}

	return TermCounter{
		terms: c,
	}
}

func (a *TermCounter) Union(b TermCounter) TermCounter {
	c := make(map[string]int)

	for t, v := range a.terms {
		c[t] = v
	}

	for t, v := range b.terms {
		val, ok := c[t]
		if ok {
			c[t] = val + v
		} else {
			c[t] = v
		}
	}

	return TermCounter{
		terms: c,
	}
}
