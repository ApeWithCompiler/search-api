package config

import (
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

var currentConfig Config

type TransformerConfig struct {
	Name string `yaml:"name"`
	Options  map[string]string `yaml:"options"`
}

// IndexConfig configures a instace of a index component
type IndexConfig struct {
	// Name of the component instance
	Name string `yaml:"name"`
	// Provider which serves this instance
	Provider string            `yaml:"provider"`
	Options  map[string]string `yaml:"options"`
	Transformer []TransformerConfig `yaml:"transformer"`
}

// ExecutorConfig configures a instance of a crawler executor
type ExecutorConfig struct {
	QueueSize         int64 `yaml:"queue_size"`
	CooldownMs        int   `yaml:"cooldown_ms"`
	Threads           int   `yaml:"threads"`
	DiscardOnOverflow bool  `yaml:"enable_discard_on_overflow"`
}

type CrawlerDatabaseConfig struct {
	Options  map[string]string `yaml:"options"`
}

type CrawlerFileConfig struct {
	Options  map[string]string `yaml:"options"`
}

type CrawlerStorageConfig struct {
	Database CrawlerDatabaseConfig `yaml:"database"`
	File CrawlerFileConfig `yaml:"file"`
}

// CrawlerConfig configures a instance of a crawler
type CrawlerConfig struct {
	// Name of the component instance
	Name       string         `yaml:"name"`
	Storage CrawlerStorageConfig `yaml:"storage"`
	Threads   int `yaml:"threads"`
	FrontierSize int      `yaml:"frontier_size"`
	DuplicateFilterSize int      `yaml:"duplicate_filter_size"`
}

// SyncConfig configures a synchronization betwen a repository and multiple indexes
type SyncConfig struct {
	// Name of the component instance
	Name       string   `yaml:"name"`
	Repository string   `yaml:"repository"`
	Indexes    []string `yaml:"indexes"`
}

type ApiConfig struct {
	ProtectQueries bool   `yaml:"protect_queries"`
	ProtectReads   bool   `yaml:"protect_reads"`
	ProtectUpdates bool   `yaml:"protect_updates"`
	User           string `yaml:"user"`
	Password       string `yaml:"password"`
}

// Config is the top level configuration for search
type Config struct {
	ApiConfig    ApiConfig          `yaml:"api"`
	Indexes      []IndexConfig      `yaml:"indexes"`
	Crawler      []CrawlerConfig    `yaml:"crawler"`
	Sync         []SyncConfig       `yaml:"sync"`
}

func (c *Config) GetIndexConfigFor(name string) (IndexConfig, bool) {
	for _, e := range c.Indexes {
		if e.Name == name {
			return e, true
		}
	}

	return IndexConfig{}, false
}

func (c *Config) GetAllIndexConfigs() []IndexConfig {
	return c.Indexes
}

func (c *Config) GetCralwlerConfigFor(name string) (CrawlerConfig, bool) {
	for _, e := range c.Crawler {
		if e.Name == name {
			return e, true
		}
	}

	return CrawlerConfig{}, false
}

func (c *Config) GetAllCrawlerConfigs() []CrawlerConfig {
	return c.Crawler
}

func (c *Config) GetAllSyncConfigs() []SyncConfig {
	return c.Sync
}

func GetDefaultFile() string {
	return "config.yml"
}

func GetCurrentConfig() Config {
	return currentConfig
}

func ReadConfigFile(file string) {
	log.WithFields(log.Fields{"module": "config"}).Info("Reading file: ", file)
	yamlFile, err := ioutil.ReadFile(file)
	if err != nil {
		log.WithFields(log.Fields{"module": "config"}).Error("yamlFile.Get err  ", err)
	}

	err = yaml.Unmarshal(yamlFile, &currentConfig)
	if err != nil {
		log.WithFields(log.Fields{"module": "config"}).Error("Unmarshal: ", err)
	}
}
