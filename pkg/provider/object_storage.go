package provider

import (
	"io"

	"gitlab.com/apewithcompiler/heru/pkg/model/storage"
)

type ObjectStorage interface {
	GetObject(key storage.ObjectKey) (io.Reader, error)
	SetObject(key storage.ObjectKey, obj io.Reader, size int64) error
	DeleteObject(key storage.ObjectKey) error
	ObjectExists(key storage.ObjectKey) (bool, error)
}

type ObjectStorageFactory interface {
	NewObjectStorage(opts map[string]string) ObjectStorage
}
