package stackexchange

import (
	"net/url"
	"strconv"

	log "github.com/sirupsen/logrus"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/util"
)

func getFirst(items []StackExchangeResponseItem, count int) []StackExchangeResponseItem {
	if count > len(items) {
		return items
	}

	return items[:count]
}

func responseToResult(resp StackExchangeResponse, count int) search.Result {
	var docs []search.ResultDocument

	for _, r := range getFirst(resp.Items, count) {
		d := search.ResultDocument{
			Id:           strconv.Itoa(r.Id),
			Score:        0,
			Title:        r.Title,
			Link:         r.Link,
			FormattedUrl: "",
			Mime:         "text/html",
			Labels:       r.Tags,
		}
		docs = append(docs, d)
	}

	return search.Result{
		Documents: docs,
	}
}

func (p *Stackexchange) Search(query search.Query, count, offset int) search.Result {
	if query.Raw == "" {
		var result search.Result
		return result
	}

	endpoint, _ := url.Parse(p.Config.Host)
	endpoint.Path = "/2.3/search"

	rq := endpoint.Query()
	rq.Set("order", "desc")
	rq.Set("sort", p.Config.Sort)
	rq.Set("site", p.Config.Site)

	rq.Set("intitle", query.Raw)

	endpoint.RawQuery = rq.Encode()

	log.WithFields(log.Fields{"module": "provider/stackexchange"}).Info("Query: ", endpoint.String())

	var resp StackExchangeResponse
	util.Get(util.Credentials{}, endpoint.String(), &resp)

	return responseToResult(resp, count)
}
