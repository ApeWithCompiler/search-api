package stackexchange

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type StackexchangeFactory struct {
}

func (f *StackexchangeFactory) NewIndexReader(conf config.IndexConfig) provider.IndexReader {
	product := &Stackexchange{
		Config: StackexchangeConfig{
			Host: conf.Options["host"],
			Sort: conf.Options["sort"],
			Site: conf.Options["site"],
		},
	}

	log.WithFields(log.Fields{"module": "provider/stackexchange"}).Debug("IndexProvider created")

	return product
}
