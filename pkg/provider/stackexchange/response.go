package stackexchange

type StackExchangeResponse struct {
	Items []StackExchangeResponseItem `json:"items"`
}

type StackExchangeResponseItem struct {
	Id    int      `json:"question_id"`
	Title string   `json:"title"`
	Link  string   `json:"link"`
	Score int      `json:"score"`
	Tags  []string `json:"tags"`
}
