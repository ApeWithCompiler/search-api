package sqlite

import (
	"time"
	"database/sql"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func (p *Sqlite) Init() error {
	conn, err := sql.Open(SQLITE_MODE, p.Path)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Exec("CREATE TABLE schedules (id INTEGER NOT NUL PRIMARY KEY, key TEXT, time DATETIME);")
	if err != nil {
		return err
	}

	_, err = conn.Exec("CREATE INDEX idx_schedules_key ON schedules(key);")
	if err != nil {
		return err
	}

	_, err = conn.Exec("CREATE INDEX idx_schedules_time ON schedules(time);")
	if err != nil {
		return err
	}

	return nil
}

func (p *Sqlite) SetSchedule(id document.DocumentId, ts time.Time) error {
	stmtTpl := "INSERT INTO schedules (key, time) VALUES (?, ?)"

	conn, err := sql.Open(SQLITE_MODE, p.Path)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(stmtTpl)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id.String(), ts)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (p *Sqlite) CancelSchedule(id document.DocumentId) error {
	stmtTpl := "DELETE FROM schedules WHERE key = ?"

	conn, err := sql.Open(SQLITE_MODE, p.Path)
	if err != nil {
		return err
	}
	defer conn.Close()

	tx, err := conn.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(stmtTpl)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id.String())
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (p *Sqlite) AcknowledgeSchedule(id document.DocumentId) error {
	return p.CancelSchedule(id)
}

func (p *Sqlite) QueryActiveSchedules(ts time.Time, limit int) ([]document.DocumentId, error) {
	stmtTpl := "SELECT key FROM schedules WHERE time < ? LIMIT ?"

	var schedules []document.DocumentId

	conn, err := sql.Open(SQLITE_MODE, p.Path)
	if err != nil {
		return schedules, err
	}
	defer conn.Close()

	rows, err := conn.Query(stmtTpl, ts, limit)
	if err != nil {
		return schedules, err
	}
	
	for rows.Next() {
		var s string
		err = rows.Scan(s)
		if err != nil {
			return schedules, err
		}
		schedule := document.PrimitiveId(s)

		schedules = append(schedules, schedule)
	}

	return schedules, nil
}
