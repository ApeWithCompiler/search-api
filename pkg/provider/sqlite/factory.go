package sqlite

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type SqliteFactory struct {
}

func (f *SqliteFactory) NewScheduleStorage(opts map[string]string) provider.ScheduleStorage {
	product := &Sqlite{
		Path: opts["path"],
	}

	log.WithFields(log.Fields{"module": "provider/sqlite"}).Debug("ScheduleStorage created")

	return product
}
