package loader

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type IndexReaderLazyLoader struct {
	Config  config.IndexConfig
	Factory provider.IndexReaderFactory
}

func (f *IndexReaderLazyLoader) Search(query search.Query, count, offset int) search.Result {
	log.WithFields(log.Fields{"module": "LazyLoder", "index": f.Config.Name}).Debug("Lazy loading provider for request")
	reader := f.Factory.NewIndexReader(f.Config)
	return reader.Search(query, count, offset)
}

type IndexWriterLazyLoader struct {
	Config  config.IndexConfig
	Factory provider.IndexWriterFactory
}

func (f *IndexWriterLazyLoader) String() string {
	return f.Config.Name
}

func (f *IndexWriterLazyLoader) SetDocument(id document.DocumentId, doc document.DocumentResult) error {
	log.WithFields(log.Fields{"module": "LazyLoder", "index": f.Config.Name}).Debug("Lazy loading provider for request")

	writer := f.Factory.NewIndexWriter(f.Config)
	err := writer.SetDocument(id, doc)
	if err != nil {
		log.WithFields(log.Fields{"module": "LazyLoder", "index": f.Config.Name}).Error("Loaded provider returned error: ", err)
	}

	return err
}

func (f *IndexWriterLazyLoader) DeleteDocument(id document.DocumentId) error {
	log.WithFields(log.Fields{"module": "LazyLoder", "index": f.Config.Name}).Debug("Lazy loading provider for request")

	writer := f.Factory.NewIndexWriter(f.Config)
	err := writer.DeleteDocument(id)
	if err != nil {
		log.WithFields(log.Fields{"module": "LazyLoder", "index": f.Config.Name}).Error("Loaded provider returned error: ", err)
	}

	return err
}
