package zinc

type Query struct {
	Value     string `json:"query"`
	Fuzziness string `json:"fuzziness,omitempty"`
}

type WildcardQuery struct {
	Value string `json:"value"`
	Boost int    `json:"boost,omitempty"`
}

type FieldExpression struct {
	Title            interface{} `json:"title,omitempty"`
	HtmlTitle        interface{} `json:"htmlTitle,omitempty"`
	Link             interface{} `json:"link,omitempty"`
	DisplayLink      interface{} `json:"displayLink,omitempty"`
	Snippet          interface{} `json:"snippet,omitempty"`
	HtmlSnippet      interface{} `json:"htmlSnippet,omitempty"`
	FormattedUrl     interface{} `json:"formattedUrl,omitempty"`
	HtmlFormattedUrl interface{} `json:"htmlFormattedUrl,omitempty"`
	Mime             interface{} `json:"mime,omitempty"`
	FileFormat       interface{} `json:"fileFormat,omitempty"`
	Labels           interface{} `json:"labels,omitempty"`
	Id               interface{} `json:"_id,omitempty"`
	Index            interface{} `json:"_index,omitempty"`
}

type BoolExpression struct {
	Bool interface{} `json:"bool"`
}

type ConditionExpression struct {
	Must    []MatchExpression `json:"must,omitempty"`
	MustNot []MatchExpression `json:"must_not,omitempty"`
	Should  []MatchExpression `json:"should,omitempty"`
	Minimum int               `json:"minimum_should_match,omitempty"`
}

type MatchExpression struct {
	Match    interface{} `json:"match,omitempty"`
	Wildcard interface{} `json:"wildcard,omitempty"`
}

type QueryContainer struct {
	Root interface{} `json:"query"`
	Size int         `json:"size"`
	From int         `json:"from"`
}
