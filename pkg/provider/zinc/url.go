package zinc

import (
	"net/url"
	"path"
)

func urlSearchIndex(host, index string) *url.URL {
	u, _ := url.Parse(host)
	u.Path = path.Join("/es", index, "_search")

	return u
}

func urlUpdateDoc(host, index, documentId string) *url.URL {
	u, _ := url.Parse(host)
	u.Path = path.Join("/api/", index, "/_doc/", documentId)

	return u
}

func urlDeleteDoc(host, index, documentId string) *url.URL {
	u, _ := url.Parse(host)
	u.Path = path.Join("/api/", index, "/_doc/", documentId)

	return u
}
