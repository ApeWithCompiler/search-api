package zinc

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/util"
)

func getLast(items []ZincItem, count int) []ZincItem {
	if count > len(items) {
		return items
	}

	return items[len(items)-count:]
}

func responseToResult(resp ZincResponse, count int) search.Result {
	var docs []search.ResultDocument

	for _, r := range getLast(resp.Data.Items, count) {
		d := search.ResultDocument{
			Id:           r.Id,
			Score:        r.Score,
			Title:        r.Data.Title,
			Link:         r.Data.Link,
			FormattedUrl: r.Data.FormattedUrl,
			Snippet:      r.Data.Snippet,
			ThumbnailUrl: r.Data.ThumbnailUrl,
			FaviconUrl:   r.Data.FaviconUrl,
			Mime:         r.Data.Mime,
			Labels:       r.Data.Labels,
			Updated:      r.Data.Updated,
		}
		docs = append(docs, d)
	}

	total := resp.Data.Total.Value

	return search.Result {
		Documents: docs,
		MaxCount: total,
	}
}

func (p *Zinc) Search(query search.Query, count, offset int) search.Result {
	if query.Raw == "" {
		var result search.Result
		return result
	}

	esParser := EsParserVisitor{}
	q := esParser.Parse(query.Expressions, count, offset)
	qBin, _ := json.Marshal(q)

	endpoint := urlSearchIndex(p.Config.Host, p.Config.Index)

	log.WithFields(log.Fields{"module": "provider/zinc"}).Debug("Query: ", string(qBin))

	var resp ZincResponse
	util.Post(p.GetCredentials(), endpoint.String(), qBin, &resp)

	return responseToResult(resp, count)

}
