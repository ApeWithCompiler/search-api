package zinc

import (
	"time"
)

type ZincLink struct {
	Title            string    `json:"title"`
	HtmlTitle        string    `json:"htmlTitle"`
	Link             string    `json:"link"`
	DisplayLink      string    `json:"displayLink"`
	Snippet          string    `json:"snippet"`
	HtmlSnippet      string    `json:"htmlSnippet"`
	FormattedUrl     string    `json:"formattedUrl"`
	HtmlFormattedUrl string    `json:"htmlFormattedUrl"`
	ThumbnailUrl     string    `json:"thumbnailUrl"`
	FaviconUrl       string    `json:"faviconUrl"`
	Mime             string    `json:"mime"`
	FileFormat       string    `json:"fileFormat"`
	Labels           []string  `json:"labels"`
	Updated          time.Time `json:"updated"`
}

type ZincItem struct {
	Index string   `json:"_index"`
	Id    string   `json:"_id"`
	Score float64  `json:"_score"`
	Data  ZincLink `json:"_source"`
}

type ZincItemContainer struct {
	Items []ZincItem `json:"hits"`
	Total ZincTotalContainer `json:"total"`
}

type ZincTotalContainer struct {
	Value int `json:"value"`
}

type ZincResponse struct {
	Data ZincItemContainer `json:"hits"`
}

type ZincIndexStats struct {
	DocumentCount int `json:"doc_num"`
}

type ZincIndex struct {
	Name  string         `json:"name"`
	Stats ZincIndexStats `json:"stats"`
}

type ZincIndexResponse struct {
	Data []ZincIndex `json:"list"`
}
