package zinc

import (
	"gitlab.com/apewithcompiler/heru/pkg/util"
)

type ZincConfig struct {
	Host     string
	Index    string
	Username string
	Password string
}

func (p *Zinc) GetCredentials() util.Credentials {
	return util.Credentials{
		Username: p.Config.Username,
		Password: p.Config.Password,
	}
}
