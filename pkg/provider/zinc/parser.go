package zinc

import (
	"strings"

	giql "gitlab.com/ApeWithCompiler/giql"
)

type EsParserVisitor struct {
	literal_cache string

	matchers_must     []MatchExpression
	matchers_must_not []MatchExpression
	matchers_should   []MatchExpression

	Context QueryContainer
}

func (v *EsParserVisitor) Parse(expr []giql.Expr, count, offset int) QueryContainer {
	typePrinter := giql.TypeVisitor{}

	for _, e := range expr {
		if typePrinter.Print(e) == "literal" {
			v.addTextword(e)
		} else {
			e.Accept(v)
		}
	}
	v.Context = QueryContainer{
		Root: BoolExpression{
			Bool: ConditionExpression{
				Must:    v.matchers_must,
				MustNot: v.matchers_must_not,
				Should:  v.matchers_should,
				Minimum: 1,
			},
		},
		Size: count,
		From: offset,
	}
	return v.Context
}

func (v *EsParserVisitor) VisitUnary(unary *giql.Unary) {
	switch unary.Operator.Type {
	case giql.PLUS:
		unary.Right.Accept(v)

		expr := MatchExpression{
			Match: FieldExpression{
				Title: Query{
					Value:     v.literal_cache,
					Fuzziness: "AUTO",
				},
			},
		}
		v.matchers_must = append(v.matchers_must, expr)
	case giql.MINUS:
	case giql.NOT:
		unary.Right.Accept(v)

		expr := MatchExpression{
			Match: FieldExpression{
				Title: Query{
					Value:     v.literal_cache,
					Fuzziness: "AUTO",
				},
			},
		}
		v.matchers_must_not = append(v.matchers_must_not, expr)
	case giql.POUND:
		unary.Right.Accept(v)

		expr := MatchExpression{
			Match: FieldExpression{
				Labels: Query{
					Value: v.literal_cache,
				},
			},
		}
		v.matchers_must = append(v.matchers_must, expr)
	}

	v.cleanLiteralCache()
}

func (v *EsParserVisitor) VisitLiteral(literal *giql.Literal) {
	v.literal_cache = literal.Value
}

func (v *EsParserVisitor) VisitStrict(strict *giql.Strict) {
	expr := MatchExpression{
			Match: FieldExpression{
				Title: Query{
					Value: strict.Value,
				},
			},
		}
	v.matchers_must = append(v.matchers_must, expr)
}

func (v *EsParserVisitor) VisitGrouping(grouping *giql.Grouping) {
	//TODO implement reparsing for groupings to zinc/es queries
}

func (v *EsParserVisitor) VisitBoolean(boolean *giql.Boolean) {
	//TODO implement reparsing for bools to zinc/es queries
}

func (v *EsParserVisitor) VisitAssignment(assignment *giql.Assignment) {
	assignment.Identifier.Accept(v)
	if v.literal_cache == "inurl" {
		assignment.Value.Accept(v)

		expr := MatchExpression{
			Wildcard: FieldExpression{
				Link: WildcardQuery{
					Value: "*" + v.literal_cache + "*",
				},
			},
		}
		v.matchers_must = append(v.matchers_must, expr)
	}

	if v.literal_cache == "intitle" {
		assignment.Value.Accept(v)

		expr := MatchExpression{
			Match: FieldExpression{
				Title: Query{
					Value: v.literal_cache,
				},
			},
		}
		v.matchers_must = append(v.matchers_must, expr)
	}

	if v.literal_cache == "id" {
		assignment.Value.Accept(v)

		expr := MatchExpression{
			Match: FieldExpression{
				Id: Query{
					Value: strings.TrimPrefix(v.literal_cache, "ID"),
				},
			},
		}
		v.matchers_must = append(v.matchers_must, expr)
	}

	v.cleanLiteralCache()
}

func (v *EsParserVisitor) cleanLiteralCache() {
	v.literal_cache = ""
}

func (v *EsParserVisitor) addTextword(expr giql.Expr) {
	expr.Accept(v)

	nameExpr := MatchExpression{
		Match: FieldExpression{
			Title: Query{
				Value:     v.literal_cache,
				Fuzziness: "AUTO",
			},
		},
	}
	v.matchers_should = append(v.matchers_should, nameExpr)

	snippetExpr := MatchExpression{
		Match: FieldExpression{
			Snippet: Query{
				Value:     v.literal_cache,
				Fuzziness: "AUTO",
			},
		},
	}
	v.matchers_should = append(v.matchers_should, snippetExpr)


	urlExpr := MatchExpression{
		Wildcard: FieldExpression{
			Link: WildcardQuery{
				Value: "*" + v.literal_cache + "*",
			},
		},
	}
	v.matchers_should = append(v.matchers_should, urlExpr)

	labelExpr := MatchExpression{
		Match: FieldExpression{
			Labels: Query{
				Value:     v.literal_cache,
				Fuzziness: "AUTO",
			},
		},
	}
	v.matchers_should = append(v.matchers_should, labelExpr)

	v.cleanLiteralCache()
}
