package zinc

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
	"gitlab.com/apewithcompiler/heru/pkg/util"
)

func (p *Zinc) DeleteDocument(id document.DocumentId) error {
	endpoint := urlDeleteDoc(p.Config.Host, p.Config.Index, id.String())

	log.WithFields(log.Fields{"module": "provider/zinc"}).Info("Index delete ", id.String())
	util.Delete(p.GetCredentials(), endpoint.String(), nil)

	return nil
}

func (p *Zinc) SetDocument(id document.DocumentId, doc document.DocumentResult) error {
	endpoint := urlUpdateDoc(p.Config.Host, p.Config.Index, id.String())

	query, _ := json.Marshal(doc)
	log.WithFields(log.Fields{"module": "provider/zinc"}).Info("Index update ", id.String())
	util.Put(p.GetCredentials(), endpoint.String(), query, nil)

	return nil
}
