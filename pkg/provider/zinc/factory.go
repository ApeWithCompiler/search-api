package zinc

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type ZincFactory struct {
}

func (f *ZincFactory) NewIndexWriter(conf config.IndexConfig) provider.IndexWriter {
	product := &Zinc{
		Config: ZincConfig{
			Host:     conf.Options["host"],
			Index:    conf.Options["index"],
			Username: conf.Options["username"],
			Password: conf.Options["password"],
		},
	}

	log.WithFields(log.Fields{"module": "provider/zinc"}).Debug("IndexProvider created")

	return product
}

func (f *ZincFactory) NewIndexReader(conf config.IndexConfig) provider.IndexReader {
	product := f.NewIndexWriter(conf)
	return product.(provider.IndexReader)
}
