package provider

import (
	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

type IndexWriter interface {
	SetDocument(id document.DocumentId, doc document.DocumentResult) error
	DeleteDocument(id document.DocumentId) error
}

type IndexWriterFactory interface {
	NewIndexWriter(conf config.IndexConfig) IndexWriter
}
