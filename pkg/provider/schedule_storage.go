
package provider

import (
	"time"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

type ScheduleStorage interface {
	SetSchedule(id document.DocumentId, ts time.Time) error
	CancelSchedule(id document.DocumentId) error
	AcknowledgeSchedule(document.DocumentId) error
	QueryActiveSchedules(ts time.Time, limit int) ([]document.DocumentId, error)
}

type ScheduleStorageFactory interface {
	NewScheduleStorage(opts map[string]string) ScheduleStorage
}
