package minio

import (
	"context"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/provider"
)

type MinioFactory struct {
}

func (f *MinioFactory) NewObjectStorage(opts map[string]string) provider.ObjectStorage {
	ctx, cancel := context.WithCancel(context.Background())

	product := &Minio{
		Config: MinioConfig{
			Host:   opts["host"],
			Bucket: opts["bucket"],
			Key:    opts["key"],
			Secret: opts["secret"],
		},
		Context: ctx,
		Cancel:  cancel,
	}

	client, err := minio.New(product.Config.Host, &minio.Options{
		Creds:  credentials.NewStaticV4(product.Config.Key, product.Config.Secret, ""),
		Secure: false,
	})

	if err != nil {
		log.WithFields(log.Fields{"module": "provider/minio"}).Error("ObjectStorageProvider creation error: ", err)
	}

	product.Client = client

	log.WithFields(log.Fields{"module": "provider/minio"}).Debug("ObjectStorageProvider created")

	return product
}
