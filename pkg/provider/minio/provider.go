package minio

import (
	"context"

	"github.com/minio/minio-go/v7"
)

type Minio struct {
	Config  MinioConfig
	Context context.Context
	Cancel  context.CancelFunc
	Client  *minio.Client
}
