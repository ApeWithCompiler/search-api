package minio

import (
	"io"

	"github.com/minio/minio-go/v7"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/storage"
)

func (m *Minio) GetObject(key storage.ObjectKey) (io.Reader, error) {
	object, err := m.Client.GetObject(
		m.Context,
		m.Config.Bucket,
		key.String(),
		minio.GetObjectOptions{},
	)

	if err != nil {
		log.WithFields(log.Fields{"module": "provider/minio"}).Error("GetObject error: ", err)
	}

	return object, err
}

func (m *Minio) SetObject(key storage.ObjectKey, obj io.Reader, size int64) error {
	_, err := m.Client.PutObject(
		m.Context,
		m.Config.Bucket,
		key.String(),
		obj,
		size,
		minio.PutObjectOptions{
			ContentType: "application/yml",
		},
	)

	if err != nil {
		log.WithFields(log.Fields{"module": "provider/minio"}).Error("SetObject error: ", err)
	}

	return err
}

func (m *Minio) DeleteObject(key storage.ObjectKey) error {
	err := m.Client.RemoveObject(
		m.Context,
		m.Config.Bucket,
		key.String(),
		minio.RemoveObjectOptions{},
	)

	if err != nil {
		log.WithFields(log.Fields{"module": "provider/minio"}).Error("DeleteObject error: ", err)
	}

	return err
}

func (m *Minio) ObjectExists(key storage.ObjectKey) (bool, error) {
	_, err := m.Client.StatObject(
		m.Context,
		m.Config.Bucket,
		key.String(),
		minio.StatObjectOptions{},
	)

	if err != nil {
		errResponse := minio.ToErrorResponse(err)
		switch errResponse.Code {
		case "NoSuchKey":
			return false, nil
		default:
			return false, err
		}
	}

	return true, nil
}
