package minio

type MinioConfig struct {
	Host   string
	Bucket string
	Key    string
	Secret string
}
