package provider

import (
	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

type Searchable interface {
	Search(query search.Query, count, offset int) search.Result
}

type IndexReader interface {
	Searchable
}

type IndexReaderFactory interface {
	NewIndexReader(conf config.IndexConfig) IndexReader
}
