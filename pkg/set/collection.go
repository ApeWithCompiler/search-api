package set

type Collection interface {
	Contains(e string) bool
}
