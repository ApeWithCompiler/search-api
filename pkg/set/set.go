package set

// StringSet represents a set of type string
type StringSet struct {
	elements map[string]bool
}

// NewStringSet initializes a new set
func NewStringSet(elements []string) StringSet {
	m := make(map[string]bool)
	for _, e := range elements {
		m[e] = true
	}

	return StringSet{
		elements: m,
	}
}

// Insert a new element into the set
func (set *StringSet) Insert(element string) {
	set.elements[element] = true
}

// Remove a element from the set
func (set *StringSet) Remove(element string) {
	delete(set.elements, element)
}

// Has checks if set has element
func (set *StringSet) Has(element string) bool {
	_, ok := set.elements[element]
	return ok
}

// Contains to implement collection interface
func (set *StringSet) Contains(e string) bool {
	return set.Has(e)
}

// Elements returns the elements as a slice
func (set *StringSet) Elements() []string {
	e := make([]string, len(set.elements))
	for k := range set.elements {
		e = append(e, k)
	}

	return e
}

// Size returns number of elements in set
func (set *StringSet) Size() int {
	return len(set.elements)
}

// Intersection of two string sets
func (a *StringSet) Intersection(b Collection) StringSet {
	c := NewStringSet([]string{})

	for e := range a.elements {
		if b.Contains(e) {
			c.Insert(e)
		}
	}

	return c
}

// Difference of two string sets
func (a *StringSet) Difference(b Collection) StringSet {
	c := NewStringSet([]string{})

	for e := range a.elements {
		if !b.Contains(e) {
			c.Insert(e)
		}
	}

	return c
}

// Union of two string sets
func (a *StringSet) Union(b *StringSet) StringSet {
	c := NewStringSet(a.Elements())

	for e := range b.elements {
		c.Insert(e)
	}

	return c
}
