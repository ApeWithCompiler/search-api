package set

import (
	"io"
	"bufio"
)

// NewStringSetFromReader creates a string set from a io reader
func NewStringSetFromReader(r io.Reader) (StringSet, error) {
	set := NewStringSet([]string{})

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		set.Insert(scanner.Text())
	}

	return set, scanner.Err()
}
