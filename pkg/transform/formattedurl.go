package transform

import (
	"fmt"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"net/url"
	"strings"
)

func toFormattedUrl(link string) string {
	lengthLimit := 20
	u, _ := url.Parse(link)
	s := strings.Split(u.Path, "/")
	length := 0
	var s2 []string
	for _, e := range s {
		if len(s)+length < lengthLimit {
			s2 = append(s2, e)
			length = length + len(s)
		}
	}
	p := strings.Join(s2, " > ")
	p = strings.TrimSuffix(p, " > ")
	return fmt.Sprintf("%v://%v%v", u.Scheme, u.Host, p)
}

type FormattedUrlTransformer struct {
	Force bool
}

func (t *FormattedUrlTransformer) TransformResult(query search.Query, result search.ResultDocument) search.ResultDocument {
	if result.FormattedUrl == "" || t.Force {
		result.FormattedUrl = toFormattedUrl(result.Link)
	}

	return result
}
