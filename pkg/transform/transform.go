package transform

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

type ResultTransformer interface {
	TransformResult(query search.Query, result search.ResultDocument) search.ResultDocument
}
