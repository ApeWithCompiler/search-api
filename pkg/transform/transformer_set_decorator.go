package transform

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

type TransformerSetDecorator struct {
	set []ResultTransformer
}

func NewTransformerSetDecorator(transformerSet []ResultTransformer) *TransformerSetDecorator {
	return &TransformerSetDecorator{
		set: transformerSet,
	}
}

func (t *TransformerSetDecorator) TransformResult(query search.Query, result search.ResultDocument) search.ResultDocument {
	for i := range t.set {
		result = t.set[i].TransformResult(query, result)
	}

	return result
}
