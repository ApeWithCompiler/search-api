package util

import (
	"testing"
)

type CursorWindowStartCase struct {
	Cursor int
	Count int
	ExpectedCursor int
}

func TestWindow_CursorToWindowStart(t *testing.T) {
	cases := []CursorWindowStartCase{
		{
			Cursor: 8,
			Count: 5,
			ExpectedCursor: 6,
		},
		{
			Cursor: 2,
			Count: 5,
			ExpectedCursor: 0,
		},
		{
			Cursor: 8,
			Count: 4,
			ExpectedCursor: 7,
		},
	}

	for _, c := range cases {
		got := CursorToWindowStart(c.Cursor, c.Count)

		if got != c.ExpectedCursor {
			t.Errorf("CursorToWindowStart() returned %v, want %v. Params: cur %v, count %v", got, c.ExpectedCursor, c.Cursor, c.Count)
		}
	}
}

type SliceWindowCase struct {
	Cursor int
	Count int
	Max int
	ExpectedStart int
	ExpectedEnd int
}

func TestWindow_SliceWindow(t *testing.T) {
	cases := []SliceWindowCase{
		{
			Cursor: 0,
			Count: 8,
			Max: 5,
			ExpectedStart: 0,
			ExpectedEnd: 5,
		},
		{
			Cursor: 12,
			Count: 5,
			Max: 30,
			ExpectedStart: 12,
			ExpectedEnd: 16,
		},
		{
			Cursor: 12,
			Count: 5,
			Max: 15,
			ExpectedStart: 11,
			ExpectedEnd: 15,
		},
		{
			Cursor: 11,
			Count: 5,
			Max: 15,
			ExpectedStart: 11,
			ExpectedEnd: 15,
		},
		{
			Cursor: 10,
			Count: 5,
			Max: 15,
			ExpectedStart: 10,
			ExpectedEnd: 14,
		},
	}

	for _, c := range cases {
		gotStart, gotEnd := SliceWindow(c.Cursor, c.Count, c.Max)

		if gotStart != c.ExpectedStart {
			t.Errorf("SliceWindow() returned start %v, want %v. Params: cur %v, count %v, max %v", gotStart, c.ExpectedStart, c.Cursor, c.Count, c.Max)
		}
		if gotEnd != c.ExpectedEnd {
			t.Errorf("SliceWindow() returned end %v, want %v. Params: cur %v, count %v, max %v", gotEnd, c.ExpectedEnd, c.Cursor, c.Count, c.Max)
		}
	}
}
