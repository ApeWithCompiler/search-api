package util

import (
	"path"
	"strings"
)

// reverse a string slice based on recursion
// the slices.Reverse() function was introduced in Go 1.21
// so I assume it is reasonable to trade backwards compatibility
// over elegance.
func reverse(input []string) []string {
	if len(input) <= 1 {
		return input
	}
	
	return append(reverse(input[1:]), input[0])
}

// match compares to string slices
// if the pattern contains '*', the comp will match
// '*' matches allso following elements recursivly
func match(pattern, comp []string) bool {
	if pattern[0] == "*" {
		return true
	}

	if pattern[0] == comp [0] {
		if len(pattern) == 1 && len(comp) == 1 {
			return true
		}

		if len(pattern) > 1 && len(comp) == 1 {
			return false
		}

		if len(pattern) == 1 && len(comp) > 1 {
			return false
		}

		return match(pattern[1:], comp[1:])
	}

	return false
}

// Path is a helper class for handling and comparison of such
type Path string

// String returns Path as a string
func (p Path) String() string {
	return string(p)
}

// Match compares two paths
// For comparison rules check 'match(pattern, comp []string) bool'
func (p Path) Match(c Path) bool {
	pSeg := strings.Split(p.String(), "/")
	cSeg := strings.Split(c.String(), "/")

	return match(pSeg, cSeg)
}

// Equals compares two paths exactly
func (p Path) Equals(c Path) bool {
	return p.String() == c.String()
}

// Formats a URL host to a Path
// www.example.com -> /com/example/www
func NewPathFromHost(host string) Path {
	s := strings.Split(host, ".")
	s = reverse(s)
	p := path.Join("/", strings.Join(s, "/"))

	return Path(p)
}
