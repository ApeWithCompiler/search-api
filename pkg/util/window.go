package util

import (

)

// CursorToWindowStar transforms a cursor from the middle of the window to the start
// uneven: for cnt=5 a input window of 2+cur+2 (5 elements) is assumed
// result is cur+4
// even: for cnt=4 a input window of 1+cur+2 (4 elements) is assumed
// result is cur+3
// negative cursors will return 0
func CursorToWindowStart(cur, cnt int) int {
	countOdd := (cnt & 1) != 0
	if countOdd {
		cnt = cnt - 1
		cur = cur - (cnt / 2)
	} else {
		cur = cur - (cnt / 2) + 1
	}

	if cur < 0 {
		return 0
	}
	return cur
}


// SliceWindow returns indexes for a fixed size window
// cur array index of curser postion
// cnt size of window
// max array index of last element available
func SliceWindow(cur, cnt, max int) (int, int) {
	if cnt >= max {
		return 0, max
	}

	if cur + cnt > max {
		return max - cnt + 1, max
	}

	return cur, cur + cnt - 1
}
