package mime

import (
	"reflect"
	"testing"
)

var (
	testEntries = []MimeEntry{
		{
			Name: "Adobe Portable Document Format",
			Mime: "application/pdf",
			Extensions: []string{
				"pdf",
			},
			Type: "application",
		},
		{
			Name: "HyperText Markup Language (HTML)",
			Mime: "text/html",
			Extensions: []string{
				"html",
			},
			Type: "text",
		},
		{
			Name: "YAML Ain't Markup Language / Yet Another Markup Language",
			Mime: "text/yaml",
			Extensions: []string{
				"yaml",
			},
			Type: "text",
		},
		{
			Name: "Malformed entry, should be ignored",
			Mime: "application/pdf",
			Extensions: []string{
				"pdf",
				"html",
			},
			Type: "text",
		},
	}
)

func TestDB_QuerybyExtension(t *testing.T) {
	want := MimeEntry{
		Name: "HyperText Markup Language (HTML)",
		Mime: "text/html",
		Extensions: []string{
			"html",
		},
		Type: "text",
	}

	sut := NewMimeDBFromEntries(testEntries)
	got, _ := sut.QueryByExtension("html")

	if !reflect.DeepEqual(got, want) {
		t.Errorf("QuerybyExtension() returned %v, want %v", got.Name, want.Name)
	}
}

func TestDB_QuerybyMime(t *testing.T) {
	want := MimeEntry{
		Name: "Adobe Portable Document Format",
		Mime: "application/pdf",
		Extensions: []string{
			"pdf",
		},
		Type: "application",
	}

	sut := NewMimeDBFromEntries(testEntries)
	got, _ := sut.QueryByMime("application/pdf")

	if !reflect.DeepEqual(got, want) {
		t.Errorf("QuerybyExtension() returned %v, want %v", got.Name, want.Name)
	}
}
