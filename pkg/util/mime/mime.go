package mime

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

type MimeFile struct {
	Entries []MimeEntry `json:"files"`
}

type MimeEntry struct {
	Name       string   `json:"name"`
	Mime       string   `json:"mime"`
	Extensions []string `json:"extensions"`
	Type       string   `json:"type"`
}

func ReadMimeJson(filepath string) ([]MimeEntry, error) {
	logger := log.WithFields(log.Fields{"module": "mime"})
	var mimeFile MimeFile

	logger.Info("Loading file: ", filepath)

	jsonFile, err := os.Open(filepath)
	if err != nil {
		logger.Error("Unable to open file: ", err)
		return mimeFile.Entries, err
	}

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		logger.Error("Unable to read file: ", err)
		return mimeFile.Entries, err
	}

	defer jsonFile.Close()

	err = json.Unmarshal(byteValue, &mimeFile)
	if err != nil {
		logger.Error("Unable to parse file content: ", err)
		return mimeFile.Entries, err
	}

	return mimeFile.Entries, nil

}

func NewMimeDBFromEntries(entries []MimeEntry) MimeDB {
	db := MimeDB{
		extensionIndex: make(map[string]MimeEntry),
		mimeIndex:      make(map[string]MimeEntry),
	}

	for _, entry := range entries {
		db.AddEntry(entry)
	}

	return db
}

type MimeDB struct {
	extensionIndex map[string]MimeEntry
	mimeIndex      map[string]MimeEntry
}

func (db *MimeDB) AddEntry(entry MimeEntry) {
	for _, ext := range entry.Extensions {
		_, extensionExists := db.extensionIndex[ext]
		if extensionExists {
			log.WithFields(log.Fields{"module": "mime"}).Warn("Extension allready exists, skipping: ", ext)
			continue
		}
		db.extensionIndex[ext] = entry
	}

	_, mimeExists := db.mimeIndex[entry.Mime]
	if mimeExists {
		log.WithFields(log.Fields{"module": "mime"}).Warn("Mime allready exists, skipping: ", entry.Mime)
		return
	}

	db.mimeIndex[entry.Mime] = entry
}

func (db *MimeDB) QueryByExtension(ext string) (MimeEntry, error) {
	entry, exists := db.extensionIndex[ext]
	if exists {
		return entry, nil
	}

	return MimeEntry{}, fmt.Errorf("Not found")
}

func (db *MimeDB) QueryByMime(mime string) (MimeEntry, error) {
	entry, exists := db.mimeIndex[mime]
	if exists {
		return entry, nil
	}

	return MimeEntry{}, fmt.Errorf("Not found")
}
