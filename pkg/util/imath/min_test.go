package imath

import (
	"testing"
)

func TestMin(t *testing.T) {
	var tests = []struct {
		A int
		B int
		Want int
	}{
		{
			A: 4,
			B: 8,
			Want: 4,
		},
		{
			A: 12,
			B: -5,
			Want: -5,
		},
		{
			A: 3,
			B: 3,
			Want: 3,
		},
	}

	for _, c := range tests {
		got := Min(c.A, c.B)

		if got != c.Want {
			t.Errorf("Min() returned %v, want %v", got, c.Want)
		}
	}
}
