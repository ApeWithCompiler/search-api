package imath

// Interval returns a slice containing the all integers
// from start to end
func Interval(start, end int) []int {
	var r []int

	if end >= start {
		for i := start; i <=end; i++ {
			r = append(r, i)
		}
	} else {
		for i := start; i >=end; i-- {
			r = append(r, i)
		}
	}

	return r
}

// IntervalV returns a slice containing the all integers
// from start over the length
func IntervalV(start, length int) []int {
	return Interval(start, start+length)
}
