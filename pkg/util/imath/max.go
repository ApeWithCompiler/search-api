package imath

// Max returns the maximal of two integers
func Max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
