package imath

import (
	"reflect"
	"testing"
)

func TestInterval(t *testing.T) {
	var tests = []struct {
		Start int
		End int
		Want []int
	}{
		{
			Start: 4,
			End: 8,
			Want: []int{4, 5, 6, 7, 8},
		},
		{
			Start: 12,
			End: 5,
			Want: []int{12, 11, 10, 9, 8, 7, 6, 5},
		},
		{
			Start: 3,
			End: 3,
			Want: []int{3,},
		},
	}

	for _, c := range tests {
		got := Interval(c.Start, c.End)

		if !reflect.DeepEqual(got, c.Want) {
			t.Errorf("Interval() returned %v, want %v", got, c.Want)
		}
	}
}

func TestIntervalV(t *testing.T) {
	var tests = []struct {
		Start int
		Length int
		Want []int
	}{
		{
			Start: 4,
			Length: 3,
			Want: []int{4, 5, 6, 7,},
		},
		{
			Start: 12,
			Length: -5,
			Want: []int{12, 11, 10, 9, 8, 7,},
		},
		{
			Start: 3,
			Length: 0,
			Want: []int{3,},
		},
	}

	for _, c := range tests {
		got := IntervalV(c.Start, c.Length)

		if !reflect.DeepEqual(got, c.Want) {
			t.Errorf("IntervalV() returned %v, want %v", got, c.Want)
		}
	}
}
