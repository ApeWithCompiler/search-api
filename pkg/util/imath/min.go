package imath

// Min returns the minimal of two integers
func Min(a, b int) int {
	if a > b {
		return b
	}

	return a
}
