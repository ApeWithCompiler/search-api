package imath

import (
	"testing"
)

func TestMax(t *testing.T) {
	var tests = []struct {
		A int
		B int
		Want int
	}{
		{
			A: 4,
			B: 8,
			Want: 8,
		},
		{
			A: 12,
			B: -5,
			Want: 12,
		},
		{
			A: 3,
			B: 3,
			Want: 3,
		},
	}

	for _, c := range tests {
		got := Max(c.A, c.B)

		if got != c.Want {
			t.Errorf("Max() returned %v, want %v", got, c.Want)
		}
	}
}
