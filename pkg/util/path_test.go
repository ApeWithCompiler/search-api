package util

import (
	"reflect"
	"testing"
)

func TestReverse(t *testing.T) {
	src := []string{"A", "B", "C", "D"}
	want := []string{"D", "C", "B", "A"}

	got := reverse(src)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("reverse() returned %v, want %v", got, want)
	}
}

func TestPath_FromHost(t *testing.T) {
	src := "www.example.org"
	want := "/org/example/www"

	sut := NewPathFromHost(src)
	got := sut.String()

	if got != want {
		t.Errorf("NewPathFrom() returned %v, want %v", got, want)
	}
}

func TestPath_Matches(t *testing.T) {
	ref := "/foo/bar/baz"
	testCases := []string{
		"/foo/bar/baz",
		"/foo/bar/*",
	}
	want := true

	for _, c := range(testCases) {
		p := Path(c)
		got := p.Match(Path(ref))

		if got != want {
			t.Errorf("Match() returned %v, want %v", got, want)
		}
	}
}

func TestPath_NotMatches(t *testing.T) {
	ref := "/foo/bar"
	testCases := []string{
		"/foo/bar/baz",
		"/foo/ba",
		"/foo",
	}
	want := false

	for _, c := range(testCases) {
		p := Path(c)
		got := p.Match(Path(ref))

		if got != want {
			t.Errorf("Match() returned %v, want %v", got, want)
		}
	}
}

func TestPath_Equals(t *testing.T) {
	ref := "/foo/bar"
	src := "/foo/bar"
	want := true

	p := Path(src)
	got := p.Equals(Path(ref))

	if got != want {
		t.Errorf("Eqals() returned %v, want %v", got, want)
	}
}

func TestPath_NotEquals(t *testing.T) {
	ref := "/foo/baz"
	src := "/foo/bar"
	want := false

	p := Path(src)
	got := p.Equals(Path(ref))

	if got != want {
		t.Errorf("Eqals() returned %v, want %v", got, want)
	}
}
