package util

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func basicAuth(cred Credentials) string {
	auth := cred.Username + ":" + cred.Password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

type Credentials struct {
	Username string
	Password string
}

func Get(credentials Credentials, enpoint string, out interface{}) {
	req, err := http.NewRequest("GET", enpoint, bytes.NewBuffer(nil))
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Get"}).Error("Instancing request ", enpoint, " - ", err)
	}
	if credentials.Username != "" {
		req.Header.Add("Authorization", "Basic "+basicAuth(credentials))
	}
	req.Header.Set("Content-Type", "application/json")

	log.WithFields(log.Fields{"module": "util/http", "source": "Get"}).Debug("Request ", enpoint)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Get"}).Error("Request ", enpoint, " - ", err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, out)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Get"}).Error("Unable to parse reponse body - ", err)
	}
}

func Post(credentials Credentials, enpoint string, query []byte, out interface{}) {
	req, err := http.NewRequest("POST", enpoint, bytes.NewBuffer(query))
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Post"}).Error("Instancing request ", enpoint, " - ", err)
	}
	if credentials.Username != "" {
		req.Header.Add("Authorization", "Basic "+basicAuth(credentials))
	}
	req.Header.Set("Content-Type", "application/json")

	log.WithFields(log.Fields{"module": "util/http", "source": "Post"}).Debug("Request ", enpoint, " ", string(query))
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Post"}).Error("Request ", enpoint, " - ", err)
	}
	defer resp.Body.Close()

	if out != nil {
		body, _ := ioutil.ReadAll(resp.Body)

		err = json.Unmarshal(body, out)
		if err != nil {
			log.WithFields(log.Fields{"module": "util/http", "source": "Post"}).Error("Unable to parse reponse body - ", err)
		}
	}
}

func Put(credentials Credentials, enpoint string, query []byte, out interface{}) {
	req, err := http.NewRequest("PUT", enpoint, bytes.NewBuffer(query))
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Put"}).Error("Instancing request ", enpoint, " - ", err)
	}
	if credentials.Username != "" {
		req.Header.Add("Authorization", "Basic "+basicAuth(credentials))
	}
	req.Header.Set("Content-Type", "application/json")

	log.WithFields(log.Fields{"module": "util/http", "source": "Put"}).Debug("Request ", enpoint, " ", string(query))
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Put"}).Error("Request ", enpoint, " - ", err)
	}
	defer resp.Body.Close()

	if out != nil {
		body, _ := ioutil.ReadAll(resp.Body)

		err = json.Unmarshal(body, out)
		if err != nil {
			log.WithFields(log.Fields{"module": "util/http", "source": "Put"}).Error("Unable to parse reponse body - ", err)
		}
	}
}

func Delete(credentials Credentials, enpoint string, out interface{}) {
	req, err := http.NewRequest("DELETE", enpoint, bytes.NewBuffer(nil))
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Delete"}).Error("Instancing request ", enpoint, " - ", err)
	}
	if credentials.Username != "" {
		req.Header.Add("Authorization", "Basic "+basicAuth(credentials))
	}
	req.Header.Set("Content-Type", "application/json")

	log.WithFields(log.Fields{"module": "util/http", "source": "Delete"}).Debug("Request ", enpoint)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Delete"}).Error("Request ", enpoint, " - ", err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, out)
	if err != nil {
		log.WithFields(log.Fields{"module": "util/http", "source": "Delete"}).Error("Unable to parse reponse body - ", err)
	}
}
