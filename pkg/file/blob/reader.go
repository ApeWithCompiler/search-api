package blob

import (
	"os"
)

// RandomAccessReader is used for reading from a blob at random patterns
type RandomAccessReader struct {
	f *os.File
}

func NewReader() RandomAccessReader {
	return RandomAccessReader{}
}

// Open same as os.Open
func (r *RandomAccessReader) Open(path string) error {
	f, err := os.Open(path)
	r.f = f

	return err
}

// Close same as (os) f.Close
func (r *RandomAccessReader) Close() error {
	return r.f.Close()
}

// ReadBytes reads bytes from a offset. Returns byte array, size read and error
func (r *RandomAccessReader) ReadBytes(offset, size int64) ([]byte, int, error) {
	b := make([]byte, size)

	n, err := r.f.ReadAt(b, offset)

	return b, n, err
}

// ReadString reads string from a offset. Returns string, size read and error
func (r *RandomAccessReader) ReadString(offset, size int64) (string, int, error) {
	b, n, err := r.ReadBytes(offset, size)
	if err != nil {
		return "", n, err
	}

	return string(b[:n]), n, nil
}
