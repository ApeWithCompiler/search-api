package lmap

import (
	"os"
	"bufio"
)

// ReadFile reads a file and outputs its line as a map
func ReadFile(path string, size int) (map[string]bool, error) {
	m := make(map[string]bool, size)

	file, err := os.Open(path)
	if err != nil {
		return m, err
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		m[scanner.Text()] = true
	}

	return m, scanner.Err()
}

// Lmap is a wrapper to provide map lookups some syntactic sugar and prevent mutations
type Lmap struct {
	m map[string]bool
}

// NewLmap creates a new lmap from a go map
func NewLmap(m map[string]bool) Lmap {
	return Lmap{
		m: m,
	}
}

// ContainsString checks if lmap contains a string
func (l *Lmap) ContainsString(s string) bool {
	_, ok := l.m[s]

	return ok
}
