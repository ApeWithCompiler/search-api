package interpreter

import (
	"context"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	log "github.com/sirupsen/logrus"
	giql "gitlab.com/ApeWithCompiler/giql"

	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/requestid"
)

var (
	queryInterpreterCount = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "search_query_interpreter_count",
			Help: "Count of interpreted search queries",
		},
		[]string{"status"})

	queryInterpreterDuration = promauto.NewSummary(
		prometheus.SummaryOpts{
			Name:       "search_query_interpreter_duration_s",
			Help:       "Duration of interpreting search queries in seconds",
			Objectives: map[float64]float64{0.01: 0.001, 0.05: 0.005, 0.5: 0.05, 0.90: 0.01, 0.99: 0.001},
		})
)

func InterpretQuery(ctx context.Context, query string) search.Query {
	reqID := requestid.FromContext(ctx)

	log.WithFields(log.Fields{"module": "interpreter", "request-id": reqID}).Debug("Interpreting query ", query)

	interpretation_start := time.Now()

	scanner := giql.NewScanner(query)
	tokens := scanner.ScanTokens()

	parser := giql.NewParser(tokens)
	exprs := parser.Parse()

	queryInterpreterCount.With(prometheus.Labels{"status": "total"}).Inc()
	if parser.HasErrors() {
		log.WithFields(log.Fields{"module": "interpreter", "request-id": reqID}).Warning("Interpreter thrown errors, continiue with fallback")
		queryInterpreterCount.With(prometheus.Labels{"status": "error"}).Inc()
		fallbackParser := giql.NewFallbackParser(tokens)
		exprs = fallbackParser.Parse()
	}

	interpretation_duration := time.Since(interpretation_start)
	queryInterpreterDuration.Observe(float64(interpretation_duration.Seconds()))

	return search.Query{
		Raw:         query,
		Expressions: exprs,
	}
}
