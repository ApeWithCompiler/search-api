package interpreter

import (
	"context"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

type Interpreter interface {
	InterpretQuery(ctx context.Context, query string) search.Query
}
