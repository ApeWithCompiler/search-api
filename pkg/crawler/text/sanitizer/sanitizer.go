package sanitizer

type Sanitizer interface {
	Transform(input string) string
}
