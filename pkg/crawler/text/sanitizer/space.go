package sanitizer

import (
	"strings"
)

type SpaceSanitizer struct {
}

func (s *SpaceSanitizer) Transform(input string) string {
	input = strings.Join(strings.Fields(input), " ")
	input = strings.TrimSpace(input)

	return input
}
