package sanitizer

import (
	"strings"
)

type NewLineSanitizer struct {
}

func (s *NewLineSanitizer) Transform(input string) string {
	input = strings.Replace(input, "\n", "", -1)

	return input
}
