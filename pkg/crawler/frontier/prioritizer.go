package frontier

import (
	"context"
	"fmt"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
)

// PrioritizerFrontier provides a fixed size FIFO task queue for the crawler
type PrioritizerFrontier struct {
	p1 chan task.Task
	p2 chan task.Task
	p3 chan task.Task
}

func NewPrioritizerFrontier(capacity uint) PrioritizerFrontier {
	return PrioritizerFrontier{
		p1: make(chan task.Task, capacity),
		p2: make(chan task.Task, capacity),
		p3: make(chan task.Task, capacity),
	}
}

// QueueTask adds a task to the queue. Throws error if queue full
func (f *PrioritizerFrontier) QueueTaskPriority1(t task.Task) error {
	select {
	case f.p1 <- t:
	default:
		return fmt.Errorf("Can't insert task, queue full")
	}
	return nil
}

// QueueTask adds a task to the queue. Throws error if queue full
func (f *PrioritizerFrontier) QueueTaskPriority2(t task.Task) error {
	select {
	case f.p2 <- t:
	default:
		return fmt.Errorf("Can't insert task, queue full")
	}
	return nil
}

// QueueTask adds a task to the queue. Throws error if queue full
func (f *PrioritizerFrontier) QueueTaskPriority3(t task.Task) error {
	select {
	case f.p3 <- t:
	default:
		return fmt.Errorf("Can't insert task, queue full")
	}
	return nil
}

// QueueTask adds a task to the queue. Throws error if queue full
func (f *PrioritizerFrontier) QueueTask(t task.Task) error {
	return f.QueueTaskPriority3(t)
}// DequeueTask returns next task in the queue. Blocks if queue empty

func (f *PrioritizerFrontier) DequeueTask() (*task.Task, error) {
	if len(f.p1) > 0 {
		t := <- f.p1
		return &t, nil
	}

	if len(f.p2) > 0 {
		t := <- f.p2
		return &t, nil
	}

	select {
	case t := <- f.p1:
		return &t, nil 
	case t := <- f.p2:
		return &t, nil 
	case t := <- f.p3:
		return &t, nil 
	}

}

// DequeueTaskContext returns next task in queue. Blocks if queue empty, provides context for timeout
func (f *PrioritizerFrontier) DequeueTaskContext(ctx context.Context) (*task.Task, error) {
	if len(f.p1) > 0 {
		t := <- f.p1
		return &t, nil
	}

	if len(f.p2) > 0 {
		t := <- f.p2
		return &t, nil
	}

	select {
	case <- ctx.Done():
		return nil, ctx.Err()
	case t := <- f.p1:
		return &t, nil 
	case t := <- f.p2:
		return &t, nil 
	case t := <- f.p3:
		return &t, nil 
	}
}

// ForNext executes callback each time a task may be dequeued
func (f *PrioritizerFrontier) ForNext(yield func(*task.Task, error) bool) bool {
	for f.HasTask() {
		t, err := f.DequeueTask()
		if err != nil {
			return false
		}

		if !yield(t, err) {
			return false
		}
	}
	return true
}

func (f *PrioritizerFrontier) HasTask() bool {
	return (len(f.p1) > 0 || len(f.p2) > 0 || len(f.p3) > 0)
}
