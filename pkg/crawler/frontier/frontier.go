package frontier

import (
	"context"
	"fmt"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
)

// Frontier provides a fixed size FIFO task queue for the crawler
type Frontier struct {
	c chan task.Task
}

func NewFrontier(capacity uint) Frontier {
	return Frontier{
		c: make(chan task.Task, capacity),
	}
}

// QueueTask adds a task to the queue. Throws error if queue full
func (f *Frontier) QueueTask(t task.Task) error {
	select {
	case f.c <- t:
	default:
		return fmt.Errorf("Can't insert task, queue full")
	}
	return nil
}

// DequeueTask returns next task in the queue. Blocks if queue empty
func (f *Frontier) DequeueTask() (task.Task, error) {
	return <- f.c, nil
}

// DequeueTaskContext returns next task in queue. Blocks if queue empty, provides context for timeout
func (f *Frontier) DequeueTaskContext(ctx context.Context) (*task.Task, error) {
	select {
	case <- ctx.Done():
		return nil, ctx.Err()
	case t := <- f.c:
		return &t, nil 
	}
}

// ForNext executes callback each time a task may be dequeued
func (f *Frontier) ForNext(yield func(task.Task, error) bool) bool {
	for t := range f.c {
		if !yield(t, nil) {
			return false
		}
	}
	return true
}
