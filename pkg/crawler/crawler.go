package crawler

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/candidate"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/filter"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/frontier"
	crawlermodel "gitlab.com/apewithcompiler/heru/pkg/crawler/model"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/sitemap"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/storage"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
	"gitlab.com/apewithcompiler/heru/pkg/model"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewCrawlStorageModel(depth uint64, seeds []web.DigestUrl, created time.Time) crawlermodel.Crawl {
	return crawlermodel.Crawl{
		Depth: depth,
		Seeds: seeds,
		Created: created,
	}
}

func NewScrapeStorageModel(crawlId int64, url web.DigestUrl, file document.DocumentData, created time.Time) crawlermodel.Scrape {
	return crawlermodel.Scrape{
		CrawlId: crawlId,
		Url: url,
		File: file,
		Created: created,
	}
}

type Crawler struct {
	webScraper      scraper.WebScraper
	robotsScraper   scraper.RobotsScraper
	sitemapScraper  scraper.SitemapScraper

	frontier        frontier.PrioritizerFrontier
	scrapeTimeout   time.Duration

	candidateSelector    candidate.CandidateSelector
	candidatePrioritizer candidate.CandidatePrioritizer

	urlTable        filter.LookupTable
	hostTable       filter.LookupTable

	storage         storage.CrawlerStorage

	logRejected bool
	logger      log.Entry
}

func (c *Crawler) Crawl(ctx context.Context, depth uint64, urls []web.DigestUrl) error {
	crawlId, err := c.storage.CreateCrawl(NewCrawlStorageModel(depth, urls, time.Now()))
	if err != nil {
		c.logger.Error("Storage error for crawl: ", err)
		return err
	}

	for _, u := range urls {
		err = c.frontier.QueueTask(task.NewAnchorFollowTask(ctx, crawlId, depth, u))
		if err != nil {
			c.logger.Error(err)
			return err
		}
	}

	return nil
}

func (c *Crawler) CrawlSitemap(ctx context.Context, urls []web.DigestUrl) error {
	crawlId, err := c.storage.CreateCrawl(NewCrawlStorageModel(0, urls, time.Now()))
	if err != nil {
		c.logger.Error("Storage error for crawl: ", err)
		return err
	}

	for _, u := range urls {
		err = c.frontier.QueueTask(task.NewSitemapTask(ctx, crawlId, u))
		if err != nil {
			c.logger.Error(err)
			return err
		}
	}

	return nil
}

func (c *Crawler) Run() {
	c.logger.Info("Thread started")
	defer c.logger.Info("Thread terminated")

	c.frontier.ForNext(func(t *task.Task, err error) bool {
		if err != nil {
			c.logger.Error("Error receiving next task: ", err.Error())
			return false
		}
		c.logger.Info("Got task: ", t.Url.String())

		if !c.candidateSelector.ShouldScrapeCandidate(*t) {
			c.logger.Info("Scraping task rejected by candidate selector: ", t.Url.String())
			return false
		}

		if t.Type == task.CRAWL_ANCHORFOLLOW {
			doc, err := c.scrapeTask(*t)
			if err != nil {
				c.logger.Error("Failed scraping task")
			}

			if doc != nil {
				err = c.storeTask(*t, *doc)
				if err != nil {
					c.logger.Error("Failed storing task data: ", err)
				}

				if !t.IsMaxDepth() {
					err = c.followAnchors(*t, *doc)
					if err != nil {
						c.logger.Error("Failed following anchors: ", err)
					}
				}

			} else {
				c.logger.Error("Task returned no data")
			}
		}

		if t.Type == task.CRAWL_SITEMAP {
			sitemaps, err := c.scrapeSitemaps(*t)
			if err != nil {
				c.logger.Error("Failed scraping task")
			}

			for _, sm := range sitemaps {
				err = c.followSitemap(*t, sm)
				if err != nil {
					c.logger.Error("Failed following sitemap: ", err)
				}
			}
		}

		time.Sleep(c.scrapeTimeout)

		return true
	})
}

func (c *Crawler) scrapeTask(t task.Task) (*scraper.ScraperWebDocument, error) {
	doc, err := c.webScraper.Scrape(t.Ctx, t.Url)
	if err != nil {
		c.logger.Error("Error scraping document: ", err.Error())
		return nil, err
	}

	if doc.WasRedirected() {
		c.logger.Info("Redirected while scrape to: ", doc.ResponseUrl())
	}

	return &doc, nil
}

func (c *Crawler) storeTask(t task.Task, doc scraper.ScraperWebDocument) error {
	data, err := doc.ToDocumentData()
	if err != nil {
		c.logger.Error("Failed getting document data: ", err)
		return err
	}

	_, err = c.storage.CreateScrape(NewScrapeStorageModel(t.CrawlId, doc.ResponseUrl(), data, time.Now()))
	if err != nil {
		c.logger.Error("Storage error for scrape: ", err)
	}

	return nil
}

func (c *Crawler) followAnchors(t task.Task, doc scraper.ScraperWebDocument) error {
	anchors, err := doc.Anchors()
	if err != nil {
		c.logger.Error("Error parsing anchors: ", err.Error())
		return err
	}

	for i := range anchors {
		edge, err := web.NewAnchorEdgeFromAnchor(doc.ResponseUrl(), anchors[i])
		if err != nil {
			c.logger.Warn("Failed converting anchor to edge:", err.Error())
			continue
		}

		if c.candidateSelector.ShouldQueueCandidate(t, doc, edge) {
			err = c.queueAnchor(t, edge)
			if err != nil {
				c.logger.Error("Error queueuing anchors")
				return err
			}
		} else {
			if c.logRejected {
				c.logger.Info("Queueing anchor rejected by candidate selector: ", anchors[i].Href())
			}
		}
	}

	return nil
}

func (c *Crawler) queueAnchor(t task.Task, anchor web.AnchorEdge) error {
	child := t.Fork(*anchor.VertexTo())
	var err error
	switch c.candidatePrioritizer.AssignPriority(t, anchor) {
	case 1:
		c.logger.Info("Queueing anchor as Prio1: ", child.Url.String())
		err = c.frontier.QueueTaskPriority1(child)
	case 2:
		c.logger.Info("Queueing anchor as Prio2: ", child.Url.String())
		err = c.frontier.QueueTaskPriority2(child)
	case 3:
		c.logger.Info("Queueing anchor as Prio3: ", child.Url.String())
		err = c.frontier.QueueTaskPriority3(child)
	default:
		c.logger.Info("Queueing anchor as unspecified Prio: ", child.Url.String())
		err = c.frontier.QueueTask(child)
	}
	if err != nil {
		c.logger.Error("Error adding task to frontier: ", err.Error())
		return err
	}

	return nil
}

func (c *Crawler) scrapeHost(ctx context.Context, host web.DigestHost) (model.HostReference, error) {
	hostRef := model.NewHostReference(host)

	robotsUrl, err := scraper.GetRobotsUrlForHost(host)
	if err != nil {
		return hostRef, err
	}

	robotsDoc, err := c.robotsScraper.Scrape(ctx, robotsUrl)

	if err != nil {
		return hostRef, err
	}

	hostRef.SetRobots(robotsUrl)
	
	if robotsDoc.HasSitemaps() {
		for _, s := range robotsDoc.Sitemaps() {
			u, err := web.UrlFromString(s)
			if err != nil {
				c.logger.Warn("Error converting sitemap url: ", err)
				continue
			}

			hostRef.AddSitemap(u)
		}
	}

	return hostRef, nil
}

func (c *Crawler) scrapeSitemaps(t task.Task) ([]sitemap.Sitemap, error) {
	robotsUrl, err := scraper.GetRobotsUrlFor(t.Url)
	if err != nil {
		return []sitemap.Sitemap{}, err
	}

	robotsDoc, err := c.robotsScraper.Scrape(t.Ctx, robotsUrl)
	if err != nil {
		c.logger.Error("Error scraping robots url:" , err.Error())
		return []sitemap.Sitemap{}, err
	}

	if !robotsDoc.HasSitemaps() {
		c.logger.Info("No sitemap in robots defined")
		return []sitemap.Sitemap{}, nil
	}

	var sitemaps []sitemap.Sitemap
	for _, sm := range robotsDoc.Sitemaps() {
		sitemapUrl, err := web.UrlFromString(sm)
		if err != nil {
			c.logger.Warn("Failed parsing sitemap url: ", err.Error())
			continue
		}

		sitemapDoc, err := c.sitemapScraper.Scrape(t.Ctx, sitemapUrl)
		if err != nil {
			c.logger.Error("Error scraping robots sitemap:" , err.Error())
			continue
		}

		sitemaps = append(sitemaps, sitemapDoc)
	}

	return sitemaps, nil
}

func (c *Crawler) followSitemap(t task.Task, sm sitemap.Sitemap) error {
	for i := range sm.Urls {
		err := c.queueUrl(t, sm.Urls[i].Loc)
		if err != nil {
			c.logger.Error("Error queueuing anchors")
			return err
		}

	}

	return nil
}

func (c *Crawler) queueUrl(t task.Task, url web.DigestUrl) error {
	err := c.frontier.QueueTask(task.NewAnchorFollowTask(t.Ctx, t.CrawlId, 0, url))
	if err != nil {
		c.logger.Error("Error adding task to frontier: ", err.Error())
		return err
	}

	return nil
}

func (c *Crawler) onNewUrlDiscovered(t task.Task, url web.DigestUrl, f func(t task.Task, url web.DigestUrl)) {
	if !c.urlTable.HasEntry(&url) {
		c.urlTable.AddEntry(&url)	
		f(t, url)
	}
}

func (c *Crawler) onNewHostDiscovered(t task.Task, host web.DigestHost, f func(t task.Task, url web.DigestHost)) {
	if !c.hostTable.HasEntry(&host) {
		c.hostTable.AddEntry(&host)	
		f(t, host)
	}
}
