package profile

import (
)

type CrawlerProfile struct {
	CrawlDepth   int
	IgnoreRobots bool
	MaxDepth     uint64
	MaxRetries   uint16
}

func (p *CrawlerProfile) ObeyRobots() bool {
	return !p.IgnoreRobots
}
