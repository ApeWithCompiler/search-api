package model

type FileKey string

func FileKeyFromString(k string) FileKey {
	return FileKey(k)
}

func (d FileKey) String() string {
	return string(d)
}
