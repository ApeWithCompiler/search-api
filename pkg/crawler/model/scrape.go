package model

import (
	"fmt"
	"time"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

type Scrape struct {
	CrawlId int64
	Url     web.DigestUrl
	FileRef fmt.Stringer
	File    document.DocumentData
	Created time.Time
}
