package model

import (
	"time"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type Crawl struct {
	Depth   uint64
	Seeds   []web.DigestUrl
	Created time.Time
}
