package filter

import (
	"sync"
	"fmt"
)

type LookupTable struct {
	mux sync.RWMutex
	m   map[string]bool
}

func NewLookupTable(capacity uint) LookupTable {
	return LookupTable{
		mux: sync.RWMutex{},
		m:   make(map[string]bool, capacity),
	}
}

func (f *LookupTable) HasEntry(e fmt.Stringer) bool {
	f.mux.RLock()
	defer f.mux.RUnlock()

	_, ok := f.m[e.String()]
	return ok
}

func (f *LookupTable) AddEntry(e fmt.Stringer) {
	f.mux.Lock()
	defer f.mux.Unlock()

	f.m[e.String()] = true
}
