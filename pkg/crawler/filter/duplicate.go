package filter

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type DuplicateFilter struct {
	l LookupTable
}

func NewDuplicateFilter(capacity uint) DuplicateFilter {
	return DuplicateFilter{
		l: NewLookupTable(capacity),
	}
}

func (f *DuplicateFilter) IsDuplicate(u web.DigestUrl) bool {
	return f.l.HasEntry(&u)
}

func (f *DuplicateFilter) AddUrl(u web.DigestUrl) {
	f.l.AddEntry(&u)
}
