package filter

import (
	"context"

	"sync"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/robots"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type RobotsFilter struct {
	agent string

	// Saves the groups so it can be used between requests
	mux   sync.RWMutex
	cache map[string]robots.RobotsDocument

	logger log.Entry
	scraper scraper.RobotsScraper
}

func NewRobotsFilter() RobotsFilter {
	logger := log.WithFields(log.Fields{"module": "robots"})

	return RobotsFilter{
		agent:  http.DEFAULT_USER_AGENT,
		mux:    sync.RWMutex{},
		cache:  make(map[string]robots.RobotsDocument),
		logger: *logger,
		scraper: scraper.NewRobotsScraper(),
	}
}

func (s *RobotsFilter) IsAllowed(ctx context.Context, url web.DigestUrl) bool {
	if !url.HasHostname() {
		s.logger.Warn("Invalid url: ", url.String())
		return false
	}

	entry, exists := s.getGroup(url.Hostname())
	if !exists {
		s.logger.Debug("Need to request robots for: ", url.Hostname())
		robotsUrl, err := scraper.GetRobotsUrlFor(url)	
		if err != nil {
			s.logger.Warn("Unable to build robots url for: ", url.String(), " - ", err)
			return false
		}

		doc, err := s.scraper.Scrape(ctx, robotsUrl)	
		if err != nil {
			s.logger.Warn("Unable to scrape robots: ", robotsUrl.String(), " - ", err)
		}

		// insert response anyway.
		// serves as a marker and returns a result anyway.
		s.addGroup(url.Hostname(), doc)
		entry = doc
	}

	return entry.IsAllowed(s.agent, url.Path())
}

func (s *RobotsFilter) getGroup(domain string) (robots.RobotsDocument, bool) {
	s.mux.RLock()
	defer s.mux.RUnlock()

	entry, exist := s.cache[domain]
	return entry, exist
}

func (s *RobotsFilter) addGroup(domain string, entry robots.RobotsDocument) {
	s.mux.Lock()
	defer s.mux.Unlock()

	s.cache[domain] = entry
}
