package candidate

import (
	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
)

type CandidateSelector interface {
	ShouldScrapeCandidate(task task.Task) bool
	ShouldQueueCandidate(task task.Task, doc scraper.ScraperWebDocument, anchor web.AnchorEdge) bool
}
