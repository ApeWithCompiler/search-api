package candidate

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/filter"
)

type DefaultCandidateSelector struct {
	duplicateFilter filter.DuplicateFilter
	robotsFilter    filter.RobotsFilter

	logger log.Entry

	followSameSite bool
}

func NewDefaultCandidateSelector() *DefaultCandidateSelector {
	return &DefaultCandidateSelector {
		robotsFilter: filter.NewRobotsFilter(),
		duplicateFilter: filter.NewDuplicateFilter(1000),
		logger: *log.WithFields(log.Fields{"module": "candidateselector"}),
		followSameSite: true,
	}
}

func (s *DefaultCandidateSelector) ShouldScrapeCandidate(task task.Task) bool {
	return true
}

func (s *DefaultCandidateSelector) ShouldQueueCandidate(task task.Task, doc scraper.ScraperWebDocument, anchor web.AnchorEdge) bool {
	if s.duplicateFilter.IsDuplicate(*anchor.VertexTo()) {
		s.logger.Debug("Reject duplicate: ", anchor.VertexTo().String())
		return false
	}
	
	if !s.robotsFilter.IsAllowed(task.Ctx, *anchor.VertexTo()) {
		s.logger.Debug("Reject robots forbidden: ", anchor.VertexTo().String())
		return false
	}

	if !s.isOutbound(anchor) && !s.followSameSite{
		s.logger.Debug("Reject no outbound: ", anchor.VertexTo().String())
		return false
	}

	s.duplicateFilter.AddUrl(*anchor.VertexTo())
	return true
}

func (s *DefaultCandidateSelector) isOutbound(anchor web.AnchorEdge) bool {
	return !anchor.VertexFrom().HostEquals(*anchor.VertexTo())
}

type DefaultCandidatePrioritizer struct {}

func NewDefaultCandidatePrioritizer() *DefaultCandidatePrioritizer {
	return &DefaultCandidatePrioritizer{}	
}

func (p *DefaultCandidatePrioritizer) AssignPriority(task task.Task, anchor web.AnchorEdge) int {
	if anchor.Label() != "" {
		queryParams := anchor.VertexTo().QueryParams()
		if len(queryParams) == 0 {
			return 1
		}

		if len(queryParams) <= 2 {
			for _, p := range queryParams {
				if p.Key == "source" {
					return 3
				}

				if len(p.Value) > 5 {
					return 3
				}
			}

			return 2
		}
	}

	return 3
}
