package candidate


import (
	"gitlab.com/apewithcompiler/heru/pkg/crawler/task"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type CandidatePrioritizer interface {
	AssignPriority(task task.Task, anchor web.AnchorEdge) int
}
