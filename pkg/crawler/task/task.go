package task

import (
	"context"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type TaskType int

const (
	CRAWL_ANCHORFOLLOW	TaskType = 0
	CRAWL_SITEMAP		TaskType = 1
)

type Task struct {
	Ctx     context.Context
	CrawlId int64
	Depth   uint
	Url     web.DigestUrl
	Type    TaskType
	Options Options
}

func NewAnchorFollowTask(ctx context.Context, crawlId int64, depth uint64, u web.DigestUrl) Task {
	return Task{
		Ctx:     ctx,
		CrawlId: crawlId,
		Url:     u,
		Type:    CRAWL_ANCHORFOLLOW,
		Options: Options{
			MaxDepth: depth,
		},
	}
}

func NewSitemapTask(ctx context.Context, crawlId int64, u web.DigestUrl) Task {
	return Task{
		Ctx:     ctx,
		CrawlId: crawlId,
		Url:     u,
		Type:    CRAWL_SITEMAP,
		Options: Options{
			MaxDepth: 0,
		},
	}
}

func (t *Task) Fork(u web.DigestUrl) Task {
	return Task{
		Ctx:     t.Ctx,
		CrawlId: t.CrawlId,
		Depth:   t.Depth + 1,
		Type:	 t.Type,
		Options: t.Options,
		Url:     u,
	}
}

func (t *Task) IsMaxDepth() bool {
	return t.Depth >= uint(t.Options.MaxDepth)
}
