package task

type Options struct {
	IgnoreRobots bool
	MaxDepth     uint64
	MaxRetries   uint16
}
