package scraper

import (
	"context"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/html"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

// ScraperWebDocument encapsulates information of single scrape process instance
// Main reason is the independend lifetime of Response.Body, so any kind of parsing can be done
// from a single response.
// Embeding this in to scrape would require all parsing to be done in one or every parsing require a
// seperate load. This would be "inefficent" against websites.
type ScraperWebDocument struct {
	HeadParser   html.HtmlHeadParser
	AnchorParser html.HtmlAnchorParser

	Context  context.Context
	Request  http.HttpLoaderRequest
	Response http.HttpLoaderResponse
}

func (d *ScraperWebDocument) Head() (document.DocumentHead, error) {
	head, err := d.HeadParser.Parse(d.Context, d.Response.Url, d.Response.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "webscraper",
			"url":    d.Response.Url.String(),
		}).Error("Error parsing document head: ", err)

		return document.DocumentHead{}, err
	}

	return head, nil

}

func (d *ScraperWebDocument) Anchors() ([]web.Anchor, error) {
	anchors, err := d.AnchorParser.Parse(d.Context, d.Response.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "webscraper",
			"url":    d.Response.Url.String(),
		}).Error("Error parsing document anchor: ", err)

		return anchors, err
	}

	return anchors, nil
}

func (d *ScraperWebDocument) ToDocumentData() (document.DocumentData, error) {
	data := document.DocumentDataFromBytes(d.Response.Body)
	head, err := d.Head()
	if err == nil {
		data.AddHead(head)
	}

	return data, nil
}

func (d *ScraperWebDocument) RequestUrl() web.DigestUrl {
	return d.Response.RequestUrl()
}

func (d *ScraperWebDocument) ResponseUrl() web.DigestUrl {
	return d.Response.ResponseUrl()
}

func (d *ScraperWebDocument) WasRedirected() bool {
	return d.Response.WasRedirected()
}

func NewWebScraper() WebScraper {
	loader := http.NewHttpLoaderImpl()

	return WebScraper{
		Loader:       &loader,
		HeadParser:   html.NewHtmlHeadParser(),
		AnchorParser: html.NewHtmlAnchorParser(),
	}
}

// WebScraper implements a general scraper for webpages.
// This means combining a http loader with html parsers.
type WebScraper struct {
	Loader       http.HttpLoader
	HeadParser   html.HtmlHeadParser
	AnchorParser html.HtmlAnchorParser
}

func (s *WebScraper) Scrape(ctx context.Context, url web.DigestUrl) (ScraperWebDocument, error) {
	req := http.RequestFromUrl(url)
	resp, err := s.Loader.Load(ctx, req)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "webscraper",
			"url":    url.String(),
		}).Error("Error loading document: ", err)

		return ScraperWebDocument{}, err
	}

	doc := ScraperWebDocument{
		HeadParser:   s.HeadParser,
		AnchorParser: s.AnchorParser,
		Context:      ctx,
		Request:      req,
		Response:     resp,
	}

	return doc, nil
}
