package scraper

import (
	"context"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/robots"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

//GetRobotsUrlFor is a helper class returning a /robots.txt url for the domain of the input url
func GetRobotsUrlFor(url web.DigestUrl) (web.DigestUrl,error) {
	return web.NewUrlBuilderFromDigest(url).
		SetPath("/robots.txt").
		DigestUrl()
}

//GetRobotsUrlFor is a helper class returning a /robots.txt url for the domain of the input url
func GetRobotsUrlForHost(host web.DigestHost) (web.DigestUrl, error) {
	return web.NewUrlBuilder().
		SetScheme("https").
		SetHost(host.String()).
		SetPath("/robots.txt").
		DigestUrl()
}

// RobotsScraper implements a general scraper for sitemaps
// It combines a http loader together with the specified sitemap parser
type RobotsScraper struct {
	Loader http.HttpLoader
	Parser robots.RobotsParser
}

func NewRobotsScraper() RobotsScraper {
	loader := http.NewHttpLoaderImpl()

	return RobotsScraper{
		Loader: &loader,
		Parser: robots.NewRobotsParser(),
	}
}

func (s *RobotsScraper) Scrape(ctx context.Context, url web.DigestUrl) (robots.RobotsDocument, error) {
	req := http.RequestFromUrl(url)
	resp, err := s.Loader.Load(ctx, req)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "sitemapscraper",
			"url":    url.String(),
		}).Error("Error loading document: ", err)

		return robots.RobotsDocument{}, err
	}

	result, err := s.Parser.Parse(ctx, resp.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "sitemapscraper",
			"url":    url.String(),
		}).Error("Error parsing document: ", err)
	
		return robots.RobotsDocument{}, err
	}

	return result, nil
}
