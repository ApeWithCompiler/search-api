package scraper

import (
	"context"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/sitemap"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

// SitemapScraper implements a general scraper for sitemaps
// It combines a http loader together with the specified sitemap parser
type SitemapScraper struct {
	Loader http.HttpLoader
	Parser sitemap.SitemapTypeAdapter
}

func NewSitemapScraper() SitemapScraper {
	loader := http.NewHttpLoaderImpl()

	return SitemapScraper{
		Loader: &loader,
		Parser: sitemap.NewSitemapTypeAdapter(sitemap.NewSitemapParser()),
	}
}

func (s *SitemapScraper) Scrape(ctx context.Context, url web.DigestUrl) (sitemap.Sitemap, error) {
	req := http.RequestFromUrl(url)
	resp, err := s.Loader.Load(ctx, req)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "sitemapscraper",
			"url":    url.String(),
		}).Error("Error loading document: ", err)

		return sitemap.Sitemap{}, err
	}

	result, err := s.Parser.Parse(ctx, resp.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "sitemapscraper",
			"url":    url.String(),
		}).Error("Error parsing document: ", err)
	
		return sitemap.Sitemap{}, err
	}

	return result, nil
}
