package storage

import (
	"gitlab.com/apewithcompiler/heru/pkg/crawler/model"
)

type CrawlerStorage interface {
	CreateCrawl(crawl model.Crawl) (int64, error)
	CreateScrape(scrape model.Scrape) (int64, error)
}
