package crawler

import (
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/candidate"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/filter"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/frontier"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/storage"
)

type CrawlerBuilder struct {
	frontier	     frontier.PrioritizerFrontier
	candidateSelector    candidate.CandidateSelector
	candidatePrioritizer candidate.CandidatePrioritizer
	duplicateFilter      filter.DuplicateFilter
	scrapeTimeout        time.Duration
	storage              storage.CrawlerStorage

	urlTableCapacity  uint
	hostTableCapacity uint

	logger log.Entry
}

func NewCrawlerBuilder() *CrawlerBuilder {
	return &CrawlerBuilder{
		frontier:  frontier.NewPrioritizerFrontier(1000),
		scrapeTimeout:     time.Duration(1000) * time.Millisecond,
		candidateSelector: candidate.NewDefaultCandidateSelector(),
		candidatePrioritizer: candidate.NewDefaultCandidatePrioritizer(),
		urlTableCapacity: 50000,
		hostTableCapacity: 200,
		logger:            *log.WithFields(log.Fields{"module": "crawler"}),
	}
}

func (b *CrawlerBuilder) WithChannelFrontier(capacity uint) *CrawlerBuilder {
	b.frontier = frontier.NewPrioritizerFrontier(1000)
	return b
}

func (b *CrawlerBuilder) WithCandidateSelector(selector candidate.CandidateSelector) *CrawlerBuilder {
	b.candidateSelector = selector
	return b
}

func (b *CrawlerBuilder) SetScrapeTimeout(timeout time.Duration) *CrawlerBuilder {
	b.scrapeTimeout = timeout
	return b
}

func (b *CrawlerBuilder) SetStorage(storage storage.CrawlerStorage) *CrawlerBuilder {
	b.storage = storage
	return b
}

func (b *CrawlerBuilder) SetUrlTableCapacity(capacity uint) *CrawlerBuilder {
	b.urlTableCapacity = capacity
	return b
}

func (b *CrawlerBuilder) SetHostTableCapacity(capacity uint) *CrawlerBuilder {
	b.hostTableCapacity = capacity
	return b
}

func (b *CrawlerBuilder) Build() Crawler {
	return Crawler{
		webScraper:      scraper.NewWebScraper(),
		robotsScraper:   scraper.NewRobotsScraper(),
		sitemapScraper:  scraper.NewSitemapScraper(),
		frontier:        b.frontier,
		scrapeTimeout:   b.scrapeTimeout,
		candidateSelector: b.candidateSelector,
		candidatePrioritizer: b.candidatePrioritizer,
		urlTable:        filter.NewLookupTable(b.urlTableCapacity),
		hostTable:       filter.NewLookupTable(b.hostTableCapacity),
		logger:          b.logger,
		storage:	 b.storage,
	}
}
