package http

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"net/http"
	"strings"
)

type HttpLoaderResponse struct {
	Url         web.DigestUrl
	Body        []byte
	Req         HttpLoaderRequest
	Resp        http.Response
	ScraperIP   string
	RedirectCnt int
}

func (r *HttpLoaderResponse) RequestUrl() web.DigestUrl {
	return r.Req.Url
}

func (r *HttpLoaderResponse) ResponseUrl() web.DigestUrl {
	return r.Url
}

func (r *HttpLoaderResponse) GetHeader(key string) string {
	return r.Resp.Header.Get(key)
}

func (r *HttpLoaderResponse) WasRedirected() bool {
	return !r.Req.Url.Equals(r.Url)
}

func (r *HttpLoaderResponse) GetScraperIP() string {
	return r.ScraperIP
}

func (r *HttpLoaderResponse) DumpToString() string {
	var b strings.Builder

	b.WriteString(r.Resp.Proto)
	b.WriteString(" ")
	b.WriteString(r.Resp.Status)
	b.WriteString("\n")

	for k, v := range r.Resp.Header {
		b.WriteString(k)
		b.WriteString(": ")
		b.WriteString(strings.Join(v, " "))
		b.WriteString("\n")
	}

	b.WriteString("\n")

	if (len(r.Body) > 0) {
		b.WriteString(string(r.Body))
		b.WriteString("\n")
	}

	return b.String()
}
