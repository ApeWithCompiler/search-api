package http

const (
	// Header Keys
	HOST       string = "Host"
	USER_AGENT        = "User-Agent"

	ACCEPT          = "Accept"
	ACCEPT_LANGUAGE = "Accept-Language"
	ACCEPT_ENCODING = "Accept-Encoding"
	ACCEPT_CHARSET  = "Accept-Charset"

	CONTENT_LENGTH    = "Content-Length"
	CONTENT_TYPE      = "Content-Type"
	CONTENT_MD5       = "Content-MD5"
	CONTENT_LOCATION  = "Content-Location"
	CONTENT_ENCODING  = "Content-Encoding"
	TRANSFER_ENCODING = "Transfer-Encoding"
	PRAGMA            = "Pragma"
	CACHE_CONTROL     = "Cache-Control"

	DATE          = "Date"
	LAST_MODIFIED = "Last-Modified"
	SERVER        = "Server"

	ACCEPT_RANGES = "Accept-Ranges"
	CONTENT_RANGE = "Content-Range"
	RANGE         = "Range"

	LOCATION = "Location"
	ETAG     = "ETag"
	VIA      = "Via"

	X_FORWARDED_FOR = "X-Forwarded-For"
	X_ROBOTS_TAG    = "X-Robots-Tag"
	X_ROBOTS        = "X-Robots"

	X_YACY_INDEX_CONTROL     = "X-YaCy-Index-Control"
	X_YACY_TRANSACTION_TOKEN = "X-YaCy-Transaction-Token"

	SET_COOKIE  = "Set-Cookie"
	SET_COOKIE2 = "Set-Cookie2"
	EXPIRES     = "Expires"

	CORS_ALLOW_ORIGIN = "Access-Control-Allow-Origin"

	// Default values
	DEFAULT_USER_AGENT = "Heru/v0.0.1 (ApeWithCompiler Search Engine) crawler"

	DEFAULT_ACCEPT          = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	DEFAULT_ACCEPT_ENCODING = "gzip, deflate, br"
	DEFAULT_ACCEPT_LANGUAGE = "en-us,en;q=0.5"
	DEFAULT_ACCEPT_CHARSET  = "ISO-8859-1,utf-8;q=0.7,*;q=0.7"
)
