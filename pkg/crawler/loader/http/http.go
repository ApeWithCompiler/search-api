package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

func isRedirect(resp http.Response) bool {
	return resp.StatusCode == 302
}

func getRedirectLocation(resp http.Response) string {
	if !isRedirect(resp) {
		return ""
	}

	return resp.Header.Get("Location")
}

func httpGet(client http.Client, loaderRequest HttpLoaderRequest) (*http.Response, error) {
	req, err := http.NewRequest(loaderRequest.Method, loaderRequest.Url.String(), bytes.NewBuffer(nil))
	if err != nil {
		return nil, fmt.Errorf("Starting http request failed")
	}

	for k, v := range loaderRequest.Header {
		req.Header.Add(k, v)
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Executing http request failed")
	}

	if resp.StatusCode >= 400 {
		return resp, fmt.Errorf("HTTP status code indicates error: %v", resp.StatusCode)
	}

	return resp, nil
}

// HttpLoader describes a abstract http client.
// It is a interface, so its implementation can be swapped on demand.
// For example mocking for tests or if a particular deployment needs a specialized http client.
type HttpLoader interface {
	Load(ctx context.Context, loaderRequest HttpLoaderRequest) (HttpLoaderResponse, error)
}

func NewHttpLoaderImpl() HttpLoaderImpl {
	return HttpLoaderImpl{}
}

// HttpLoaderImpl is a http client for the crawler.
// It tries to abstract some details away, as setting headers for example.
type HttpLoaderImpl struct{}

func (h *HttpLoaderImpl) Load(ctx context.Context, loaderRequest HttpLoaderRequest) (HttpLoaderResponse, error) {

	logger := log.WithFields(log.Fields{
		"module": "loader/http",
		"url":    loaderRequest.Url.String(),
	})

	logger.Info("Loading http documment")

	client := http.Client{}

	resp, err := httpGet(client, loaderRequest)

	if err != nil {
		logger.Error(err)
		return HttpLoaderResponse{}, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		logger.Error("Reading http response body failed: ", err)
		return HttpLoaderResponse{}, fmt.Errorf("Reading http response body failed")

	}

	// Provide url after redirects as the response url
	resUrl, err := web.UrlFromString(resp.Request.URL.String())
	if err != nil {
		logger.Error("Parsing response url failed: ", err) 
		return HttpLoaderResponse{}, fmt.Errorf("Parsing response url failed")
	}

	return HttpLoaderResponse{
		Url:         resUrl,
		Body:        body,
		Req:         loaderRequest,
		Resp:        *resp,
		RedirectCnt: 0,
	}, nil
}

// HttpFileDownloader implements a http client for donloading files to the local device.
type HttpFileDownloader struct{}

func (h *HttpFileDownloader) Load(ctx context.Context, loaderRequest HttpLoaderRequest, destination string) error {
	logger := log.WithFields(log.Fields{
		"module": "loader/http",
		"url":    loaderRequest.Url.String(),
	})

	logger.Info("Loading http documment")

	req, err := http.NewRequest("GET", loaderRequest.Url.String(), bytes.NewBuffer(nil))
	if err != nil {
		logger.Error("Starting http request failed: ", err)
		return fmt.Errorf("Starting http request failed")
	}

	for k, v := range loaderRequest.Header {
		req.Header.Add(k, v)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error("Executing http request failed: ", err)
		return fmt.Errorf("Executing http request failed")
	}

	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
