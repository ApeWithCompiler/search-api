package http

import (
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"strings"
)

const (
	GET   string = "GET"
	POST         = "POST"
	PATCH        = "PATCH"
)

func RequestFromUrl(url web.DigestUrl) HttpLoaderRequest {
	header := make(map[string]string)

	header[USER_AGENT] = DEFAULT_USER_AGENT
	header[ACCEPT] = DEFAULT_ACCEPT
	//header[ACCEPT_ENCODING]	= DEFAULT_ACCEPT_ENCODING
	header[ACCEPT_LANGUAGE] = DEFAULT_ACCEPT_LANGUAGE
	header[ACCEPT_CHARSET] = DEFAULT_ACCEPT_CHARSET

	return HttpLoaderRequest{
		Url:         url,
		Method:      GET,
		Proto:       "HTTP/1.1",
		MaxRedirect: 10,
		Header:      header,
	}
}

type HttpLoaderRequest struct {
	Url         web.DigestUrl
	Proto       string
	Method      string
	MaxRedirect int
	Header      map[string]string
}

func (r *HttpLoaderRequest) RequestFromUrl(url web.DigestUrl) HttpLoaderRequest {
	return HttpLoaderRequest{
		Url:         url,
		Method:      r.Method,
		MaxRedirect: r.MaxRedirect,
		Header:      r.Header,
	}
}

func (r *HttpLoaderRequest) DumpToString() string {
	var b strings.Builder

	b.WriteString(r.Method)
	b.WriteString(" ")
	b.WriteString(r.Url.Path())
	b.WriteString(" ")
	b.WriteString(r.Proto)
	b.WriteString("\n")

	for k, v := range r.Header {
		b.WriteString(k)
		b.WriteString(": ")
		b.WriteString(v)
		b.WriteString("\n")
	}

	b.WriteString("\n")

	return b.String()
}
