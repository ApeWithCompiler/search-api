package canonical

import (
	"net/url"

	log "github.com/sirupsen/logrus"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/scraper"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

// GetByQueryParam retrieves the canonical url from the "canonical_url" query parameter
// If the parameter is not set, a empty string and false are returned
// The value is query unescaped
func GetByQueryParam(u web.DigestUrl) (string, bool) {
	val, found := u.GetQueryParam("canonical_url")
	if found {
		decodedVal, err := url.QueryUnescape(val)
		if err != nil {
			log.WithFields(log.Fields{"module": "canonical"}).Warn("Error url unescape cannonical url: ", err)
			return "", false
		}

		return decodedVal, true
	}

	return "", false
}

// GetByHttpRedirect retrives the canonical url by returning the response url upon http redirect
// If not redirected, a empty string and false are returned
func GetByHttpRedirect(resp http.HttpLoaderResponse) (string, bool) {
	if resp.WasRedirected() {
		u := resp.ResponseUrl()
		return u.String(), true
	}

	return "", false
}

// GetByHttpHeader retrives the canonical url if the rel=canonical header is present in the response
// If not present, a empty string and false are returned
func GetByHttpHeader(resp http.HttpLoaderResponse) (string, bool) {
	if resp.GetHeader("rel") == "canonical" {
		u := resp.ResponseUrl()
		return u.String(), true
	}

	return "", false
}

// GetByHtmlHead retrives the canonical url if the rel=canonical tag is present in the html head
// If not present, a empty string and false are returned
func GetByHtmlHead(doc scraper.ScraperWebDocument) (string, bool) {
	head, err := doc.Head()
	if err != nil {
		log.WithFields(log.Fields{"module": "canonical"}).Warn("Error parsing the html head: ", err)
		return "", false
	}

	if head.CanonicalUrl != "" {
		return head.CanonicalUrl, true
	}

	return "", false
}
