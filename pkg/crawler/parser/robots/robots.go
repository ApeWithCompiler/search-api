package robots

import (
	"context"

	log "github.com/sirupsen/logrus"

	"github.com/temoto/robotstxt"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

func GetRobotsUrlFor(url web.DigestUrl) (web.DigestUrl, error) {
	return web.NewUrlBuilderFromDigest(url).
		SetPath("/robots.txt").
		DigestUrl()
}

type RobotsDocument struct {
	defaultAction bool
	r *robotstxt.RobotsData
}

func (d *RobotsDocument) Sitemaps() []string {
	if d.r != nil {
		return d.r.Sitemaps
	}

	return []string{}
}

func (d *RobotsDocument) HasSitemaps() bool {
	if d.r == nil {
		return false
	}

	return len(d.r.Sitemaps) > 0
}

func (d *RobotsDocument) IsAllowed(agent, path string) bool {
	if d.r == nil {
		return d.defaultAction
	}

	g := d.r.FindGroup(agent)
	if g == nil {
		return d.defaultAction
	}

	return g.Test(path)
}

type RobotsParser struct {}

func NewRobotsParser() RobotsParser {
	return RobotsParser{}
}

func (p *RobotsParser) Parse(ctx context.Context, body []byte) (RobotsDocument, error) {
	log.WithFields(log.Fields{
		"module": "parser/robots",
	}).Info("Parsing robots")

	robots, err := robotstxt.FromBytes(body)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "parser/robots",
		}).Error("Failed parsing robots: ", err)
		return RobotsDocument{}, err
	}

	return RobotsDocument{
		defaultAction: false,
		r: robots,
	}, nil
}	
