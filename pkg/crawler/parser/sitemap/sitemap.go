package sitemap

import (
	"context"
	"encoding/xml"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

type Url struct {
	XMLName xml.Name `xml:"url"`
	Loc string `xml:"loc"`
	Lastmod string `xml:"lastmod"`
	Changefreq string `xml:"changefreq"`
	Priority string `xml:"priority"`
}

// Implement Link interface
func (u *Url) Location() string {
	return u.Loc
}

type Urlset struct {
	XMLName xml.Name `xml:"urlset"`
	Urls []Url `xml:"url"`

}

type SitemapParser struct {}

func NewSitemapParser() SitemapParser {
	return SitemapParser{}
}

func (p *SitemapParser) Parse(ctx context.Context, body []byte) (Urlset, error) {
	var urlset Urlset

	logger := log.WithFields(log.Fields{
		"module": "parser/sitemap",
	})

	logger.Info("Parsing sitemap")
	err := xml.Unmarshal(body, &urlset)
	if err != nil {
		logger.Error("Parsing sitemap failed: ", err)
		return Urlset{}, err
	}

	return urlset, nil
}

type UrlT struct {
	Loc web.DigestUrl
	Lastmod time.Time
	Changefreq string
	Priority string
}

type Sitemap struct {
	Urls []UrlT
}

type SitemapTypeAdapter struct {
	adaptee SitemapParser
}

func NewSitemapTypeAdapter(adaptee SitemapParser) SitemapTypeAdapter {
	return SitemapTypeAdapter{
		adaptee: adaptee,
	}
}

func (p *SitemapTypeAdapter) parseUrl(url Url) (UrlT, error) {
	u, err := web.UrlFromString(url.Loc)
	if err != nil {
		return UrlT{}, err
	}

	t, err := time.Parse(time.DateOnly, url.Lastmod)
	if err != nil {
		return UrlT{}, err
	}

	return UrlT{
		Loc: u,
		Lastmod: t,
		Changefreq: url.Changefreq,
		Priority: url.Priority,
	}, err
}

func (p *SitemapTypeAdapter) Parse(ctx context.Context, body []byte) (Sitemap, error) {
	logger := log.WithFields(log.Fields{
		"module": "parser/sitemaptypeadapter",
	})

	urlSet, err := p.adaptee.Parse(ctx, body)
	if err != nil {
		logger.Error("Adaptee returned error: ", err)
		return Sitemap{}, err
	}

	var sitemap Sitemap
	var parseErr error

	for i := range urlSet.Urls {
		ut, err := p.parseUrl(urlSet.Urls[i]) 
		if err != nil {
			logger.Warn("Failed parsing url to types: ", err)
			parseErr = err
		} else {
			sitemap.Urls = append(sitemap.Urls, ut)
		}
	}

	return sitemap, parseErr
}
