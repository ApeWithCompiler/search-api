package sitemap

import (
	"testing"

	"context"

	"gitlab.com/apewithcompiler/heru/mock"
)

func TestSitemapParser_ParseSitemap(t *testing.T) {
	src := mock.EXAMPLE_SITEMAP

	sut := NewSitemapParser()

	got, err := sut.Parse(context.Background(), []byte(src))

	if err != nil {
		t.Errorf("Parse() returned error %v", err)
	}

	if len(got.Urls) == 0 {
		t.Errorf("Parse() returned 0 urls")
		t.SkipNow()
	}

	if got.Urls[0].Loc != "https://example.com/tagged/coordination" {
		t.Errorf("Parse() returned %v, want %v", got.Urls[0].Loc, "https://example.com/tagged/coordination")
	}

	if got.Urls[0].Lastmod != "2011-11-29" {
		t.Errorf("Parse() returned %v, want %v", got.Urls[0].Lastmod, "2011-11-29")
	}

	if got.Urls[0].Changefreq != "monthly" {
		t.Errorf("Parse() returned %v, want %v", got.Urls[0].Changefreq, "monthly")
	}

	if got.Urls[0].Priority != "1.0" {
		t.Errorf("Parse() returned %v, want %v", got.Urls[0].Priority, "1.0")
	}
}

func TestSitemapAdapter_ParseSitemap(t *testing.T) {
	src := mock.EXAMPLE_SITEMAP

	sut := NewSitemapTypeAdapter(NewSitemapParser())

	got, err := sut.Parse(context.Background(), []byte(src))
	if err != nil {
		t.Errorf("Parse() returned error %v", err)
	}

	if len(got.Urls) == 0 {
		t.Errorf("Parse() returned 0 urls")
		t.SkipNow()
	}

	if got.Urls[0].Loc.String() != "https://example.com/tagged/coordination" {
		t.Errorf("Parse() returned %v, want %v", got.Urls[0].Loc.String(), "https://example.com/tagged/coordination")
	}

	y, m, d := got.Urls[0].Lastmod.Date()
	if  y != 2011 || m != 11 || d != 29 {
		t.Errorf("Parse() returned %v-%v-%v, want %v", y, m, d, "2011-11-29")
	}
}
