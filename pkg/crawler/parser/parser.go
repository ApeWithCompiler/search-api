package parser

import (
	"io"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

type DocumentHeadParser interface {
	Parse(reader io.Reader) document.DocumentHead
}

type ChunkType int

const (
	MAIN_HEADING ChunkType = 0
	SUB_HEADING            = 1
	TEXT                   = 2
)

type Chunk struct {
	Type ChunkType
	Text string
}

func NewChunk(ctype ChunkType, text string) Chunk{
	return Chunk{
		Type: ctype,
		Text: text,
	}
}

type TextParser interface {
	Parse(reader io.Reader) ([]Chunk, error)
}
