package html

var DEFAULT_HEADINGS = []string{"h1", "h2", "h3", "h4", "h5", "h6"}
var DEFAULT_TEXTS = []string{"p", "span", "div"}
