package html

import (
	"bytes"
	"context"
	"io"

	log "github.com/sirupsen/logrus"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/parser/html/extractor"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/text/sanitizer"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func NewHtmlHeadParser() HtmlHeadParser {
	return HtmlHeadParser{}
}

// HtmlHeadParser implements a parser for extracting data of the html <head></head> section.
type HtmlHeadParser struct {
}

func (p *HtmlHeadParser) Parse(ctx context.Context, source web.DigestUrl, body []byte) (document.DocumentHead, error) {
	logger := log.WithFields(log.Fields{
		"module": "parser/html",
		"url":    source.String(),
	})

	logger.Info("Parsing documment")

	head := document.DocumentHead{}

	buff := bytes.NewBuffer(body)
	query, err := goquery.NewDocumentFromReader(buff)
	if err != nil {
		logger.Error("Parsing documment failed: ", err)

		return head, err
	}

	head.MimeType = "text/html"

	head.Charset = extractor.NewCharsetExtractor(query).Extract()
	head.Languages = extractor.NewLanguageExtractor(query).Extract()
	head.CanonicalUrl = extractor.NewCanonicalExtractor(query).Extract()
	head.Titles = extractor.NewTitleExtractor(query).Extract()
	head.Descriptions = extractor.NewDescriptionExtractor(query).Extract()
	head.Creator = extractor.NewAuthorExtractor(query).Extract()
	head.Keywords = extractor.NewKeywordExtractor(query).Extract()

	icons := extractor.NewIconExtractor(query).Extract()
	icon := extractor.MaxIcon(icons)
	head.Favicon = icon.Href

	return head, nil
}

func NewHtmlAnchorParser() HtmlAnchorParser {
	return HtmlAnchorParser{
		NewLineSanitizer: &sanitizer.NewLineSanitizer{},
		SpaceSanitizer:   &sanitizer.SpaceSanitizer{},
	}
}

type HtmlAnchorParser struct {
	NewLineSanitizer sanitizer.Sanitizer
	SpaceSanitizer   sanitizer.Sanitizer
}

func (p *HtmlAnchorParser) Parse(ctx context.Context, body []byte) ([]web.Anchor, error) {
	var anchors []web.Anchor

	log.WithFields(log.Fields{
		"module": "parser/html",
	}).Info("Parsing documment")

	buff := bytes.NewBuffer(body)
	query, err := goquery.NewDocumentFromReader(buff)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "parser/html",
		}).Error("Parsing documment failed: ", err)

		return anchors, err
	}

	log.WithFields(log.Fields{
		"module": "parser/html",
	}).Debug("Parsing anchors")

	return extractor.NewAnchorExtractor(query).Extract(), nil
}

type HtmlTextTag struct {
	Tag string
	Type parser.ChunkType
}

type HtmlTextParser struct {
	tags []HtmlTextTag
}

func NewHtmlTextParser() HtmlTextParser {
	return HtmlTextParser{
		tags: []HtmlTextTag{
			{
				Tag: "h1",
				Type: parser.MAIN_HEADING,
			},
			{
				Tag: "h2",
				Type: parser.SUB_HEADING,
			},
			{
				Tag: "h3",
				Type: parser.SUB_HEADING,
			},
			{
				Tag: "h4",
				Type: parser.SUB_HEADING,
			},
			{
				Tag: "p",
				Type: parser.TEXT,
			},
			{
				Tag: "span",
				Type: parser.TEXT,
			},
		},
	}
}

func (p *HtmlTextParser) Parse(reader io.Reader) ([]parser.Chunk, error) {
	var chunks []parser.Chunk

	log.WithFields(log.Fields{
		"module": "parser/html",
	}).Info("Parsing text")

	query, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "parser/html",
		}).Error("Parsing documment failed: ", err)

		return chunks, err
	}

	for _, t := range p.tags {
		for _, block := range extractor.NewTextBlockExtractor(query, t.Tag).Extract() {
			chunks = append(chunks, parser.NewChunk(t.Type, block))
		}
	}

	return chunks, nil
}
