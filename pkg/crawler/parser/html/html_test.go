package html

import (
	"testing"

	"context"

	"gitlab.com/apewithcompiler/heru/mock"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

func TestHtmlParser_ParseHead(t *testing.T) {
	src := mock.EXAMPLE_HTML

	sut := NewHtmlHeadParser()
	url, _ := web.UrlFromString("http://example.com")
	got, err := sut.Parse(context.Background(), url, []byte(src))

	if err != nil {
		t.Errorf("Parse() returned error %v", err)
	}

	if got.Creator != mock.EXAMPLE_HTML_AUTHOR {
		t.Errorf("Parse() returned %v, want %v", got.Creator, mock.EXAMPLE_HTML_AUTHOR)
	}
}
