package extractor

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

func parseKeywordString(keywords string) []string {
	return strings.Split(keywords, ",")
}

func trimToken(token string) string {
	return strings.TrimSpace(token)
}

func normalizeKeywordCase(keyword string) string {
	return strings.ToLower(keyword)
}

type KeywordExtractor struct{
	doc *goquery.Document
}

func NewKeywordExtractor(doc *goquery.Document) *KeywordExtractor {
	return &KeywordExtractor{
		doc: doc,
	}
}

func (x *KeywordExtractor) Extract() []string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract keywords")

	var keywords []string

	x.doc.Find("meta[name=keywords]").Each(func(i int, s *goquery.Selection) {
		val, ok := s.Attr("content")
		if ok {
			for _, keyword := range parseKeywordString(val) {
				keywords = append(keywords, normalizeKeywordCase(trimToken(keyword)))
			}
		}
	})

	return keywords
}
