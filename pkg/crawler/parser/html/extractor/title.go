package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type TitleExtractor struct{
	doc *goquery.Document
}

func NewTitleExtractor(doc *goquery.Document) *TitleExtractor {
	return &TitleExtractor{
		doc: doc,
	}
}

func (x *TitleExtractor) Extract() []string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract titles")

	var titles []string

	x.doc.Find("title").Each(func(i int, s *goquery.Selection) {
		titles = append(titles, s.Text())
	})

	return titles
}
