package extractor

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/text/sanitizer"
)

type AnchorExtractor struct{
	doc *goquery.Document

	newLineSanitizer sanitizer.Sanitizer
	spaceSanitizer   sanitizer.Sanitizer
}

func NewAnchorExtractor(doc *goquery.Document) *AnchorExtractor {
	return &AnchorExtractor{
		doc: doc,

		newLineSanitizer: &sanitizer.NewLineSanitizer{},
		spaceSanitizer:   &sanitizer.SpaceSanitizer{},
	}
}

func (x *AnchorExtractor) Extract() []web.Anchor {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract anchors")

	var anchors []web.Anchor

	x.doc.Find("a[href]").Each(func(i int, s *goquery.Selection) {
		if href, hasHref := s.Attr("href"); hasHref {
			if href == "" || href == "#" {
				log.WithFields(log.Fields{
					"module": "parser/html/extractor",
				}).Info("Skipping, href is empty")
				return
			}

			if strings.HasSuffix(href, ";") {
				log.WithFields(log.Fields{
					"module": "parser/html/extractor",
				}).Info("Skipping, suspecting javascript as href: ", href)
				return
			}

			text := x.spaceSanitizer.Transform(x.newLineSanitizer.Transform(s.Text()))
			
			rel, _ := s.Attr("rel")

			log.WithFields(log.Fields{
				"module": "parser/html",
			}).Debug("Create AnchorEdge for: ", href)

			anchors = append(anchors, web.NewAnchor(href, text, rel))
		}
	})

	return anchors
}
