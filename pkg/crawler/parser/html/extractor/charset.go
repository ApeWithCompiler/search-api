package extractor

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

func extractCharsetFromContentType(contentType string) string {
	segments := strings.Split(contentType, "; ")
	if len(segments) == 0 {
		return ""
	}

	for _, s := range segments {
		kv := strings.Split(s, "=")
		if len(kv) == 2 {
			key := kv[0]
			value := kv[1]

			if key == "charset" {
				return value
			}
		}
	}

	return ""
}

type CharsetExtractor struct{
	doc *goquery.Document
}

func NewCharsetExtractor(doc *goquery.Document) *CharsetExtractor {
	return &CharsetExtractor{
		doc: doc,
	}
}

func (x *CharsetExtractor) Extract() string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract charset")

	charset := ""

	x.doc.Find("meta[http-equiv=Content-Type]").Each(func(i int, s *goquery.Selection) {
		attr, ok := s.Attr("content")
		if ok {
			charset = extractCharsetFromContentType(attr)
		}
	})

	x.doc.Find("meta[charset]").Each(func(i int, s *goquery.Selection) {
		attr, ok := s.Attr("charset")
		if ok {
			attr = attr
		}
	})

	return charset
}
