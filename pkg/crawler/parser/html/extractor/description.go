package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type DescriptionExtractor struct{
	doc *goquery.Document
}

func NewDescriptionExtractor(doc *goquery.Document) *DescriptionExtractor {
	return &DescriptionExtractor{
		doc: doc,
	}
}

func (x *DescriptionExtractor) Extract() []string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract descriptions")

	var descriptions []string

	x.doc.Find("meta[name=description]").Each(func(i int, s *goquery.Selection) {
		val, ok := s.Attr("content")
		if ok {
			descriptions = append(descriptions, val)

		}
	})

	return descriptions
}
