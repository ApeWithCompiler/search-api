package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type AuthorExtractor struct{
	doc *goquery.Document
}

func NewAuthorExtractor(doc *goquery.Document) *AuthorExtractor {
	return &AuthorExtractor{
		doc: doc,
	}
}

func (x *AuthorExtractor) Extract() string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract author")

	author := ""

	x.doc.Find("meta[name=author]").Each(func(i int, s *goquery.Selection) {
		val, ok := s.Attr("content")
		if ok {
			author = val
		}
	})

	return author
}
