package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type LanguageExtractor struct{
	doc *goquery.Document
}

func NewLanguageExtractor(doc *goquery.Document) *LanguageExtractor {
	return &LanguageExtractor{
		doc: doc,
	}
}

func (x *LanguageExtractor) Extract() []string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract languages")

	var languages []string

	x.doc.Find("html[lang]").Each(func(i int, s *goquery.Selection) {
		val, ok := s.Attr("lang")
		if ok {
			languages = append(languages, val)
		}
	})

	return languages
}
