package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type CanonicalExtractor struct{
	doc *goquery.Document
}

func NewCanonicalExtractor(doc *goquery.Document) *CanonicalExtractor {
	return &CanonicalExtractor{
		doc: doc,
	}
}

func (x *CanonicalExtractor) Extract() string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract canonical url")

	canonical := ""

	x.doc.Find("link[rel=canonical]").Each(func(i int, s *goquery.Selection) {
		val, ok := s.Attr("href")
		if ok {
			canonical = val
		}
	})

	return canonical
}
