package extractor

import (
	"strconv"
	"strings"

	"sort"

	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type Icon struct {
	Href     string
	SizeAttr string
	Size     int
}

func sizeAttrToInt(size string) int {
	s := strings.Split(size, "x")
	if len(s) != 2 {
		log.WithFields(log.Fields{
			"module": "parser/html/extractor",
		}).Error("Error parsing icon size: unexpected format")
		return 0
	}

	res, err := strconv.Atoi(s[0])
	if err != nil {
		log.WithFields(log.Fields{
			"module": "parser/html/extractor",
		}).Error("Error parsing icon size to int")
		return 0
	}

	return res
}

func MaxIcon(icons []Icon) Icon {
	// When input is empty
	if len(icons) == 0 {
		return Icon{}
	}

	// When only one is available, choice is simple
	if len(icons) == 1 {
		return icons[0]
	}

	sort.Slice(icons, func(i, j int) bool { return icons[i].Size > icons[j].Size })

	return icons[0]
}

type IconExtractor struct{
	doc *goquery.Document
}

func NewIconExtractor(doc *goquery.Document) *IconExtractor {
	return &IconExtractor{
		doc: doc,
	}
}

func (x *IconExtractor) Extract() []Icon {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract icons")

	icons := []Icon{}

	x.doc.Find("link[rel=icon]").Each(func(i int, s *goquery.Selection) {
		icon := Icon{}

		href, ok := s.Attr("href")
		if ok {
			icon.Href = href
		}

		size, ok := s.Attr("sizes")
		if ok {
			icon.SizeAttr = size
			icon.Size = sizeAttrToInt(size)
		}

		icons = append(icons, icon)
	})

	return icons
}
