package extractor

import (
	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
)

type TextBlockExtractor struct{
	doc *goquery.Document
	tag string
}

func NewTextBlockExtractor(doc *goquery.Document, tag string) *TextBlockExtractor {
	return &TextBlockExtractor{
		doc: doc,
		tag: tag,
	}
}

func (x *TextBlockExtractor) Extract() []string {
	log.WithFields(log.Fields{"module": "parser/html/extractor"}).Debug("Extract text blocks")

	var blocks []string

	x.doc.Find(x.tag).Each(func(i int, s *goquery.Selection) {
		if s.Text() != "" {
			blocks = append(blocks, s.Text())
		}
	})

	return blocks
}
