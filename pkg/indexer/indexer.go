package indexer

import (
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/model/document"
)

func CreateResult(metadata document.DocumentMetadata, data document.DocumentData) document.DocumentResult {
	log.WithFields(log.Fields{"module": "indexer"}).Debug("Creating result: ", metadata.Id)

	var result document.DocumentResult

	result.Id = metadata.Id.String()
	result.Link = metadata.Url.String()

	head := data.GetHead()

	if len(head.Titles) > 0 {
		result.Title = head.Titles[0]
	}

	result.Snippet = strings.Join(head.Descriptions, "\n")
	result.Mime = head.MimeType
	result.Labels = head.Keywords

	result.Updated = time.Now()

	return result
}
