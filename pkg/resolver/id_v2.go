package resolver

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/apewithcompiler/heru/pkg/model/web"
	"gitlab.com/apewithcompiler/heru/pkg/util"
)

const (
	KEY_DELIMITER	string = "-"
	SEG_HOST_SIZE	int    = 24
	SEG_PATH_SIZE          = 32
	SEG_QUERY_SIZE         = 12
)

func cut(s string, size int) string {
	if len(s) <= size {
		return s
	}

	return s[0:size]
}

func Segment(s string, size int) string {
	if s == "" {
		return strings.Repeat("0", size)
	}

	h := util.Sha1Hash(s)
	return cut(h, size)
}

type DocumentIdv2 struct {
	host string
	path string
	query string
}

func (id *DocumentIdv2) Host() string {
	return id.host
}

func (id *DocumentIdv2) Path() string {
	return id.path
}

func (id *DocumentIdv2) Query() string {
	return id.query
}

// Implement model.DocumentId interface
func (id *DocumentIdv2) String() string {
	return strings.Join([]string{id.Host(),
		id.Path(),
		id.Query(),},
		KEY_DELIMITER)	
}

// Implement model.DocumentId interface
func (id *DocumentIdv2) Version() int8 {
	return 2
}

// MarshalJSON implementation for JSON Marshaler interface
func (id *DocumentIdv2) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}

// UnmarshalJSON implementation for JSON Unmarshaler interface
func (id *DocumentIdv2) UnmarshalJSON(data []byte) error {
	s := string(data)

	s = strings.TrimPrefix(s, "\"")
	s = strings.TrimSuffix(s, "\"")

	p, err := NewDocumentIdV2FromString(s)
	if err != nil {
		return err
	}
	
	id.host = p.Host()
	id.path = p.Path()
	id.query = p.Query()

	return nil
}

func NewDocumentIdV2FromUrl(url web.DigestUrl) DocumentIdv2 {
	return DocumentIdv2{
		host: Segment(url.Hostname(), SEG_HOST_SIZE),
		path: Segment(url.Path(), SEG_PATH_SIZE),
		query: Segment(url.RawQuery(), SEG_QUERY_SIZE),
	}
}

func NewDocumentIdV2FromString(input string) (DocumentIdv2, error) {
	s := strings.Split(input, KEY_DELIMITER)
	if len(s) != 3 {
		return DocumentIdv2{}, fmt.Errorf("Invalid segment count")
	}

	host := s[0]
	if len(host) != SEG_HOST_SIZE {
		return DocumentIdv2{}, fmt.Errorf("Invalid segment size: host")
	}

	path := s[1]
	if len(path) != SEG_PATH_SIZE {
		return DocumentIdv2{}, fmt.Errorf("Invalid segment size: path")
	}

	query := s[2]
	if len(query) != SEG_QUERY_SIZE {
		return DocumentIdv2{}, fmt.Errorf("Invalid segment size: query")
	}

	return DocumentIdv2{
		host: host,
		path: path,
		query: query,
	}, nil
}
