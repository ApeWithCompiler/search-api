package version

import (
	"fmt"
)

// Version information.
// This is meant to be set while compilation.
var GIT_TAG string
var GIT_COMMIT string
var GIT_BRANCH string

func GetVersionString() string {
	return fmt.Sprintf("heru %v\nBuild commit %v (%v)\n", GIT_TAG, GIT_COMMIT, GIT_BRANCH)
}
