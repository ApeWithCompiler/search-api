package index

import (
	"encoding/json"
	"net/http"
	"strconv"
	"math"

	log "github.com/sirupsen/logrus"

	"github.com/google/jsonapi"

	actor "gitlab.com/apewithcompiler/heru/pkg/actor/search"
	"gitlab.com/apewithcompiler/heru/pkg/http/response"
	"gitlab.com/apewithcompiler/heru/pkg/model/search"
	"gitlab.com/apewithcompiler/heru/pkg/util"
	"gitlab.com/apewithcompiler/heru/pkg/util/imath"
)

func parseCountParam(paramCount string, defaultVal int) int {
	if paramCount == "" {
		return defaultVal
	}
	
	i, err := strconv.Atoi(paramCount)
	if err != nil {
		log.Warning("Invalid count parameter supplided: ", paramCount)
		return defaultVal
	}
	return i
}

func parsePageParam(paramPage string, defaultVal int) int {
	if paramPage == "" {
		return defaultVal
	}
	
	i, err := strconv.Atoi(paramPage)
	if err != nil {
		log.Warning("Invalid page parameter supplided: ", paramPage)
		return defaultVal
	}
	return i
}

func getOffset(page, count int) int {
	if page == 1 {
		return 0
	}

	return page * count
}

func getPageCount(maxResults, countPerPage int) int {
	res := int(maxResults / countPerPage)
	rem := math.Remainder(float64(maxResults), float64(countPerPage))
	if rem > 0 {
		return res + 1
	}

	return res
}

type SearchHandler struct {
	Actor *actor.SearchActor
}

// @Summary	Search index
// @Description execute a search quey on a index
// @Tags	index
// @Accept	json
// @Produce	json
// @Param	index	path	string	true	"index to search in"
// @Param	q	query	string	true	"submitted query"
// @Param	count	query	string	false	"limit result count per page"
// @Param	page	query	string	false	"pagination"
// @Success	200	{object} response.SearchResponse
// @Router	/indexes/{index}/search [get]
func (h *SearchHandler) Handle(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	paramQuery := r.FormValue("q")
	paramCount := r.FormValue("count")
	paramPage := r.FormValue("page")
	
	count := parseCountParam(paramCount, 10)
	page := parsePageParam(paramPage, 1)
	offset := getOffset(page, count)

	searchMsg := actor.NewSearchMessage(ctx, paramQuery, count, offset)
	h.Actor.SendToMailbox(searchMsg)
	result, err := searchMsg.AwaitResult()
	if err != nil {
		log.Error("Error processing the request: ", err)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		err = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{{
			Title:  "Internal Server Error",
			Detail: "The search request resulted in a error. Please see the log for further information.",
			Status: "500",
		}})
		if err != nil {
			log.Error("Error marshalling response: ", err)
		}

		return
	}

	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	maxPages := getPageCount(result.GetMaxCount(), count)

	prevPage := imath.Max(1, page - 1)
	nextPage := imath.Min(maxPages, page + 1)

	cursor := util.CursorToWindowStart(page - 1, 7)
	wStart, wEnd := util.SliceWindow(cursor, 7, maxPages -1)
	window := imath.Interval(wStart + 1, wEnd + 1) 

	pagination := search.Pagination{
		Next: nextPage,
		Current: page,
		Previous: prevPage,
		Window: window,
		Max: maxPages,
	}

	resp := response.SearchResponseFromResult(result, pagination)	
	err = json.NewEncoder(w).Encode(resp)	
	if err != nil {
		log.Error("Error parsing the response: ", err)

		w.WriteHeader(http.StatusInternalServerError)
		err = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{{
			Title:  "Internal Server Error",
			Detail: "The search reponse could not be parsed. Please see the log for further information.",
			Status: "500",
		}})
		if err != nil {
			log.Error("Error marshalling response: ", err)
		}

		return
	}
}
