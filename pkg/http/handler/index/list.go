package index

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/google/jsonapi"

	"gitlab.com/apewithcompiler/heru/pkg/model/index"
)

type IndexResourceHandler struct {
	Indexes []index.Index
}

// @Summary	List indexes
// @Description list indexes
// @Tags	index
// @Accept	json
// @Produce	json
// @Param	q	query	string	false	"name search by q"
// @Success	200	{array}	index.Index
// @Router	/indexes [get]
func (h *IndexResourceHandler) HandleListIndexes(w http.ResponseWriter, r *http.Request) {
	paramName := r.FormValue("name")

	var indexes []index.Index

	if paramName != "" {
		i := h.searchIndex(paramName)
		if i != nil {
			indexes = append(indexes, *i)
		}
	} else {
		for _, c := range h.Indexes {
			i := index.Index{
				Name:     c.Name,
				Provider: c.Provider,
			}
			indexes = append(indexes, i)
		}
	}

	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var payload []*index.Index
	for i := range indexes {
		payload = append(payload, &indexes[i])
	}
	err := jsonapi.MarshalPayload(w, payload)
	if err != nil {
		log.Error("Error marshalling response: ", err)
	}
}

func (h *IndexResourceHandler) searchIndex(name string) *index.Index {
	for _, i := range h.Indexes {
		if i.Name == name {
			return &i
		}
	}

	return nil
}
