package repository

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"

	"gitlab.com/apewithcompiler/heru/pkg/actor/repository"
	"gitlab.com/apewithcompiler/heru/pkg/resolver"
)

// @Summary	Delete document
// @Description delete a document from repository
// @Tags	repository
// @Accept	json
// @Produce	json
// @Param	repository	path	string	true	"respository context"
// @Param	documentId	path	string	true	"id of the document"
// @Success	204
// @Router	/repositories/{repository}/docs/{documentId} [delete]
func (h *DocumentHandler) HandleDelete(w http.ResponseWriter, r *http.Request) {
	parameters := mux.Vars(r)
	paramDocId := parameters["documentId"]

	id, err := resolver.NewDocumentIdV2FromString(paramDocId)
	if err != nil {
		log.Error("Error processing the request: ", err)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		err = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{{
			Title:  "Bad request",
			Detail: "The provided id was no valid DocumentIdV2.",
			Status: "400",
		}})
		if err != nil {
			log.Error("Error marshalling response: ", err)
		}

		return
	}

	msgDelete := repository.NewDeleteDocumentMessage(&id)
	h.Actor.SendToMailbox(&msgDelete)

	w.WriteHeader(http.StatusNoContent)
}
