package repository

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"

	"gitlab.com/apewithcompiler/heru/pkg/actor/repository"
	"gitlab.com/apewithcompiler/heru/pkg/resolver"
)

// @Summary	Get document metadata
// @Description get a document metadata
// @Tags	repository
// @Accept	json
// @Produce	json
// @Param	repository	path	string	true	"respository context"
// @Param	documentId	path	string	true	"id of the document"
// @Success	200	{object} document.DocumentMetadata
// @Router	/repositories/{repository}/docs/{documentId}/metadata [get]
func (h *DocumentHandler) HandleGetMetadata(w http.ResponseWriter, r *http.Request) {
	parameters := mux.Vars(r)
	paramDocId := parameters["documentId"]

	id, err := resolver.NewDocumentIdV2FromString(paramDocId)
	if err != nil {
		log.Error("Error processing the request: ", err)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		err = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{{
			Title:  "Bad request",
			Detail: "The provided id was no valid DocumentIdV2.",
			Status: "400",
		}})
		if err != nil {
			log.Error("Error marshalling response: ", err)
		}

		return
	}

	msg := repository.NewGetMetadataMessage(&id)
	h.Actor.SendToMailbox(&msg)

	metadata, err := msg.AwaitResult()
	if err != nil {
		log.Error("Error processing the request: ", err)

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		err = jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{{
			Title:  "Internal Server Error",
			Detail: "The repository request resulted in a error. Please see the log for further information.",
			Status: "500",
		}})
		if err != nil {
			log.Error("Error marshalling response: ", err)
		}

		return
	}

	metadata.RawId = metadata.Id.String()
	metadata.RawUrl = metadata.Url.String()

	w.Header().Add("Content-Type", "application/json")
	if err := jsonapi.MarshalPayload(w, &metadata); err != nil {
		log.Error("Error marshalling response: ", err)
	}
}
