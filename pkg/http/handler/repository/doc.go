package repository

import (
	"gitlab.com/apewithcompiler/heru/pkg/actor/repository"
)

type DocumentHandler struct {
	Actor *repository.RepositoryActor
}
