package requestid

import (
	"gitlab.com/apewithcompiler/heru/pkg/requestid"
	"net/http"
)

const ContextKeyRequestID string = "requestId"

func RequestIdMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		ctx = requestid.NewToContext(ctx)

		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
