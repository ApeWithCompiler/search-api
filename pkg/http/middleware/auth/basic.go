package auth

import (
	"crypto/sha256"
	"crypto/subtle"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/requestid"
)

func BasicAuth(authRequired bool, next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !authRequired {
			next.ServeHTTP(w, r)
			return
		}

		reqID := requestid.FromContext(r.Context())

		username, password, ok := r.BasicAuth()
		log.WithFields(log.Fields{"module": "auth/basic", "request-id": reqID}).Debug("Checking auth: ", username)
		if ok {
			usernameHash := sha256.Sum256([]byte(username))
			passwordHash := sha256.Sum256([]byte(password))

			conf := config.GetCurrentConfig()

			expectedUsernameHash := sha256.Sum256([]byte(conf.ApiConfig.User))
			expectedPasswordHash := sha256.Sum256([]byte(conf.ApiConfig.Password))

			usernameMatch := (subtle.ConstantTimeCompare(usernameHash[:], expectedUsernameHash[:]) == 1)
			passwordMatch := (subtle.ConstantTimeCompare(passwordHash[:], expectedPasswordHash[:]) == 1)

			if usernameMatch && passwordMatch {
				next.ServeHTTP(w, r)
				return
			}
		}

		if username == "" {
			log.WithFields(log.Fields{"module": "auth/basic", "request-id": reqID}).Info("Unauthorized: No credentials supplied")
		} else {
			log.WithFields(log.Fields{"module": "auth/basic", "request-id": reqID}).Info("Unauthorized: ", username)
		}
		w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
	})
}
