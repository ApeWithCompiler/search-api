package route

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/apewithcompiler/heru/pkg/actor/search"
	"gitlab.com/apewithcompiler/heru/pkg/http/handler/index"
	"gitlab.com/apewithcompiler/heru/pkg/http/middleware/auth"
	"gitlab.com/apewithcompiler/heru/pkg/http/middleware/requestid"
	indexModel "gitlab.com/apewithcompiler/heru/pkg/model/index"
)

func wrapHandler(protected bool, h http.HandlerFunc) http.Handler {
	return handlers.LoggingHandler(os.Stdout, requestid.RequestIdMiddleware(auth.BasicAuth(protected, h)))
}

func StripPath(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		prefix := r.URL.Path

		p := strings.TrimPrefix(r.URL.Path, prefix)
		rp := strings.TrimPrefix(r.URL.RawPath, prefix)
		if len(p) < len(r.URL.Path) && (r.URL.RawPath == "" || len(rp) < len(r.URL.RawPath)) {
			r2 := new(http.Request)
			*r2 = *r
			r2.URL = new(url.URL)
			*r2.URL = *r.URL
			r2.URL.Path = p
			r2.URL.RawPath = rp
			h.ServeHTTP(w, r2)
		} else {
			http.NotFound(w, r)
		}
	})
}

func NewHttpRouterControler(listenaddr string) HttpRouterControler {
	logger := log.WithFields(log.Fields{"module": "HttpRouterControler"})
	router := mux.NewRouter()

	return HttpRouterControler{
		Router:     router,
		ListenAddr: listenaddr,
		logger:     *logger,
	}
}

// HttpRouterControler encapsulates logic needed for managing
// the router.
// It provides a interface that is oriented torwards
// the domain context of the app, hiding details.
//
// @title	Heru API
// @version	1.2
// @description A web search engine
//
// @host	localhost:9098
// @BasePath	/api
type HttpRouterControler struct {
	logger log.Entry
	Router *mux.Router

	ListenAddr string

	ProtectReads   bool
	ProtectQueries bool
	ProtectWrites  bool
	IndexResources []indexModel.Index

	RedirectHomeIndex string
}

func (r *HttpRouterControler) RegisterMetrics() {
	r.Router.Path("/metrics").Handler(promhttp.Handler())
	r.logPathRegistered("GET", "/metrics")
}

func (r *HttpRouterControler) RegisterFrontend(frontend embed.FS) {
	stripped, err := fs.Sub(frontend, "ui/dist")

	if err != nil {
		r.logger.Error("Error stripping embedded frontend fs: ", err)
	}

	frontendFS := http.FileServer(http.FS(stripped))

	// VueJS dist files will only have a index.html.
	// The fileserver would fail to specifically serve search.html,
	// So the prefix is stripped and VueJS routes within its index.html
	r.Router.PathPrefix("/about").Handler(handlers.LoggingHandler(os.Stdout, http.StripPrefix("/about", frontendFS)))
	r.Router.PathPrefix("/home").Handler(handlers.LoggingHandler(os.Stdout, StripPath(frontendFS)))
	r.Router.PathPrefix("/search").Handler(handlers.LoggingHandler(os.Stdout, StripPath(frontendFS)))
	
	if r.RedirectHomeIndex != "" {
		r.logger.Info("Setup redirect to index: ", r.RedirectHomeIndex)
		r.Router.Path("/").Handler(http.RedirectHandler("/home/" + r.RedirectHomeIndex, http.StatusSeeOther))
	}

	r.Router.PathPrefix("/assets").Handler(handlers.LoggingHandler(os.Stdout, frontendFS))
}

func (r *HttpRouterControler) HandleRequest() {
	r.logger.Info("Listening on addr: ", r.ListenAddr)
	r.logger.Fatal(http.ListenAndServe(r.ListenAddr, r.Router))
}

func (r *HttpRouterControler) RegisterIndexCollection() {
	indexResourceHandler := index.IndexResourceHandler{Indexes: r.IndexResources}
	r.Router.Handle("/api/indexes", handlers.LoggingHandler(os.Stdout, requestid.RequestIdMiddleware(auth.BasicAuth(r.ProtectReads, indexResourceHandler.HandleListIndexes)))).Methods("GET")
	r.logPathRegistered("GET", "/api/indexes")
}

func (r *HttpRouterControler) RegisterIndexResource(name string, srv *search.SearchActor) {
	r.logger.Info("Register index resource")

	searchHandler := index.SearchHandler{Actor: srv}

	r.Router.Handle("/api/indexes/"+name+"/search", wrapHandler(r.ProtectQueries, searchHandler.Handle)).Methods("GET")
	r.logPathRegistered("GET", "/api/indexes/"+name+"/search")

	apiModel := indexModel.Index{
		Name: name,
		Provider: "",
	}

	r.IndexResources = append(r.IndexResources, apiModel)
}

func (r *HttpRouterControler) logPathRegistered(method, path string) {
	m := strings.ToUpper(method)

	r.logger.Info(fmt.Sprintf("Path registered: %v %v", m, path))
}
