package response

import (
	"time"

	"gitlab.com/apewithcompiler/heru/pkg/model/search"
)

type ResultDocumentAttributes struct {
	Source string `json:"source"`
	// Score is the the score of the result
	Score float64 `json:"score"`
	// Title is the title of the result
	Title string `json:"title"`
	// HtmlTitle is the title, but HTML formatted
	HtmlTitle string `json:"htmlTitle"`
	// Link is the complete link to the result
	Link string `json:"link"`
	// DisplayLink is the shortened link to the result
	DisplayLink string `json:"displayLink"`
	// Snippet is the short text descriping the result
	Snippet string `json:"snippet"`
	// HtmlSnippet is the short text descriping the result, bur HTML formatted
	HtmlSnippet string `json:"htmlSnippet"`
	// FormattedUrl is the url displayed after every result
	FormattedUrl string `json:"formattedUrl"`
	// HtmlFormattedUrl is the url displayed after every result, bur HTML formatted
	HtmlFormattedUrl string `json:"htmlFormattedUrl"`
	// ThumbnailUrl provides a URL to a thumbnail decorating the result
	ThumbnailUrl string `json:"thumbnailUrl"`
	// FaviconUrl provides a URL to a favicon for the site
	FaviconUrl string `json:"faviconlUrl"`
	// Mime is the MIME type of the result
	Mime string `json:"mime"`
	// FileFormat is the file format of the result
	FileFormat string `json:"fileFormat"`
	// Labels for additional information
	Labels []string `json:"labels"`
	// Updated indicated when the result was last modified
	Updated time.Time `json:"updated"`
}

type ResultDocument struct {
	Type string `json:"type"`
	Id string `json:"id"`
	Attributes ResultDocumentAttributes `json:"attributes"`
}

type ResultMeta struct {
	MaxCount int `json:"maxCount"`
	Pagination search.Pagination `json:"pagination"`
}

type SearchResponse struct {
	Data []ResultDocument `json:"data"`
	Meta ResultMeta `json:"meta"`
}

func parseResultDocument(doc search.ResultDocument) ResultDocument {
	return ResultDocument{
		Type: "resultdocument",
		Id: doc.Id,
		Attributes: ResultDocumentAttributes{
			Source: doc.Source,
			Score: doc.Score,
			Title: doc.Title,
			HtmlTitle: doc.HtmlTitle,
			Link: doc.Link,
			DisplayLink: doc.DisplayLink,
			Snippet: doc.Snippet,
			HtmlSnippet: doc.HtmlSnippet,
			FormattedUrl: doc.FormattedUrl,
			HtmlFormattedUrl: doc.HtmlFormattedUrl,
			ThumbnailUrl: doc.ThumbnailUrl,
			FaviconUrl: doc.FaviconUrl,
			Mime: doc.Mime,
			FileFormat: doc.FileFormat,
			Labels: doc.Labels,
			Updated: doc.Updated,
		},
	}
}

func SearchResponseFromResult(result search.Result, pagination search.Pagination) SearchResponse {
	var docs []ResultDocument

	for i := range result.Documents {
		docs = append(docs, parseResultDocument(result.Documents[i]))
	}

	return SearchResponse{
		Data: docs,
		Meta: ResultMeta{
			MaxCount: result.GetMaxCount(),
			Pagination: pagination,
		},
	}
}
