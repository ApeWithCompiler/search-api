package response

import (
	"net/http"
)

// https://jsonapi.org/format/#errors
type ErrorResponseSource struct {
	Pointer   string `json:"pointer"`
	Parameter string `json:"parameter"`
}

type ErrorResponseObject struct {
	Status string              `json:"status"`
	Code   string              `json:"code"`
	Title  string              `json:"title"`
	Detail string              `json:"detail"`
	Source ErrorResponseSource `json:"source"`
}

type ErrorResponseBody struct {
	Errors []ErrorResponseObject `json:"errors"`
}

func ParseErrorNotFound(r *http.Request, objectId string) ErrorResponseBody {
	var resp ErrorResponseBody

	err := ErrorResponseObject{
		Status: "404",
		Code:   "404",
		Title:  "Not found",
		Detail: "The requested resource could not be found",
		Source: ErrorResponseSource{
			Pointer:   r.RequestURI,
			Parameter: objectId,
		},
	}

	resp.Errors = append(resp.Errors, err)

	return resp
}

func ParseErrorBadRequest(r *http.Request, message, parameter string) ErrorResponseBody {
	var resp ErrorResponseBody

	err := ErrorResponseObject{
		Status: "400",
		Code:   "400",
		Title:  "Bad Request",
		Detail: message,
		Source: ErrorResponseSource{
			Pointer:   r.RequestURI,
			Parameter: parameter,
		},
	}

	resp.Errors = append(resp.Errors, err)

	return resp
}

func ParseErrorUnprocessable(r *http.Request, objectId string) ErrorResponseBody {
	var resp ErrorResponseBody

	err := ErrorResponseObject{
		Status: "423",
		Code:   "423",
		Title:  "Unprocessable Entity",
		Detail: "The URL of the entity doesn't match its previous ID",
		Source: ErrorResponseSource{
			Pointer:   r.RequestURI,
			Parameter: objectId,
		},
	}

	resp.Errors = append(resp.Errors, err)

	return resp
}
