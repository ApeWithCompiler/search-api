package service

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/actor/index"
	"gitlab.com/apewithcompiler/heru/pkg/actor/search"
	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/provider/loader"
	"gitlab.com/apewithcompiler/heru/pkg/registry"
	"gitlab.com/apewithcompiler/heru/pkg/transform"
)

var (
	actorMailboxQueue = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "search_actor_mailbox_count",
			Help: "Count of requests in mailbox",
		},
		[]string{"name", "type"})
	actorMailboxSize = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "search_actor_mailbox_size",
			Help: "Size of mailbox",
		},
		[]string{"name", "type"})
)

type Orchestrator struct {
	IndexRegistry      registry.IndexRegistry
	ObjectStorageRegistry registry.ObjectStorageRegistry

	Index       map[string]*search.SearchActor
	IndexWriter map[string]*index.IndexUpdateActor
}

func NewOrchestrator(indexRegistry registry.IndexRegistry) Orchestrator {
	return Orchestrator{
		IndexRegistry:      indexRegistry,
		Index:              make(map[string]*search.SearchActor),
		IndexWriter:        make(map[string]*index.IndexUpdateActor),
	}
}

func (o *Orchestrator) CreateIndex(cfg config.IndexConfig) error {
	logger := log.WithFields(log.Fields{"module": "Orchestrator", "index": cfg.Name})

	factory, err := o.IndexRegistry.GetFactory(cfg.Provider)
	if err != nil {
		logger.Error("Cant get resource, provider not found: ", cfg.Provider)
		return err
	}

	provider := &loader.IndexReaderLazyLoader{
		Config:  cfg,
		Factory: factory.Reader,
	}

	var trans []transform.ResultTransformer
	for _, tcfg := range cfg.Transformer {
		switch tcfg.Name {
		case "urlfmt":
			logger.Info("Add transformer: ", tcfg.Name)
			trans = append(trans, &transform.FormattedUrlTransformer{})
		default:
			logger.Warn("Unknown transformer: ", tcfg.Name)
		}
	}

	srv := search.NewSearchActor(cfg.Name, provider, transform.NewTransformerSetDecorator(trans), 2000)

	go srv.DoWork()

	o.Index[cfg.Name] = &srv

	return nil
}
