package mock

/*
A sitemap document to inject for automated tests.
*/
const (
	EXAMPLE_SITEMAP string = `
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>https://example.com/tagged/coordination</loc>
		<lastmod>2011-11-29</lastmod>
		<changefreq>monthly</changefreq>
		<priority>1.0</priority>
	</url>
	<url>
		<loc>https://example.com/tagged/distributed</loc>
		<lastmod>2016-08-16</lastmod>
		<changefreq>monthly</changefreq>
		<priority>1.0</priority>
	</url>
</urlset>
`
)
