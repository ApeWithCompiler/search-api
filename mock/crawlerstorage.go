package mock

import (
	"fmt"
	"gitlab.com/apewithcompiler/heru/pkg/crawler/model"
)

type PrintCrawlerStorage struct {}

func (s *PrintCrawlerStorage) CreateCrawl(crawl model.Crawl) (int64, error) {
	fmt.Printf("Storage CreateCrawl depth: %v seeds: %v\n", crawl.Depth, crawl.Seeds)
	return 0, nil
}

func (s *PrintCrawlerStorage) CreateScrape(scrape model.Scrape) (int64, error) {
	fmt.Printf("Storage CreateScrape  %v\n", scrape.Url)
	return 0, nil
}
