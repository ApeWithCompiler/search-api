package mock

/*
A html document to inject for automated tests.
Whenever possible avoid requiring to couple on this.
Do not hardcode values in the tests. Instead define
constants here and reference these. This limits the
hazzard of future changes somewhat
*/
const (
	EXAMPLE_HTML_TITLE       string = "Title"
	EXAMPLE_HTML_DESCRIPTION string = "Mock HTML Document"
	EXAMPLE_HTML_AUTHOR      string = "Mock"
	EXAMPLE_HTML_KEYWORD     string = "MOCK"

	EXAMPLE_HTML string = `
<!doctype html>
<html>
	<head>
		<title>Title</title>
		<meta name="description" content="Mock HTML Document">
		<meta name="author" content="Mock">
		<meta name="keywords" content="MOCK">
	</head>
	<body>
	</body>
</html>
`
)
