package mock

import (
	"context"

	"gitlab.com/apewithcompiler/heru/pkg/crawler/loader/http"
)

type HttpLoaderMock struct {
	ResponeText string
}

func (l *HttpLoaderMock) Load(ctx context.Context, loaderRequest http.HttpLoaderRequest) (http.HttpLoaderResponse, error) {
	return http.HttpLoaderResponse{
		Url:         loaderRequest.Url,
		Body:        []byte(l.ResponeText),
		RedirectCnt: 0,
	}, nil
}
