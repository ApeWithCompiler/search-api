CREATE DATABASE IF NOT EXISTS crawler;
USE crawler;

CREATE TABLE IF NOT EXISTS hosts (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name CHAR(255) UNIQUE KEY,
	INDEX host_name (name)
	);

CREATE TABLE IF NOT EXISTS urls (
	id INT AUTO_INCREMENT PRIMARY KEY,
	address CHAR(255) UNIQUE KEY,
	host_id INT,
	INDEX url_address (address),
	INDEX url_host (host_id),
	FOREIGN KEY (host_id) REFERENCES hosts(id)
	);

CREATE TABLE IF NOT EXISTS files (
	id INT AUTO_INCREMENT PRIMARY KEY,
	ref CHAR(255) UNIQUE KEY
	);

CREATE TABLE IF NOT EXISTS crawls (
	id INT AUTO_INCREMENT PRIMARY KEY,
	depth INT,
	created TIMESTAMP
	);

CREATE TABLE IF NOT EXISTS seeds (
	id INT AUTO_INCREMENT PRIMARY KEY,
	crawl_id INT,
	url_id INT,
	FOREIGN KEY (crawl_id) REFERENCES crawls(id),
	FOREIGN KEY (url_id) REFERENCES urls(id)
	);

CREATE TABLE IF NOT EXISTS scrapes (
	id INT AUTO_INCREMENT PRIMARY KEY,
	crawl_id INT,
	url_id INT,
	file_id INT,
	created TIMESTAMP,
	INDEX scrape_crawl (crawl_id),
	INDEX scrape_url (url_id),
	INDEX scrape_file (file_id),
	INDEX url_id_created (id, created),
	FOREIGN KEY (crawl_id) REFERENCES crawls(id),
	FOREIGN KEY (url_id) REFERENCES urls(id),
	FOREIGN KEY (file_id) REFERENCES files(id)
	);

CREATE USER 'crawler'@'%' IDENTIFIED BY 'crawler';
GRANT SELECT, INSERT, UPDATE, DELETE ON crawler.* TO 'crawler'@'%';
