stages:
  - pre
  - vers
  - lint
  - test
  - build
  - dist

.runners:
  tags:
    - onprem
    - selfhosted

environment:
  image: docker:latest
  stage: pre
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - echo "Connecting to registry $CI_REGISTRY"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build -f "buildenv.Dockerfile" -t "$CI_REGISTRY_IMAGE/buildenv:latest" .
    - docker push "$CI_REGISTRY_IMAGE/buildenv:latest"
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - buildenv.Dockerfile

.bump-minor-version:
  image: $CI_REGISTRY_IMAGE/gitlabapi:latest
  stage: vers
  variables:
    AUTH_HEADER: "PRIVATE-TOKEN: ${CI_JOB_TOKEN}"
  script:
    - export NEW_VERSION=$(git tag -l | sort -V | tail -n 1 | awk -F. '{$2++; $3=0; print $1"."$2"."$3}')
    - curl --request POST --header "$AUTH_HEADER" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$NEW_VERSION&ref=master&message=autobump"
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "master" && $CI_COMMIT_TAG == null

.lint-go:
  extends: .runners
  image: $CI_REGISTRY_IMAGE/buildenv:latest
  stage: lint
  before_script:
    # Create UI dummy files.
    # Linter expects files to embed, but these will build in in the frontend job after.
    - mkdir -p ui/dist
    - touch ui/dist/foo
    - touch ui/dist/bar
  script:
    - go mod tidy
    - golangci-lint run --timeout 10m -e "SA9004" ./pkg/...

unittest-go:
  extends: .runners
  image: $CI_REGISTRY_IMAGE/buildenv:latest
  stage: test
  cache:
    - key:
        files:
          - go.mod
      paths:
        - /go/
  script:
    - go mod tidy
    - go test ./pkg/...
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - ./pkg/**/*

build-frontend:
  extends: .runners
  image: node:latest
  stage: build
  cache:
    - key: $CI_COMMIT_REF_SLUG
      paths:
        - node_modules/
  script:
    - cd ui
    - npm install
    - npm run build
  artifacts:
    name: "frontend"
    expire_in: 1 week
    paths:
      - ui/dist

build-querier:
  extends: .runners
  image: $CI_REGISTRY_IMAGE/buildenv:latest
  stage: build
  cache:
    - key:
        files:
          - go.mod
      paths:
        - /go/
  before_script:
    # Yoink the last tag from git cli if the commit does not provide gitlabs predifined var
    - if [ -z "$CI_COMMIT_TAG" ]; then export CI_COMMIT_TAG=$(git tag -l | sort -V | tail -n 1); fi
    - mkdir cmd/querier/ui
    - cp -r ui/dist cmd/querier/ui
  script:
    - echo -e "Build version $CI_COMMIT_TAG"
    - go mod tidy
    - go build -ldflags "-X gitlab.com/apewithcompiler/heru/pkg/version.GIT_TAG=$CI_COMMIT_TAG -X gitlab.com/apewithcompiler/heru/pkg/version.GIT_COMMIT=$CI_COMMIT_SHORT_SHA -X gitlab.com/apewithcompiler/heru/pkg/version.GIT_BRANCH=$CI_COMMIT_BRANCH" cmd/querier/querier.go
  artifacts:
    name: "heru"
    expire_in: 1 week
    paths:
      - querier
  needs:
    - build-frontend

build-docker:
  image: docker:latest
  stage: dist
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - echo "Connecting to registry $CI_REGISTRY"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build -f "Dockerfile" -t "$CI_REGISTRY_IMAGE/querier:latest" .
    - docker push $CI_REGISTRY_IMAGE/querier:latest
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
