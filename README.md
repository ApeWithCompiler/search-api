![logo](documentation/logo_full.png)

<br>

Something that will be a search engine, eventually.

Search tries to provide the functionallity of a search engine, but for hosting, configuring, modifying and curating yourself. This provides the advantage of fitting Search to your domain. For example searching for scientific pages, you will not be  irritated with commercial pages. And if, you can delete them from your index.

**Note:** Many parts are not mature yet. It is currently more like a proof of concept to learn and improve on. Breaking changes are expected. 

# Usage
Running Search requires a configuration file. For reference the ```config.yml.template``` can be used.
Provide the ```-config-file <filepath>``` flag, for using your configuration file.

## User Interface
Search comes with a VueJS based UI, embedded in its binary.
It will be available on your specified port, for example http://localhost:9098/

![ui](documentation/screenshot_ui.png)

## API
Search provides a HTTP API as it is a essential requrement for other applications to interact with it.
API endpoints are available behind a `/api` prefix, for example http://localhost:9098/api/indexes

## Quering

As with Google you can just enter your query. But also you have the option to advanced search operators. For that a specialized query interpreter was developed: [GIQL](https://gitlab.com/ApeWithCompiler/giql)
These operators are described in the [grammar description](https://gitlab.com/ApeWithCompiler/giql/-/blob/master/grammar_description.md).

**Note:** Wile the query is interpreted to a AST, the speciffic index provider needs to support the functionallity. For example the provider offering access to stackexchanges API, does not support these. 

# Building
Building Search is a two step processes.
As the user interface is embedded, Golang expects a user interface to embed, or it will fail. The required path for embedding is `ui/dist`. As it is a artifact resulting from building the VueJS page, it is not included, but must be builded yourself.

## User Interface

Building the user interface requires these steps:

* Navigate in the ui folder
* Download npm dependencies
* Build frontend

```
cd ui
npm install
npm run build
```
This should provide you with a `ui/dist` folder.

## Binary

Successfully optained the `ui/dist` folder, the main binary can now be build.
Building the binary requires following septs:

* Clean up Golang dependencies
* Download Golang dependencies
* Build binary

```
go mod tidy
go build heru.go
```
