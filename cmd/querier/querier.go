package main

import (
	"flag"
	"fmt"
	"os"

	"embed"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/pkg/config"
	"gitlab.com/apewithcompiler/heru/pkg/http/route"
	"gitlab.com/apewithcompiler/heru/pkg/provider/stackexchange"
	"gitlab.com/apewithcompiler/heru/pkg/provider/zinc"
	"gitlab.com/apewithcompiler/heru/pkg/registry"
	"gitlab.com/apewithcompiler/heru/pkg/service"
	"gitlab.com/apewithcompiler/heru/pkg/validator"
	"gitlab.com/apewithcompiler/heru/pkg/version"
)

//go:embed ui/dist
var frontend embed.FS

// flagDefaultAfterParse helper function to choose default if value empty
func flagDefaultAfterParse(val, def string) string {
	if val != "" {
		return val
	}

	return def
}

// getFirstIndexName helper function to access slice with length check
func getFirstIndexName(indexes []config.IndexConfig) string {
	if len(indexes) > 0 {
		return indexes[0].Name
	}
		
	log.WithFields(log.Fields{"module": "main"}).Warn("No index configured in config.")

	return ""
}

func main() {
	portFlag := flag.String("listen-address", ":9098", "Listen address of the api")
	confFlag := flag.String("config-file", config.GetDefaultFile(), "path to the config file")
	logLevelFlag := flag.String("log-level", "warn", "log level")
	versionFlag := flag.Bool("version", false, "Print version")
	homeRedirectFlag := flag.String("redirect-home", "", "Redirect to specific index home view")

	flag.Parse()

	// If version flag used, print version, ignore everything else and exit peacefully.
	if *versionFlag {
		fmt.Print(version.GetVersionString())
		os.Exit(0)
	}

	switch *logLevelFlag {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		fmt.Println("Invalid log level. Valid parameters are: \"debug\", \"info\", \"warn\", \"error\"")
		os.Exit(1)
	}

	config.ReadConfigFile(*confFlag)
	conf := config.GetCurrentConfig()
	// Register available providers
	// Config validation needs to know which providers are available
	// to warn about invalid provider reference
	indexReg := registry.NewIndexRegistry()

	indexReg.RegisterFactory("stackexchange", registry.IndexRegistryEntry{
		Reader: &stackexchange.StackexchangeFactory{},
	})

	indexReg.RegisterFactory("zinc", registry.IndexRegistryEntry{
		Reader: &zinc.ZincFactory{},
		Writer: &zinc.ZincFactory{},
	})

	indexValidator := validator.NewIndexConfigValidator(indexReg, conf)
	if !indexValidator.Validate() {
		log.WithFields(log.Fields{"module": "main"}).Error("Invalid config")
		os.Exit(1)
	}

	orchestrator := service.NewOrchestrator(indexReg)

	for _, cfg := range conf.GetAllIndexConfigs() {
		orchestrator.CreateIndex(cfg)
	}

	router := route.NewHttpRouterControler(*portFlag)
	router.RegisterMetrics()

	router.ProtectReads = false
	router.ProtectQueries = false
	router.ProtectWrites = false

	for name, srv := range orchestrator.Index {
		router.RegisterIndexResource(name, srv)
	}
	router.RegisterIndexCollection()

	router.RedirectHomeIndex = flagDefaultAfterParse(*homeRedirectFlag, getFirstIndexName(conf.Indexes))
	router.RegisterFrontend(frontend)

	router.HandleRequest()
}
