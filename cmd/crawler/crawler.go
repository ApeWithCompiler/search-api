package main

import (
	"context"
	"time"
	"flag"

	log "github.com/sirupsen/logrus"

	"gitlab.com/apewithcompiler/heru/mock"
	"gitlab.com/apewithcompiler/heru/pkg/crawler"
	"gitlab.com/apewithcompiler/heru/pkg/model/web"
)

func main() {
	log.SetLevel(log.InfoLevel)

	sitemapArg := flag.Bool("scrape-sitemap", false, "Scrape the sitemap for links to follow")
	flag.Parse()
	
	s := mock.PrintCrawlerStorage{}

	c := crawler.NewCrawlerBuilder().
	SetScrapeTimeout(time.Duration(5000) * time.Millisecond).
	SetStorage(&s).
	Build()

	var urls []web.DigestUrl
	for _, arg := range flag.Args() {

		u, _ := web.UrlFromString(arg)
		urls = append(urls, u)
	}
	
	ctx := context.Background()

	if *sitemapArg {
		c.CrawlSitemap(ctx, urls)
	} else {
		c.Crawl(ctx, 3, urls)
	}

	c.Run()
}
