FROM alpine:latest

RUN apk add libc6-compat

COPY querier /bin/querier
COPY demo.yml /config.yml

RUN chmod u+x /bin/querier

EXPOSE 9098
ENTRYPOINT ["/bin/querier"]
CMD ["-config-file=/config.yml"]
