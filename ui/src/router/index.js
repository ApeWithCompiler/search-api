import {createRouter, createWebHistory} from 'vue-router'

import HomeView from '../views/HomeView.vue'
import SearchView from '../views/SearchView.vue'

const DEFAULT_TITLE = 'Heru'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/home/:index',
            name: 'home',
            component: HomeView
        },
        {
            path: '/search/:index',
            name: 'search',
            component: SearchView
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (About.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import('../views/AboutView.vue')
        }
    ]
})

router.beforeEach((to, from, next) => {
    const query = to.query["q"];
    const index = to.params.index;

    if (query != null) {
      document.title = decodeURIComponent(query) + " - " + index;
    } else {
      document.title = index;
    }
    
    next();
});

export default router
