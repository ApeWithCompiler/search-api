import './assets/base.css';
import './assets/colors.css';

import * as mdijs from '@mdi/js'
import mdiVue from 'mdi-vue/v3'
import {createApp} from 'vue'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(router)
app.use(mdiVue, {icons: mdijs})

app.mount('#app')
