# Components

## Indices
Indices provides searching functionality over datasets. It needs to handel complex queries for delivering precise results in reasonable time.

``A index is not meant to be concerned with data persistance`` To clarify this statement: The index is used as a specialized model for reading that is derived from some dataset itself.

### Foreign Indices
Foreign Indices describe Indices that are managed by a different entity.
That could be an API like stack exchange or a federaded instance of *search*.
This implies that this *search* instance has zero responsibility or assurence over the index data.
As a consequence Foreign Indices are truthly considered a *read only* data model.

### Local Indices
Local Indices in contrast describe Indices that are managed by this *search* instance.