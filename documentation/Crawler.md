# Crawler

## Start a crawl


```json
POST /api/crawler/:name/crawl

{
	"data": {
		"type": "crawlerjob",
		"attributes": {
				"depth": 2,
				"links": [
					"https://example.org/"
				]
			}				
		}
}
```
