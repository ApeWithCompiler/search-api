# Stackexchange

## Description

Provides a module to  connect to a [stackexchange api](https://api.stackexchange.com/).

## Capabilties

 * ReadIndex

## Usage

```yaml
- name: example
  provider: stackexchange
  options:
  ...
```

## Options

| Name | Type | Default | Description |
| ----------- | ----------- | ----------- | ----------- |
| host | string | none | Address of the stackexchange api |
| site | string | none | Stackexchange sub page |
| sort | string | none | Key of which the api sorts its results |