# Zincsearch

## Description

Provides a module to  connect to a [Zincsearch](https://zincsearch-docs.zinc.dev/) index.

## Capabilties

 * ReadIndex
 * WriteIndex

## Usage

```yaml
- name: example
  provider: zinc
  options:
  ...
```

## Options

| Name | Type | Default | Description |
| ----------- | ----------- | ----------- | ----------- |
| host | string | none | Host which zincsearch runs on |
| index | string | none | Name of the index to be used |
| username | string | none | Username for authentication |
| password | string | none | Password for authentication |