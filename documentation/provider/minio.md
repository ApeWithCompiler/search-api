# MinIO

## Description

Provides a module to  connect to a [MinIO](https://min.io/) storage.

## Capabilties

 * Repository

## Usage

```yaml
- name: example
  provider: minio
  options:
  ...
```

## Options

| Name | Type | Default | Description |
| ----------- | ----------- | ----------- | ----------- |
| host | string | none | Host which MinIO runs on |
| bucket| string | none | Name of the Bucket to be used |
| key | string | none | Name of the access key |
| secret | string | none | Secret of the access key |