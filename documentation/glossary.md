# Glossary

| Name | Description |
| ----------- | ----------- |
| Component | A concept that search includes. Valid components are <ul><li>Pipeline</li><li>Index</li><li>Storage</li></ul> |
| Component Name | Name of the instance of the component |
| Component Type | Type of the component |
| Provider | A implementation of one or more components |
| Provider Name | Name of the provider implementation |