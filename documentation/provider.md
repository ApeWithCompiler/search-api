# Provider

## Interface

For a provider to offer either indexing or storage capabilities, the following interfaces must be met.

### Index

``` go
type DocumentIndex interface {
    Search(query model.Query) []model.SearchResult
}
```

## Provider structure

A provider module should follow these guided structure
to encourage a consistancy troughout the project.
It is not needed to include all these files, even when working with a specific scope. These are purely optional guidelines.

Scope description:

- `general` General files
- `index` Files that are common when the provider offers index capabilities
- `remote` Files that are common the provider uses a remote resource (e.g. webserver)

| File | Scope | Description |
| ----------- | ----------- | ----------- |
| `provider.go` | general | Contains the most general provider stuct |
| `config.go` | general | Contains provider configuration |
| `search.go` | index | Contains code for searching capabilities |
| `parser.go` | index | If the provider uses the search query AST, a parsing client for transforming should be put here |
| `url.go` | remote | If remote urls are needed, the needed code can be put here |
| `query.go` | remote | If remote queries are needed, they can be placed here |
| `response.go` | remote | Responses from remote should be put here |